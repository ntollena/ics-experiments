open Batteries

(* Assumes l is sorted and p is a percentile *)
let percentile p l =
  let len = List.length l in
  let pf = float_of_int len  *. float_of_int p /. 100. in
  let floor = int_of_float % Float.floor in
  List.init (len / floor pf) (fun i -> List.at l (i * (floor pf)))

let percentile_list p cmp l  =
  List.sort cmp l
  |> percentile p

open Float 

let sum = List.fold_left (+) zero

  let mean l =
    sum l /  float_of_int (List.length l)

let square x = x * x

let pseudo_variance mean_l l =
  List.map (fun x -> square (x - mean_l)) l
  |> sum

let variance l =
  let mean_l = mean l in
  pseudo_variance mean_l l / (float_of_int @@ List.length l)

let std_dev l =
sqrt @@ variance l 

let correlation  lx ly =
  let mean_x = mean lx
  and mean_y = mean ly in
  let cov x y = (x - mean_x) * (y - mean_y) in
  let covariance_xy = List.map2 cov lx ly
                    |> sum in
  covariance_xy / sqrt (pseudo_variance mean_x lx * pseudo_variance mean_y ly)


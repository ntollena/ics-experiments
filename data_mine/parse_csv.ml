open Batteries
module Ag = Angstrom
open Angstrom
open Parse_common
open Utils

let float =
  let* pre = many1 num_char in
  let* point = char '.'  in
  let+ post = many num_char in
  pre @ [point] @ post
  |> String.implode
  |> float_of_string

(* Parse float first and if fail parses as integer.
 * The order matters because a float can be validly parsed as an int 
 * (before the dot) *)
let float_int = (float >>| Either.left) <|> (integer >>| Either.right)

type pline = Int of int | Fl of float | Ident of string [@@deriving show]


let line_pat = (float >>| fun f -> Fl f) <|> (integer >>| fun i -> Int i)
               <|> (many1 (not_char ',') >>| fun s -> Ident (String.implode s))

let parse_int = sep_by (enclose_ws @@ char ',') integer
let parse_line = sep_by (enclose_ws @@ char ',') float_int

let parse_extended = sep_by (enclose_ws @@ char ',') line_pat

let parse_first_line = sep_by (enclose_ws @@ char ',') (many1 (not_char ',') >>| String.implode) 


let parse_with pattern = parse_string ~consume:All pattern
                         %> tee (function Error e -> print_endline e | _ -> ())
                         %> function Ok v -> v
                                   | Error s -> failwith s

let sep_input first_line next_lines =
  let list_values = zip_all next_lines in
  List.map2 (fun name value -> name, value) first_line list_values

let parse_integers filename =
  File.lines_of filename
  |> List.of_enum
  |> List.map ( parse_with parse_int)

let parse_without_header filename =
  File.lines_of filename
  |> List.of_enum
  |> List.map  @@ parse_with parse_line

let parse_csv_extended filename =
  File.lines_of filename
  |> List.of_enum
  |> List.map ( parse_with parse_extended)

let parse_csv filename =
  File.lines_of filename
  |> List.of_enum
  |> function h::tail ->
    let first = parse_with parse_first_line h in
    let values = List.map (parse_with parse_line) tail in
    sep_input first values
            | [] -> failwith "Empty file"


let vec_size = 16

type dim = F | C | Y | X | H | W [@@deriving show, eq]

let dim_from_str = function
  | 'f' -> F
  | 'c' -> C
  | 'y' -> Y
  | 'x' -> X
  | 'h' -> H
  | 'w' -> W
  | _ -> failwith "Invalid char" 

type spec = UTV of int * dim
          | ULambda of dim
          | Apply of dim * (int * int) list
          | Other [@@deriving show]



import pandas as pd

df0 = pd.read_csv("./cleancsvs/fulldata.csv")

df = df0[['benchname','perf','computation','lambda','loop1','loop2','L1size_0','L2size_0','L1vol_0','L2vol_0','L1size_1','L2size_1','L1vol_1','L2vol_1','L1size_2','L2size_2','L1vol_2','L2vol_2']]
for i in range(3):
    df.insert(1,f'OI_{i}',[ x/(2*y+z) for x,y,z in zip(df[f'computation'].values, df[f'L1vol_{i}'].values, df[f'L2vol_{i}'].values)])

df.to_csv('test_weka.csv',index=False)

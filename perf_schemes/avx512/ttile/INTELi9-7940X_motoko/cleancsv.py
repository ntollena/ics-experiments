#!/usr/bin/env python3
## read log file: dump well formatted scheme
import re

defaultCsvList=["skx_mobilNet_1_tiles.log","skx_mobilNet_5_tiles.log","skx_mobilNet_9_tiles.log","skx_resNet18_1_tiles.log","skx_resNet18_5_tiles.log","skx_resNet18_9_tiles.log","skx_yolo9000_18_tiles.log","skx_yolo9000_5_tiles.log","skx_mobilNet_2_tiles.log","skx_mobilNet_6_tiles.log","skx_resNet18_10_tiles.log","skx_resNet18_2_tiles.log","skx_resNet18_6_tiles.log","skx_yolo9000_0_tiles.log","skx_yolo9000_19_tiles.log","skx_yolo9000_8_tiles.log","skx_mobilNet_3_tiles.log","skx_mobilNet_7_tiles.log","skx_resNet18_11_tiles.log","skx_resNet18_3_tiles.log","skx_resNet18_7_tiles.log","skx_yolo9000_12_tiles.log","skx_yolo9000_2_tiles.log","skx_yolo9000_9_tiles.log","skx_mobilNet_4_tiles.log","skx_mobilNet_8_tiles.log","skx_resNet18_12_tiles.log","skx_resNet18_4_tiles.log","skx_resNet18_8_tiles.log","skx_yolo9000_13_tiles.log","skx_yolo9000_4_tiles.log"]

newls = []
for fname in defaultCsvList:
    with open(fname, 'r') as inf:
        with open(re.sub("tiles","tiles-formatted",fname), "w") as ouf:
            ouf.write("scheme"+"\n")
        cnt = 1
        for l in inf.readlines():
            ls = l.split()
            #print("pre slip",ls)
            if ls[1] ==':':
                assert int(ls[0]) == cnt
                #print("Final version split",newls)
                #print("Final version"," ".join(newls))
                finall =" ".join(newls)
                finall = "\"" + finall
                finall = finall + "\""
                #print("Final version",finall)
                if newls:
                    with open(re.sub("tiles","tiles-formatted",fname), "a") as ouf:
                        ouf.write(finall+"\n")
                    print(finall)
                cnt += 1
                newls = ls[2:].copy()
            else:
                newls.extend(ls)
        print(finall)

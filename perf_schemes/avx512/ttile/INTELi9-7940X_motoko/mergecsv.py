import pandas as pd
import numpy as np

dfbuggy = pd.read_csv("volume_metric_buggy_ioopt/fulldata.csv")
dfdefault = pd.read_csv("volume_metric_default_perm/fulldata.csv")
dfioopt = pd.read_csv("volume_metric_ioopt/fulldata.csv")

dfioopt['version'] = "ioopt"
dfdefault['version'] = "default"
dfbuggy['version'] = "buggy"


dfmerge = dfioopt.append(dfdefault).append(dfbuggy)

lcomp = []
liom = []
ldm = []
lbm = []
#print("(name,maxperf,ratioIoopt,ratioDefault,ratioBuggy)")
for bench in list(set(dfmerge.benchname.values)):
    m = max(dfmerge.loc[dfmerge.benchname == bench,'perf'].values)
    iom = max(dfioopt.loc[dfioopt.benchname == bench,'perf'].values)
    liom.append(iom/m)
    dm = max(dfdefault.loc[dfdefault.benchname == bench,'perf'].values)
    ldm.append(dm/m)
    bm = max(dfbuggy.loc[dfbuggy.benchname == bench,'perf'].values)
    lbm.append(bm/m)
    lcomp.append((bench,m,"{:.2f}".format(iom/m),"{:.2f}".format(dm/m),"{:.2f}".format(bm/m)))
#print(lcomp)

def stat(l):
    return ("{:.2f}".format(np.mean(l)),"{:.2f}".format(np.median(l)),"{:.2f}".format(np.std(l)),"{:.2f}".format(min(l)),"{:.2f}".format(max(l)))

print("mean,median,std,min,max")
print("Ioopt")
print(stat(liom))
print("Default")
print(stat(ldm))
print("Buggy")
print(stat(lbm))



# Information


## XeonGold6130
* 1 socket Cascade Lake:
* Nancy gros
* modelname: Intel(R) Xeon(R) Gold 5220 CPU @ 2.20GHz
* AVX512 2FMA
* 18 cores
* l1,l2,l3 = 32KB(32768),1024KB(1048576),24.75MB(25952256)

methode mesure: metrique
python3 launch.py XeonGold6130 --avx avx512 --architecture skylake --nbthreads 32


## Xeon Gold5220
* 2 sockets Skylake:
* Grenoble dahu
* modelname: Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz
* AVX512 2FMA
* 16 cores
* l1,l2,l3 = 32KB(32768),1024KB(1048576),22MB(23068672)

methode mesure: random
--> modify the code inside tvm_ttile/ttile/tensorize/parser.py line 342 ("metric" into "random")
python3 launch.py XeonGold6130 --avx avx512 --architecture skylake --nbthreads 32

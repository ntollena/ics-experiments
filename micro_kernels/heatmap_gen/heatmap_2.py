import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    #for i in range(data.shape[0]):
    #    for j in range(data.shape[1]):
    #        kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
    #        text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
    #        texts.append(text)

    return texts


fname = "data_microkern_AVX512_nohw"
file2read=fname+".txt"
df = pd.read_csv(file2read)
F = [int(x) for x in list(df['f'])]
Y = [int(y) for y in list(df['y'])]
perf = list(df["perf"])
data=[]
datasquared=[]
name = "motokoData"
size =16
for x in range(size-1):
    data.append([0 for x in range(size-1)])
    datasquared.append([0 for x in range(size-1)])
for f,y,p in zip(F,Y,perf):
    #print(y,f,p)
    data[y-1][f-1] = round(p, 1)
    datasquared[y-1][f-1] = data[y-1][f-1]**2

fig, ax = plt.subplots()

im, cbar = heatmap(np.array(data), [str(x) for x in range(1,16)],
#im, cbar = heatmap(np.array(data), [str(x) for x in range(1,16)],
        [str(x) for x in range(1,16)], ax=ax,
                   cmap="YlGn", cbarlabel="% of peak performance")
texts = annotate_heatmap(im, valfmt="{x/100:.1f} t")

#F=2 : Y= [8, 15] (sauf que 14)
plt.vlines(x=0.5, ymin=6.5, ymax=13.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.vlines(x=1.5, ymin=6.5, ymax=13.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=6.5, xmin=0.5, xmax=1.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=13.5, xmin=0.5, xmax=1.5, colors='red', ls='-', lw=2, label='Above 85 %')

# F=3 : Y = [5, 10]
plt.vlines(x=1.5, ymin=3.5, ymax=9.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.vlines(x=2.5, ymin=3.5, ymax=9.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=3.5, xmin=1.5, xmax=2.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=9.5, xmin=1.5, xmax=2.5, colors='red', ls='-', lw=2, label='Above 85 %')

# F=4 : Y = [6, 7]
plt.vlines(x=2.5, ymin=4.5, ymax=6.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.vlines(x=3.5, ymin=4.5, ymax=6.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=4.5, xmin=2.5, xmax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=6.5, xmin=2.5, xmax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')

# F=5 : Y= [5, 6]
plt.vlines(x=3.5, ymin=3.5, ymax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.vlines(x=4.5, ymin=3.5, ymax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=3.5, xmin=3.5, xmax=4.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=5.5, xmin=3.5, xmax=4.5, colors='red', ls='-', lw=2, label='Above 85 %')

# F=6: Y=4
plt.vlines(x=4.5, ymin=2.5, ymax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.vlines(x=5.5, ymin=2.5, ymax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=2.5, xmin=4.5, xmax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')
plt.hlines(y=3.5, xmin=4.5, xmax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')


#plt.hlines(y=9.5, xmin=1.5, xmax=2.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.vlines(x=2.5, ymin=9.5, ymax=7.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=7.5, xmin=2.5, xmax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.vlines(x=3.5, ymin=5.5, ymax=7.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=5.5, xmin=3.5, xmax=4.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.vlines(x=4.5, ymin=4.5, ymax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=4.5, xmin=4.5, xmax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.vlines(x=5.5, ymin=3.5, ymax=4.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=3.5, xmin=5.5, xmax=6.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.vlines(x=6.5, ymin=2.5, ymax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=2.5, xmin=5.5, xmax=6.5, colors='red', ls='-', lw=2, label='Above 85 %')

#plt.vlines(x=4.5, ymin=3.5, ymax=2.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.vlines(x=3.5, ymin=4.5, ymax=3.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=3.5, xmin=3.5, xmax=4.5, colors='red', ls='-', lw=2, label='Above 85 %')
#plt.hlines(y=2.5, xmin=4.5, xmax=5.5, colors='red', ls='-', lw=2, label='Above 85 %')
fig.tight_layout()
plt.show()

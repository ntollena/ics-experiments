import pandas as pd

fname = "onednn_32threads_2sockets.csv"
df = pd.read_csv(fname)

dfnew = df.copy()
dfnew['perf_XYF_XYC_WHCF'] =dfnew['perf_XYF_XYC_WHCF']/32. 
dfnew['perf_FYX_CYX_FCHW'] =dfnew['perf_FYX_CYX_FCHW']/32. 
dfnew['best'] = dfnew[['perf_FYX_CYX_FCHW','perf_XYF_XYC_WHCF']].max(axis=1)
print(df)
print(dfnew)
dfnew.to_csv("results_32threads_2sockets_formatted.csv")

import re
import pandas as pd
import os


defaultCsvList=["MobilNet_01.csv","MobilNet_05.csv","MobilNet_09.csv","ResNet18_01.csv",
        "ResNet18_05.csv","ResNet18_09.csv","Yolo9000_18.csv","Yolo9000_05.csv","MobilNet_02.csv","MobilNet_06.csv","ResNet18_10.csv","ResNet18_02.csv",
        "ResNet18_06.csv","Yolo9000_0.csv","Yolo9000_19.csv","Yolo9000_08.csv","MobilNet_03.csv",
        "MobilNet_07.csv","ResNet18_11.csv","ResNet18_03.csv","ResNet18_07.csv",
        "Yolo9000_12.csv","Yolo9000_02.csv","Yolo9000_09.csv","MobilNet_04.csv","MobilNet_08.csv",
        "ResNet18_12.csv","ResNet18_04.csv",
        "ResNet18_08.csv","Yolo9000_13.csv","Yolo9000_04.csv","Yolo9000_23.csv"]

def reformat(csvdir,prefix="result_",debug=0):
    newdir = csvdir + "/formatted/"
    os.system(f'mkdir -p {newdir}')
    for fname in defaultCsvList:
        fname = prefix+fname
        fin = csvdir+'/'+fname
        fout = newdir + fname
        if debug:
            print("Formatting...")
        with open(fout, "w")  as ouf:
            if debug:
                print("Dump in",fout)
            with open(fin, "r") as inf:
                if debug:
                    print("From",fin)
                first = True
                for l in inf.readlines():
                    if first:
                        print(l)
                        first = False
                        ls = l.split(";")
                        last = ls[-1]
                        slast = last.split("(")
                        keep = slast[0]
                        slast = str(slast[1])
                        n = re.sub("\)\n","",slast)
                        p = ls[:-1]+[keep]
                        ouf.write(";".join(p)+"\n")
                        second = [-1,"ttile",n,0,0,0,0,0,n]
                        ouf.write(";".join([str(x) for x in second])+"\n")
                    else:
                        ouf.write(l)
    return newdir

def get_bench_name(prefix,fname,suffix):
    reg = f"{prefix}([a-zA-Z0-9]+\_[0-9]+){suffix}\.csv"
    m = re.search(reg,fname)
    csize_name = "debug"
    if not m:
        print(reg,prefix,fname,suffix)
        print("regex not found")
        exit(-1)
    else:
        csize_name = m.group(1)
    csize_name = change_name(csize_name)
    return csize_name


def change_name(old):
    new = old
    reg = "([a-zA-Z0-9]+\_)([0-9])\]"
    new += "]"
    m_sub = re.search(reg,new)
    if m_sub:
        new = m_sub.group(1) +  "0" + m_sub.group(2)
    else:
        new =new[:-1]
    new = new[0].upper() + new[1:]
    print("old/new",old,new)
    return new

def change_names(src,dst):
    print("change_names",src,dst)
    with open(src,"r") as inf:
        with open(dst,"w") as ouf:
            for l in inf.readlines():
                ls = l.split(',')
                new = old = ls[1]
                reg = "([a-zA-Z0-9]+\_)([0-9])\]"
                new += "]"
                m_sub = re.search(reg,new)
                if m_sub:
                    new = m_sub.group(1) +  "0" + m_sub.group(2)
                else:
                    new =new[:-1]

                ls.pop(1)
                ls.insert(1,new)
                newl = ",".join(ls)
                ouf.write(newl)

def merge_name(csvdir, namel=defaultCsvList, sep=";",dump=False,debug=0):
    dfs = []
    print("Merging...")
    for fname in namel:
        fin = csvdir+fname
        if debug:
            print("Reading",fin)
        df = pd.read_csv(fin,sep=sep)
        reg = "skx\_([a-zA-Z0-9_]+)\.csv"
        m_sub = re.search(reg,fin)
        if m_sub:
            benchname = m_sub.group(1)
        else:
            print("Error:",m_sub,reg,fin)
            exit(-1)
        df['benchname'] = [benchname] * len(df.perf.values)
        dfs.append(df)
    
    big_df = pd.concat(dfs)
    if dump:
        big_df.to_csv(csvdir+"/tmp.csv", index=False, header=True)
        change_names(csvdir+"/tmp.csv",csvdir+"/fulldata.csv")
    return big_df

def merge(csvdir, namel=defaultCsvList, prefix="", suffix="",sep=";",dump=False,debug=0):
    """ From csvdir: merge all csv files included into one 
        dataframe (or fulldata.csv if dump=True)
    """
    dfs = []
    print("Merging...")
    for fname in namel:
        fname = prefix+fname
        fin = csvdir+fname
        if debug:
            print("Reading",fin)
        df = pd.read_csv(fin,sep=sep)
        df["benchname"] = get_bench_name(prefix,fname,suffix)
        dfs.append(df)
    big_df = pd.concat(dfs)
    if dump:
        big_df.to_csv(csvdir+"/tmp.csv", index=False, header=True)
        change_names(csvdir+"/tmp.csv",csvdir+"/fulldata.csv")
    return big_df

def reformat_and_merge(csvdir,prefix="result",debug=0):
    newdir = reformat(csvdir,prefix=prefix,debug=debug)
    merge(newdir,prefix=prefix,sep=";",dump=True,debug=debug)

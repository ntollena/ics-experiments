#!/usr/bin/env python3

import argcomplete, argparse
import math
import numpy as np
import os
import pandas as pd 
import re
import sys
from reformat import get_bench_name

defaultCsvList=["skx_mobilNet_1_all.csv","skx_mobilNet_5_all.csv","skx_mobilNet_9_all.csv","skx_resNet18_1_all.csv","skx_resNet18_5_all.csv","skx_resNet18_9_all.csv","skx_yolo9000_18_all.csv","skx_yolo9000_5_all.csv","skx_mobilNet_2_all.csv","skx_mobilNet_6_all.csv","skx_resNet18_10_all.csv","skx_resNet18_2_all.csv","skx_resNet18_6_all.csv","skx_yolo9000_0_all.csv","skx_yolo9000_19_all.csv","skx_yolo9000_8_all.csv","skx_mobilNet_3_all.csv","skx_mobilNet_7_all.csv","skx_resNet18_11_all.csv","skx_resNet18_3_all.csv","skx_resNet18_7_all.csv","skx_yolo9000_12_all.csv","skx_yolo9000_2_all.csv","skx_yolo9000_9_all.csv","skx_mobilNet_4_all.csv","skx_mobilNet_8_all.csv","skx_resNet18_12_all.csv","skx_resNet18_4_all.csv","skx_resNet18_8_all.csv","skx_yolo9000_13_all.csv","skx_yolo9000_4_all.csv"]


parser = argparse.ArgumentParser(description='Generate 1 csv from one per bench. 300sec on my laptop (Hugo).')
parser.add_argument('root_dir', help="Precise absolute path for root dir of ics-experiments.")
parser.add_argument('--csv', help= "list of csv files to read per bench brutdata read from.", nargs = '+', type=str, default=defaultCsvList)
parser.add_argument('--output', help= "csv filename.", type=str, default="fulldata.csv")
parser.add_argument('--archi', help= "csv filename prefix: default skx.", type=str, default="skx")
parser.add_argument('--suffix', help= "csv filename suffix: default _all.", type=str, default="_all")
parser.add_argument('--debugname', help= "name for debug.", type=str, default="debug")
parser.add_argument('--vectorsize', help= "Vector size in number of words(floats).", type=int, default=16)
parser.add_argument('--L1L2size', help= "L1 and L2 cache size in number of lines of cache: list of couples. Default --L1L2size 512 16384", nargs='+', type=int, default=[512,16384])
parser.add_argument('--verbose', help="for debug purpose", type=int, default=0)
parser.add_argument('--microperf', help="Add microperf column: double time to generate csv on laptop.", action='store_true',default=False)


argcomplete.autocomplete(parser)
args = parser.parse_args()

fallnames = args.csv
file2write = args.output 
print("Writing in",file2write)

# in cache lines
L1size = args.L1L2size[0]
L2size = args.L1L2size[1]
lL1size = [args.L1L2size[index] for index in range(0,len(args.L1L2size),2)]
lL2size = [args.L1L2size[index] for index in range(1,len(args.L1L2size),2)]
print(L1size,L2size,lL1size,lL2size)
vector_size = args.vectorsize

DEBUG=args.verbose

def display(parsed_tree,currindex):
    global DEBUG
    if DEBUG<1:
        return
    mbranch = parsed_tree[0]
    branch1 = parsed_tree[1]
    branch2 = parsed_tree[2]
    assert len(branch1) == len(branch2)
    mindex = currindex - len(branch1)
    if mindex > 0:
        print(branch1)
        print(branch2)
        print(mbranch[:mindex])
    else:
        print(branch1[:currindex+1])
        print(branch2[:currindex+1])

def pdebug(s,s1="",s2="",debug=1):
    global DEBUG
    if DEBUG>debug:
        print(s,s1,s2)

def computeIterations(currindex, mainbranch, branch1=None, branch2 = None, control_coeff=0):
    """ Returns the product of all levels of iterations
        If branch: tuple (branch1, branch2) 
        if just one: tuple (main_branch,)
        returns only 2 numbers in branch and 1 number above
    """
    pdebug("COMPUTE ITERATIONS",currindex)
    if branch1:
        tmp_branch1 = branch1[currindex:]
        tmp_branch2 = branch2[currindex:]
        tmp_mbranch = []
        assert len(branch1) == len(branch2)
        mindex = max(currindex - len(branch1),0)
        tmp_mbranch = mainbranch[mindex:]
        pdebug("tmp_branch1",tmp_branch1)
        pdebug("tmp_branch2",tmp_branch2)
        pdebug("tmp_mbranch",tmp_mbranch)
        ## Branch 1
        totaliterations = [1,1]
        control_coeffbr1 = control_coeff
        control_coeffbr2 = control_coeff
        first = True
        for key,size,t in tmp_branch1:
            if t =="T" and not first:
                totaliterations[0] += control_coeffbr2
                #control_coeffbr2 -=4
                #control_coeffbr2 = max(control_coeffbr2,0)
                first = False
            totaliterations[0] *= size
        first = True
        for key,size,t in tmp_branch2:
            if t =="T" and not first:
                totaliterations[1] += control_coeffbr1
                #control_coeffbr1 -=4
                #control_coeffbr1 = max(control_coeffbr1,0)
            first = False
            totaliterations[1] *= size
        for key,size,t in tmp_mbranch:
            totaliterations[0] *= size
            totaliterations[1] *= size
        if mindex>0:## in main branch
            ## only main branch, tmp branch1 and 2 are empty
            assert totaliterations[0] == totaliterations[1]
            return [(totaliterations[0] + totaliterations[1])/2]
        else: ## branch1 and 2 not merged
            return totaliterations
    else:
        tmp_mbranch = mainbranch[currindex:]
        totaliterations = 1
        first = True
        for key,size,t in tmp_mbranch:
            if t =="T" and not first:
                size += control_coeff
                #control_coeff -=4
                #control_coeff = max(control_coeff,0)
            first = False
            totaliterations *= size
        return [totaliterations]

def computeFootprint(currindex, mainbranch, branch1=None, branch2 = None, computeVolume=False):
    """
         output: f*x*y
         input: c*(x+w-1)*(y+h-1) 
         params: f*c*w*h
         returns only 2 numbers in branch and 1 number above
    """
    if computeVolume:
        volScale = 2
        divScale = 1
    else:
        volScale = 1
        divScale = 2
    def footprint(currsizes,volScale):
        sf =  currsizes["f"]
        sx = currsizes["x"]
        sy = currsizes["y"]
        sc = currsizes["c"]
        sw = currsizes["w"]
        sh = currsizes["h"]

        ## counting cache lines
        ## 1 word is 4 bytes, 64/4 --> 16 words
        ## 16 words by Line of Cache
        # output: inner dimension is F
        sf_inner = math.ceil(sf/16)
        outputs = volScale * sf_inner * sx * sy 
        # params: inner dimension is F
        params =  sf_inner * sc * sw * sh
        sc_inner = math.ceil(sc/16)
        # inputs: inner dimension is C
        inputs = sc_inner * (stride*sx + sw -1) * (stride*sy + sh -1)
        pdebug(f"footprint = outputs ({sf_inner} {sx} {sy} ) + params ({sf_inner} {sc} {sw} {sh}) + inputs ({sc_inner}*({sx}+{sw}-1)*({sy}+{sh}-1)")
        return (outputs+params+inputs)

    if branch1:
        tmp_branch1 = branch1[:currindex+1]
        tmp_branch2 = branch2[:currindex+1]
        tmp_mbranch = []
        assert len(branch1) == len(branch2)
        mindex = currindex + 1 - len(branch1)
        if mindex > 0:
            tmp_mbranch = mainbranch[:mindex]
        ## Branch 1
        currsizes = {"x": 1, "y": 1, "f": 1, "c": 1, "h": 1, "w": 1}
        for key,size,t in tmp_branch1:
            currsizes[key] *= size
        for key,size,t in tmp_mbranch:
            currsizes[key] *= size
        br1_footprint = footprint(currsizes,volScale)
        ## Branch 2
        currsizes = {"x": 1, "y": 1, "f": 1, "c": 1, "h": 1, "w": 1}
        for key,size,t in tmp_branch2:
            currsizes[key] *= size
        for key,size,t in tmp_mbranch:
            currsizes[key] *= size
        br2_footprint = footprint(currsizes,volScale)
        if mindex > 0: ## in main branch
            # this is mean because loops are serialized
            #FIXME: tested with volScale and divScale does not change figures.
            ## if footprint --> mean
            ## else volume --> no mean
            return [(br1_footprint+br2_footprint)//divScale]
        else: ## branch1 and 2 not merged
            return [br1_footprint,br2_footprint]
    else:
        tmp_mbranch = mainbranch[:currindex+1]
        currsizes = {"x": 1, "y": 1, "f": 1, "c": 1, "h": 1, "w": 1}
        for key,size,t in tmp_mbranch:
            currsizes[key] *= size
        return [footprint(currsizes,volScale)]

def parseScheme(scheme,dfmicro=None):
    lambdastring = "; (Lambda_apply y \[\(\(Iter [0-9]+\), \(Arg [0-9]+\)\); \(\(Iter [0-9]+\), \(Arg [0-9]+\)\)\])"
    tstring = " (T \([0-9]+, [xyhfcw]\))"
    ustring = " (U \([0-9]+, [xyhfcw]\))"
    vstring = "(V f)"
    vhoist =" (Hoist_vars) \[c\]" 
    acore = 0
    bcore = 0 
    loop1 = 0
    loop2 = 0
    schemesplit = re.sub("\]","",re.sub("\[","",scheme)).split(";")
    sizeL1y = 1
    ## list of tuples (dimension, value)
    parsed_list = []
    parsed_tree = None
    #ulambda_ind = 2
    pdebug("Scheme split",schemesplit)
    abcoeff = 1.0
    acore =1.
    bcore = 1.
    lambdaapp = False
    loopcnt = 0
    microkey1 = {'f':1,'y':1,'w':1,'h':1}
    microkey2 = {'f':1,'y':1,'w':1,'h':1}
    lambda_top = True
    passed_lambda=False
    for i,gr in enumerate(schemesplit):
        if " Hoist_vars c" in gr:
            continue
        if gr == " ULambda y": 
            lambdaapp =True
            ulambda_ind = len(parsed_list)
            continue
        if "Lambda_apply" in gr:
            passed_lambda = True
            gr = ";".join([gr,schemesplit[i+1]])
            reg = " Lambda_apply y \(\(Iter ([0-9]+)\), \(Arg ([0-9]+)\)\); \(\(Iter ([0-9]+)\), \(Arg ([0-9]+)\)\)" 
            m_sub = re.search(reg,gr)
            first_branch = parsed_list.copy()
            first_branch.insert(ulambda_ind, ("y", int(m_sub.group(2)),"T"))
            first_branch.append(("y", int(m_sub.group(1)), "T"))
            second_branch = parsed_list.copy()
            second_branch.insert(ulambda_ind, ("y", int(m_sub.group(4)), "T"))
            second_branch.append(("y", int(m_sub.group(3)), "T"))
            abcoeff = int(m_sub.group(1))/(int(m_sub.group(3))+int(m_sub.group(1)))
            acore *= float(m_sub.group(2))
            microkey1['y'] =int(m_sub.group(2))
            bcore *= float(m_sub.group(4))
            microkey2['y'] =int(m_sub.group(4))
            ## parsed_tree with main branch splitting into first and second
            parsed_tree = ([], first_branch, second_branch)
            continue
        if gr == "V f":
            acore *= vector_size
            bcore *= vector_size
            parsed_list.append(("f",vector_size, "V"))
            continue
        if "Iter" in gr:
            continue
        ## Only T or U left
        reg = "([A-Z]+) \(([0-9]+), ([a-z]+)\)"
        m_sub = re.search(reg,gr)
        if not m_sub:
            print(reg,gr)
            exit(-1)
        capletter = str(m_sub.group(1))
        value = float(m_sub.group(2))
        letter = str(m_sub.group(3))
        elt = (letter, value,capletter)
        if capletter == "U":
            microkey1[letter] = int(value)
            microkey2[letter] = int(value)
            acore *= value
            bcore *= value
        if capletter == "T" and loopcnt ==0:
            loop1 = value
        if capletter == "T" and loopcnt ==1:
            loop2 = value
        if capletter == "T":
            if passed_lambda:
                if value > 1:
                    lambda_top = False
            loopcnt += 1
        if parsed_tree:
            ## appending to tree main branch
            parsed_tree[0].append(elt)
        else:
            parsed_list.append(elt) 
    microperf1 = 0
    microperf2 = 0
    if args.microperf:
        my = microkey1['y']
        mf = microkey1['f']
        mh = microkey1['h']
        mw = microkey1['w']
        dfq = dfmicro.query(f'y == {my} and f == {mf}  and h == {mh} and w == {mw}')
        if dfq.perf.values: 
            v = dfq.perf.values[0]
            microperf1 = float(v)
        else:
            microperf1=-1.
    if not lambdaapp:
        abcoeff = 1.0
        bcore = 0.
        microperf2 = microperf1
    else:
        if args.microperf:
            my2 = microkey2['y']
            mf2 = microkey2['f']
            mh2 = microkey2['h']
            mw2 = microkey2['w']
            dfq2 = dfmicro.query(f'y == {my2} and f == {mf2}  and h == {mh2} and w == {mw2}')
            if dfq2.perf.values:
                v = dfq2.perf.values[0]
                microperf2 = float(v)
            else:
                microperf2 = -1.
    controlratio = (abcoeff,acore,bcore,loop1,loop2)
    return lambda_top,controlratio,parsed_tree,parsed_list,microperf1,microperf2

def findVolume(L1size,L2size):
    """ Reutnr volumes
    """
    # 2 levels: L1 and L2
    l1_footprint = 0
    l2_footprint = 0
    l1_footprint_br1 = 0
    l1_footprint_br2 = 0
    l2_footprint_br1 = 0
    l2_footprint_br2 = 0
    (l1_saturated,l2_saturated,l1_saturated_br1,l2_saturated_br1,l1_saturated_br2,l2_saturated_br2)=(False,False,False,False,False,False)
    l1_volume = 0
    l2_volume = 0
    computation = 0
    max_index = len(parsed_list)
    if parsed_tree:
        max_index =len(parsed_tree[0]) + len(parsed_tree[1])
    for currindex in range(max_index+1):
        pdebug("CURRINDEX",currindex)
        if parsed_tree:
            mbranch = parsed_tree[0]
            branch1 = parsed_tree[1]
            branch2 = parsed_tree[2]
            display(parsed_tree,currindex)
            lcurrent_footprint = computeFootprint(currindex, mbranch, branch1, branch2) 
            pdebug("lcurrent_footprint",lcurrent_footprint,np.mean(lcurrent_footprint))
            pdebug("L1size",L1size)
            pdebug("L2size",L2size)
            if currindex == len(parsed_list):
                lcurrent_iteration = [1]
            else:
                lcurrent_iteration = computeIterations(currindex+1, mbranch, branch1, branch2)
            pdebug("lcurrent_iteration",lcurrent_iteration,np.mean(lcurrent_iteration))
            #https://github.com/S12P/tvm_ttile/blob/bench_multi_core/ttile/bench/bench_multi_core_avx512/README.md# L1 volume 
            if not l1_saturated_br1:
                pdebug("l1_footprint_br1",l1_footprint_br1)
                l1_footprint_br1 = computeFootprint(currindex, mbranch, branch1, branch2, computeVolume=True)[0]
                pdebug("l1_footprint_br1_for_volume (output*2)",l1_footprint_br1)
                l1_iteration_br1 = lcurrent_iteration[0]
            if lcurrent_footprint[0] > L1size: l1_saturated_br1=True
            if len(lcurrent_footprint)>1 and not l1_saturated_br2: 
                pdebug("l1_footprint_br2",l1_footprint_br2)
                l1_footprint_br2 = computeFootprint(currindex, mbranch, branch1, branch2, computeVolume=True)[1]
                pdebug("l1_footprint_br2_for_volume (output *2)",l1_footprint_br2)
                ## if currindex is at LambdaApply: only main branch for iteration count.
                l1_iteration_br2 = lcurrent_iteration[1] if len(lcurrent_iteration)>1 else lcurrent_iteration[0]
            if len(lcurrent_footprint)>1 and lcurrent_footprint[1] > L1size: l1_saturated_br2=True
            l1_volume = (l1_footprint_br1*l1_iteration_br1+l1_footprint_br2*l1_iteration_br2)
            pdebug("Current volume L1",l1_volume)
            
            ## L2 volume 
            if not l2_saturated_br1:
                pdebug("l2_footprint_br1",l2_footprint_br1)
                l2_footprint_br1 = computeFootprint(currindex, mbranch, branch1, branch2, computeVolume=True)[0]
                pdebug("l2_footprint_br1_for_volume (output*2)",l2_footprint_br1)
                l2_iteration_br1 = lcurrent_iteration[0]
            if lcurrent_footprint[0] > L2size: l2_saturated_br1=True
            if len(lcurrent_footprint)>1 and not l2_saturated_br2: 
                pdebug("l2_footprint_br2",l2_footprint_br2)
                l2_footprint_br2 = computeFootprint(currindex, mbranch, branch1, branch2, computeVolume=True)[1]
                pdebug("l2_footprint_br2_for_volume (output *2)",l2_footprint_br2)
                ## if currindex is at LambdaApply: only main branch for iteration count.
                l2_iteration_br2 = lcurrent_iteration[1] if len(lcurrent_iteration)>1 else lcurrent_iteration[0]
            if len(lcurrent_footprint)>1 and lcurrent_footprint[1] > L2size: l2_saturated_br2=True
            pdebug("l2_iteration_br1 for volume",l1_iteration_br1)
            pdebug("l2_iteration_br2 for volume",l2_iteration_br2)
            l2_volume = (l2_footprint_br1*l2_iteration_br1+l2_footprint_br2*l2_iteration_br2)
            pdebug("Current volume L2",l2_volume)
        else:## one branch only
            mbranch = parsed_list
            lcurrent_footprint = computeFootprint(currindex, mbranch) 
            pdebug("footprintCouple",lcurrent_footprint )
            if currindex == len(parsed_list):
                lcurrent_iteration = [1]
            else:
                lcurrent_iteration = computeIterations(currindex+1, mbranch)
            pdebug("iterCouple",lcurrent_iteration )
            if not l1_saturated:
                l1_footprint = computeFootprint(currindex, mbranch, computeVolume=True)[0]
                l1_volume = l1_footprint*lcurrent_iteration[0]
                pdebug("Current volume L1",l1_volume)
            if lcurrent_footprint[0] > L1size: l1_saturated=True
            if not l2_saturated:
                l2_footprint = computeFootprint(currindex, mbranch, computeVolume=True)[0]
                l2_volume = l2_footprint*lcurrent_iteration[0]
                pdebug("Current volume L2",l2_volume)
            if lcurrent_footprint[0] > L2size: l2_saturated=True
    return l1_volume,l2_volume

## compute intensity is computed as
## loop size weighted by coeff
## T x X T y Y T z Z --> ( (X+5) * Y + 5) * Z + 5 )
controlCoeff = 5
dfconv = pd.read_csv(f"{args.root_dir}/common_csv/conv_sizes.csv")
with open(file2write,"w") as ouf:
    l1vol = ",".join([f"L1vol_{x}" for x in range(len(lL1size))])
    l2vol = ",".join([f"L2vol_{x}" for x in range(len(lL1size))])
    l1s = ",".join([f"L1size_{x}" for x in range(len(lL1size))])
    l2s = ",".join([f"L2size_{x}" for x in range(len(lL1size))])
    ouf.write(f"fname,benchname,scheme,lambda,computationWithControlCoeff,abcoeff,acore,bcore,loop1,loop2,perf,perfmicro1,perfmicro2,L1miss,L2miss,L1volumeorigin,L2volumeorigin,{l1s},{l2s},{l1vol},{l2vol},computation,lambdatop\n")

dfmicro = None
if args.microperf:
    dfmicro = pd.read_csv("perf_micro_kernel.csv")
for fname in fallnames:
    csize_name = get_bench_name(args.archi,os.path.basename(fname),args.suffix)
    data = pd.read_csv(fname)
    v = dfconv.loc[dfconv['name'] == csize_name, 'stride']
    stride = int(v)
    schemes = data["scheme"]
    lperf             = data["perf"].values
    ll2_volume_origin = []
    ll1_volume_origin = []
    ll1miss           = []
    ll2miss           = []
    if "L1volume" in data:
        ll2_volume_origin = data["L2volume"].values
        ll1_volume_origin = data["L1volume"].values
    if "L1miss" in data:
        ll1miss           = data["L1miss"].values
        ll2miss           = data["L2miss"].values
    for i,scheme in enumerate(schemes):
        l2_volume_origin = 0
        l1_volume_origin = 0
        if ll2_volume_origin:
            l2_volume_origin = ll2_volume_origin[i]
            l1_volume_origin = ll1_volume_origin[i]
        perf             = lperf[i]
        l1miss           = 0
        l2miss           = 0
        if ll1miss:
            l1miss           = ll1miss[i]
            l2miss           = ll2miss[i]
        ## parse scheme
        lambdatop,controlratio,parsed_tree,parsed_list,perfmicro1,perfmicro2 = parseScheme(scheme,dfmicro)
        abcoeff,acore,bcore,loop1,loop2 = controlratio

        ll1volume = []
        ll2volume = []
        for L1size,L2size in zip(lL1size,lL2size):
            l1_volume,l2_volume = findVolume(L1size,L2size)
            ll1volume.append(l1_volume)
            ll2volume.append(l2_volume)
        ll1_volume_str = ",".join([str(x) for x in ll1volume])
        ll2_volume_str = ",".join([str(x) for x in ll2volume])
        ll1_size_str = ",".join([str(x) for x in lL1size])
        ll2_size_str = ",".join([str(x) for x in lL2size])

        if parsed_tree:
            #FIXME: computation is sum of 2 branches computation
            computationWithControlCoeff = sum(computeIterations(0,parsed_tree[0],parsed_tree[1],parsed_tree[2],controlCoeff))
            computation = sum(computeIterations(0,parsed_tree[0],parsed_tree[1],parsed_tree[2]))
        else:
            computationWithControlCoeff = computeIterations(0,parsed_list,control_coeff=controlCoeff)[0]
            computation = computeIterations(0,parsed_list)[0]
        lambdaBool = False
        if "Lambda" in scheme:
            lambdaBool =True
        with open(file2write,"a") as ouf:
            ouf.write(fname+","
                + csize_name+",\""
                + scheme+"\",\""
                +str(lambdaBool)+"\","
                +str(computationWithControlCoeff)+","
                +str(abcoeff)+","
                +str(acore)+","
                +str(bcore)+","
                +str(loop1)+","
                +str(loop2)+","
                +str(perf)+","
                +str(perfmicro1)+","
                +str(perfmicro2)+","
                +str(l1miss)+","
                +str(l2miss)+","
                +str(l1_volume_origin)+","
                +str(l2_volume_origin)+","
                +ll1_size_str+","
                +ll2_size_str+","
                +ll1_volume_str+","
                +ll2_volume_str+","
                +str(computation)+","
                +str(lambdatop)+"\n")
        ## for debug
        #print(scheme+"\","+ str(l1_volume)+","+str(l2_volume)+","+str(oL1vol)+","+str(oL2vol))


#!/usr/bin/env python3
import pandas as pd
import argcomplete, argparse
import numpy as np
import os

from reformat import reformat_and_merge

archiavx512_motoko = 'avx512Gold6130'
archiavx512_tobast ='avx512Silver4114'
archiavx2 = 'avx2NehlemE31240'
notobast = 'notobast'
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('rootdir', help="Precise absolute path for root dir of ics-experiments.")
parser.add_argument('--archi', type=str, help='Which archi to plot', choices=('all',archiavx512_motoko,archiavx512_tobast, archiavx2, notobast), default=archiavx512_motoko)
parser.add_argument('--onednntime', help="OneDNN has been measure with gettimeofday (this option) and PapiCycles counter (default).", action='store_true', default=False)
parser.add_argument('--onednn2layouts', help="Display both layouts: w and k last (w last only by default)", action='store_true', default=False)
parser.add_argument('--debug', help="More verbose", action='store_true', default=False)
argcomplete.autocomplete(parser)
args = parser.parse_args()

d = pd.read_csv(args.rootdir+"/common_csv/conv_sizes.csv")

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    if args.debug:
        print(freq)
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)

def genPlotComparison(outfname,dfttile_avx2,opts_avx2,dfttile_gold,opts_gold):

    dfttile_avx2 = dfttile_avx2.loc[dfttile_avx2['NameConv'] != "ttile"].copy()
    dfttile_avx2 = dfttile_avx2.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    dfttile_gold = dfttile_gold.loc[dfttile_gold['NameConv'] != "ttile"].copy()
    dfttile_gold = dfttile_gold.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    l = [(dfttile_avx2,opts_avx2),(dfttile_gold,opts_gold)]
    for df,cple in l:
        core,fma,vectorsize,freq = cple
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl
        df = df[['NameConv','perPP','std']].copy
    
    import plotly.graph_objects as go
    
    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=dfttile_gold['NameConv'].values,
        error_y=dict(type='data', array=dfttile_gold['std'].values),
        y=dfttile_gold['perPP'].values,
        name='TVM+TTILE gold',
        marker_color='green'
    ))

    fig.add_trace(go.Bar(
        x=dfttile_avx2['NameConv'].values,
        error_y=dict(type='data', array=dfttile_avx2['std'].values),
        y=dfttile_avx2['perPP'].values,
        name='TVM+TTILE avx2',
        marker_color='indianred'
    ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45,xaxis_type = 'category')
    fig.update_layout(height=600, width=1200)
    fig.update_yaxes(title="% of peak performance")
    fig.write_image(f'{outfname}.png')

def genPlot(outfname,dfttile,dfautotvm,dfonednn,core,fma,vectorsize,freq,debug=False):
    """
    """
    ## per peak performance list for one archi
    ## handle ttile CSV (multiple values)
    dfttile = dfttile.loc[dfttile['NameConv'] != "ttile"].copy()
    dfttile = dfttile.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    l = [dfttile,dfautotvm]
    if args.onednntime:
        dfonednn_wlast = dfonednn[['bench','perf_xlast','std_dev_xlast']].rename(columns={"bench": "NameConv", "perf_xlast": "Time(ms)", "std_dev_xlast" : "std"}).copy() 
        l += [dfonednn_wlast]

    for i,df in enumerate(l):
        #if i ==1:
        #    core = 1 
        if debug:
            print("DEBUG",df)
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl
        df = df[['NameConv','perPP','std']].copy
    if not args.onednntime:
        dfonednn_wlast = dfonednn[['bench','perf_xlast','std_dev_xlast']].rename(columns={"bench": "NameConv", "perf_xlast": "perPP", "std_dev_xlast" : "std"}).copy() 
        ## not in the final plot
        dfonednn_klast = dfonednn[['bench','perf_flast','std_dev_flast']].rename(columns={"bench": "NameConv", "perf_flast": "perPP", "std_dev_flast" : "std"}).copy() 
    #concat = [dfttile[['NameConv','perPP','std']],dfautotvm[['NameConv','perPP','std']],dfonednn_wlast[['NameConv','perPP','std']]]
    
    import plotly.graph_objects as go
    
    fig = go.Figure()
    #df = pd.concat(concat)

    ttile_mean = np.mean(dfttile['perPP'].values)
    dttile_mean = {'perPP':ttile_mean,'NameConv':"_Mean",'std': 0}
    dttile_yolo23 = {'perPP': 0,'NameConv':"Yolo9000_23",'std': 0}
    dfttile.loc[len(dfttile)] = dttile_yolo23
    dfttile.loc[len(dfttile)] = dttile_mean

    autotvm_mean = np.mean(dfautotvm['perPP'].values)
    dautotvm_mean = {'perPP':autotvm_mean,'NameConv':"_Mean",'std': 0}
    dfautotvm.loc[len(dfautotvm)] = dautotvm_mean

    onednn_mean = np.mean(dfonednn_wlast['perPP'].values)
    donednn_mean = {'perPP':onednn_mean,'NameConv':"_Mean",'std': 0}
    dfonednn_wlast.loc[len(dfonednn_wlast)] = donednn_mean

    if args.debug:
        print("ttile")
        print(np.mean(dfttile['perPP'].values))
        print("tvm")
        print(np.mean(dfautotvm['perPP'].values))
        print("ondnn")
        print(np.mean(dfonednn_wlast['perPP'].values))
    fig.add_trace(go.Bar(
        x=dfttile['NameConv'].values,
        error_y=dict(type='data', array=dfttile['std'].values),
        y=dfttile['perPP'].values,
        name='TVM+TTILE',
        marker_color='indianred'
    ))
    
    fig.add_trace(go.Bar(
        x=dfautotvm['NameConv'].values,
        y=dfautotvm['perPP'].values,
        error_y=dict(type='data', array=dfautotvm['std'].values),
        name='AutoTVM',
        marker_color='lightsalmon'
    ))

    if args.onednn2layouts:
        fig.add_trace(go.Bar(
            x=dfonednn_klast['NameConv'].values,
            y=dfonednn_klast['perPP'].values,
            ## Standard Deviation as been normalized by TTile already (it is a %)
            error_y=dict(type='data', array=dfonednn_klast['std'].values*dfonednn_klast['perPP'].values/100.),
            name='onednn_clast',
            marker_color='brown'
        ))

    fig.add_trace(go.Bar(
        x=dfonednn_wlast['NameConv'].values,#+["geomean"],
        y=dfonednn_wlast['perPP'].values,#+[],
        ## Standard Deviation as been normalized by TTile already (it is a %)
        error_y=dict(type='data', array=dfonednn_wlast['std'].values*dfonednn_wlast['perPP'].values/100.),
        name='onednn_wlast',
        marker_color='green'
    ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45,xaxis_type = 'category')
    fig.update_layout(height=600, width=1200)
    fig.update_yaxes(title="% of peak performance")
    fig.write_image(f'{outfname}.png')

## AVX512 Gold
if args.archi == 'all' or args.archi == archiavx512_motoko or args.archi == notobast:
    ttile_datadir = args.rootdir+"/perf_schemes/avx512/TVMTTILE/XeonGold/XeonGoldMetricParTvmTtile/results/"
    dfautotvm = pd.read_csv(args.rootdir+"/perf_ref/autotvm/multicore/avx512/data-avx512-grid500-autotvm/result_withenvvar/result.csv")
    reformat_and_merge(ttile_datadir,True)
    dfttile = pd.read_csv(ttile_datadir + "/formatted/fulldata.csv")
    if args.onednntime:
        dfonednn = pd.read_csv(args.rootdir+"/perf_ref/onednn/par/avx512/onednn-eval-grid5000-Gold6130/results_32threads_normalized_time.csv")
    else:
        dfonednn = pd.read_csv(args.rootdir+"/perf_ref/onednn/par/avx512/onednn-eval-grid5000-Gold6130/results_32threads_normalized.csv")
    #genPlot("XeonGold-avx512",dfttile,dfautotvm,dfonednn,core=32,fma=2,vectorsize=16,freq=2100000000)

## AVX512 frequence ...
if args.archi == 'all' or args.archi == archiavx512_tobast:
    dfautotvm = pd.read_csv(args.rootdir+"/perf_ref/autotvm/multicore/avx512/silver4114/cold_cache/result.csv")
    dfttile = pd.read_csv(args.rootdir+"/perf_schemes/avx512/TVMTTILE/AVX512Silver/results/formatted/fulldata.csv")
    dfonednn = pd.read_csv(args.rootdir+"perf_ref/onednn/par/avx512/onednn-results-tobastavx512-parallel/results-flush-normalized-20cores-removed-5.csv")
    genPlot("Xeon-avx512-tobast",dfttile,dfautotvm,dfonednn,core=20,fma=1,vectorsize=16,freq=2200000000)
#
### AVX2
if args.archi == 'all' or args.archi == archiavx2 or args.archi == notobast:
    dfonednn = pd.read_csv(args.rootdir+"/perf_ref/onednn/par/avx2/Nehalem-E3-1240/results-4thread-normalized-stddev.csv")
    #dfautotvm = pd.read_csv(args.rootdir+"perf_ref/autotvm/1core/avx2/E3-1240-timelog/1core-INTEL-E3-1240.csv")#"/perf_ref/autotvm/multicore/avx2/E3-1240-notimelog/result/result_with_cache_flush.csv")
    dfautotvm = pd.read_csv(args.rootdir+"/perf_ref/autotvm/multicore/avx2/E3-1240-notimelog/result/result_with_cache_flush.csv")
    ttile_datadir = args.rootdir + "/perf_schemes/avx2/TVMTTILE/AVX2/results/"
    reformat_and_merge(ttile_datadir,True)
    dfttile = pd.read_csv(ttile_datadir+"/formatted/fulldata.csv")
    genPlot("Xeon-avx2-tobast",dfttile,dfautotvm,dfonednn,core=4,fma=2,vectorsize=8,freq=3300000000,debug=args.debug)
#genPlot("XeonGold-avx512",dfttile,dfautotvm,dfonednn,core=32,fma=2,vectorsize=16,freq=2100000000)

#ttile_datadir = args.rootdir+"/perf_schemes/avx512/TVMTTILE/XeonGold/XeonGoldMetricParTvmTtile/results/"
#reformat_and_merge(ttile_datadir,True)
#dfttile_gold = pd.read_csv(ttile_datadir + "/formatted/fulldata.csv")
#ttile_datadir = args.rootdir + "/perf_schemes/avx2/TVMTTILE/AVX2/results/"
#reformat_and_merge(ttile_datadir,True)
#dfttile_avx2 = pd.read_csv(ttile_datadir+"/formatted/fulldata.csv")
#dfautotvm = pd.read_csv(args.rootdir+"perf_ref/autotvm/1core/avx2/E3-1240-timelog/1core-INTEL-E3-1240.csv")#"/perf_ref/autotvm/multicore/avx2/E3-1240-notimelog/result/result_with_cache_flush.csv")
#dfautotvm = pd.read_csv(args.rootdir+"/perf_ref/autotvm/multicore/avx2/E3-1240-notimelog/result/result_with_cache_flush.csv")
#genPlotComparison("JustTtile",dfttile_avx2,(4,2,8,3000000000),dfttile_gold,(32,2,16,2100000000))

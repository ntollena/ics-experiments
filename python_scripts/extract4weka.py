#!/usr/bin/env python3

from ParseArguments import parseArgsForWeka
import pandas as pd

args = parseArgsForWeka()
df0 = pd.read_csv(args.csvfile)

tmplist = []
for i in range(args.nbL1L2):
    tmplist += [f"L1size_{i}", f"L2size_{i}", f"L1vol_{i}", f"L2vol_{i}"]

df = df0[
    ["benchname", "perf", "computation", "lambda", "loop1", "loop2", "L1miss", "L2miss"]
    + tmplist
]
for i in range(args.nbL1L2):
    df.insert(
        1,
        f"OI_{i}",
        [
            x / (2 * y + z)
            for x, y, z in zip(
                df[f"computation"].values,
                df[f"L1vol_{i}"].values,
                df[f"L2vol_{i}"].values,
            )
        ],
    )

df.to_csv(args.newcsvfile, index=False)

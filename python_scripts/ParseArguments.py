import argcomplete, argparse


def parseArgsForWeka():
    parser = argparse.ArgumentParser(
        description="Generate 1 csv from one per bench. 300sec on my laptop (Hugo)."
    )
    parser.add_argument("csvfile", help="csv file to convert")
    parser.add_argument(
        "newcsvfile", help="csv file to dump new format good for weka study"
    )
    parser.add_argument(
        "nbL1L2",
        help="Number of OI we want to compute (number of couple L1L2 used to generate input csv)",
        type=int,
    )
    argcomplete.autocomplete(parser)
    return parser.parse_args()


def parseArgumentForAutosched():
    parser = argparse.ArgumentParser(
        description="""
        Generates csv file to contain best % of peak performance from log files.
        In practice, for now, this script only concatenate the different csv from
        log files. These csv have been generated with the treat_data.py script
        inside the autoscheduler_log subdirectory.
        """
    )
    parser.add_argument(
        "ics_home",
        help="Absolute path to the home of ics-experiments directory.",
        required=True,
    )
    parser.add_argument(
        "outputsubdir",
        help="output subdirectory relative path.",
        default="./paper_versions/asplos/bothGold5220_Gold6130/",
    )
    parser.add_argument(
        "outputname",
        help="output file base name (without the .csv)",
        default="results_all_autoscheduler_fromlogs",
    )
    parser.add_argument(
        "convsizes",
        help="path to conv size csv (ics-experiments/common_csv/conv_sizes.csv)",
    )
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    return args

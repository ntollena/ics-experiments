#!/usr/bin/env python3

## Perf against L1+L2 miss and volume with just one csv
##
##
import argcomplete, argparse
import math
import numpy as np
import os
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly
import plotly.express as px

parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('--metric', type=str, help='chose color metric from', choices=('incfilter', 'crossfilter', 'OI', 'OIbest', 'controlloop', 'controlkernel', 'controlbest', 'nolambda', 'lambdabest', 'L1volume', 'L2volume', 'L1miss' , 'L2miss'), default='incfilter')
parser.add_argument('--outputformat', type=str, help='select one or more output formats', choices=('svg','html','png'), action='append')
parser.add_argument('--kernelcontrol', action='store_true')
parser.add_argument('--csv', type=str, default='./test.csv')
parser.add_argument('--bandwidth', type=float, default=2, help='L1 bandwidth assuming L2 is 1')

argcomplete.autocomplete(parser)
args = parser.parse_args()

bandwidth={'L1':args.bandwidth, 'L2':1}

output=args.outputformat
if not output: output=['svg']


if args.kernelcontrol: control='microkernel'
else: control='loop1'
color=args.metric

file2read = args.csv
df2 = pd.read_csv(file2read)


layout = go.Layout(yaxis={'tickformat':'.2e', 'rangemode': 'tozero',
           'ticks': 'outside'})

print('Fields: ',list(df2.columns), sep='')
def genFigures(benchname_list):
    def best(df, filters, verbose=True):
        tmpdf=df.assign(tmp=True)
        if verbose: print(f'filter {filters}',end='\t')
        for f in filters:
            field=f['field'];
            length=tmpdf.tmp.sum()
            if 'num' not in f and 'ratio' in f: num=int(length*f['ratio'])
            elif 'num' in f and 'ratio' not in f: num=f['num']
            elif 'num' in f and 'ratio' in f: num=max( f['num'], int(length*f['ratio']))
            else: num=int(length*0.2)
            threshold=int(min(tmpdf.nlargest(num, ['tmp', field])[field]))
            tmpdf.tmp=tmpdf.tmp & (tmpdf[field]>=threshold)
            if verbose: print('num:{}/{}'.format(tmpdf.tmp.sum(), len(df[field])),f'threshold:{threshold}', end='\t')
        return tmpdf.tmp
        
    ltrueres = []##no lambda OR lambdatop
    lfalseres = []## Ref max
    lperfref = []
    for benchname in benchname_list:
        df = df2.loc[df2['benchname'] == benchname].copy()
        lperfref.append(max(list(df.perf)))
        #print(benchname)
        yeslambda = list(df.loc[df["lambda"] == True].perf)
        lnolambda = list(df.loc[df["lambda"] == False].perf)
        lambdatop = 0
        nolambda = 0
        if yeslambda:
            llambdatop =list(df.loc[df["lambdatop"] == True].perf)
            if llambdatop:
                lambdatop = max(llambdatop)
        if lnolambda:
            nolambda = max(lnolambda)
        ltrueres.append(max(lambdatop,nolambda))
            #print(benchname,"nolambda",max(l2))
    print("Perf ref")
    print(lperfref) 
    print("NO lambda OR lambda TOP")
    print(ltrueres) 
    print("Ratio")
    print(["{:.2f}".format(x/y) for x,y in zip(ltrueres,lperfref)])

benchs=list(set(df2['benchname']))
benchs.sort()
genFigures(benchs)

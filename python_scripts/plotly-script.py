#!/usr/bin/env python3

## Perf against L1+L2 miss and volume with just one csv
##
##
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly
import plotly.express as px
import math
import numpy as np
import argcomplete, argparse
import os

# If autocompletion does not work:
#  activate-global-python-argcomplete --user
#  eval "$(register-python-argcomplete ./plotly-script.py)"

min_oi_best = 40##400
min_nolambda_best = 100#1000
min_control_best = 20#200
oi_ratio=0.6
control_ratio=0.6

parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('--metriccolor', type=str, help='metric for color', choices=('version','lambdatop','weka','incfilter', 'crossfilter', 'OI', 'OIbest', 'controlloop', 'controlkernel', 'controlbest', 'nolambda', 'lambdabest', 'L1volume', 'L2volume', 'L1miss' , 'L2miss', 'kernelperf', 'kernelroofline'), default='weka')
parser.add_argument('--outputformat', type=str, help='select one or more output formats', choices=('svg','html','png'), action='append')
parser.add_argument('--subdir', type=str, help='path for output default is ./')
parser.add_argument('--metricxaxis', type=str, help='metric for x axis', choices=('weka','OI', 'controlloop', 'controlkernel', 'L1volume', 'L2volume', 'L1miss', 'L2miss', 'kernelperf'), default='weka')
parser.add_argument('--kernelcontrol', action='store_true')
parser.add_argument('--csv', type=str, default='./fulldata.csv')
parser.add_argument('--bandwidth', type=float, default=2, help='L1 bandwidth assuming L2 is 1')

argcomplete.autocomplete(parser)
args = parser.parse_args()

bandwidth={'L1':args.bandwidth, 'L2':1}

xaxis=args.metricxaxis 

outputformat=args.outputformat
subdir=args.subdir
if not outputformat and subdir:
    for f in ['png', 'svg', 'html']:
        if subdir.find(f)!=-1: outputformat=[f]
if not outputformat: outputformat=['svg']
if not subdir:
    for f in outputformat:
        if not os.path.exists(f'{f}s'): os.makedirs(f'{f}s')
elif not os.path.exists(subdir): os.makedirs(subdir)

def filename(outformat, name):
    if subdir: return f'{subdir}/{name}.{outformat}'
    else: return f'{outformat}s/{name}.{outformat}'
        
if args.kernelcontrol: control='microkernel'
else: control='loop1'
color=args.metriccolor

file2read = args.csv
df2 = pd.read_csv(file2read)


layout = go.Layout(yaxis={'tickformat':'.2e', 'rangemode': 'tozero',
           'ticks': 'outside'})

print('Fields: ',list(df2.columns), sep='')
def genFigures(benchname_list):
    def best(df, filters, verbose=True):
        """ filters is a list of dictionnary
        """
        tmpdf=df.assign(tmp=True)
        if verbose: print(f'filter {filters}',end='\t')
        for f in filters:
            field=f['field'];
            length=tmpdf.tmp.sum()
            if 'num' not in f and 'ratio' in f: num=int(length*f['ratio'])
            elif 'num' in f and 'ratio' not in f: num=f['num']
            elif 'num' in f and 'ratio' in f: num=max( f['num'], int(length*f['ratio']))
            else: num=int(length*0.2)
            threshold=int(min(tmpdf.nlargest(num, ['tmp', field])[field]))
            tmpdf.tmp=tmpdf.tmp & (tmpdf[field]>=threshold)
            if verbose: print('num:{}/{}'.format(tmpdf.tmp.sum(), len(df[field])),f'threshold:{threshold}', end='\t')
        return tmpdf.tmp
        
    for benchname in benchname_list:
        for f in outputformat: print('\t', filename(f, f'threshold-{benchname}'), end=' ')

        df = df2.loc[df2['benchname'] == benchname].copy()
        df['OI'] = df['computation']/ (df["L1vol_0"]/bandwidth['L1']+df["L2vol_0"]/bandwidth['L2'])
        if color in ['OIbest', 'crossfilter']:
            df['OIbest'] = best(df, [{'field':'OI', 'ratio':oi_ratio, 'num':min_oi_best}])


        df['abcoeff2']=df.abcoeff*df.acore/(df.abcoeff*df.acore+(1-df.abcoeff)*df.bcore)
        df['kernelperf']=df.perfmicro1*df.abcoeff2+df.perfmicro2*(1-df.abcoeff2)
        if max(df.kernelperf>100): print(df[df.abcoeff>1]['scheme'])
        df['kernelroofline']=df['kernelperf']<df['perf']
                                    
        
        df['controlloop']=df.loop1
        ## V f ; U f ; U x ; U y1 ; T c ; T x; T y1;
        ## V f ; U f ; U x ; U y2 ; T c ; T x; T y2;
        ## abcoeff = T y1 / (T y1 + T y2)
        ## Tc * (Uy1 * 0.4 + Uy2* 0.6)
        df['controlkernel']=df['control']=df.loop1*(df.acore*df.abcoeff+df.bcore*(1-df.abcoeff))
        if control=='loop1': df['control']=df.controlloop
        elif control=='microkernel': df['control']=df.controlkernel
        if color in ['controlbest','crossfilter']:
            df['controlbest']= best(df, [{'field':'control', 'ratio':control_ratio, 'num': min_control_best}])

        df['nolambda']=df['lambda']==False
        if color in ['lambdabest','crossfilter']:
            df['lambdabest']=best(df, [{'field':'nolambda', 'num':min_nolambda_best}])

        if color=='crossfilter':
            df[color]=df.nolambda*1+df.controlbest*1+df.OIbest*1

        if color=='incfilter':
            df[color]=best(df,  [{'field':'nolambda', 'num':min_nolambda_best}, {'field':'control', 'ratio':control_ratio, 'num': min_control_best}, {'field':'OI', 'ratio': oi_ratio, 'num':min_oi_best}])
        if color == 'weka':
            xpnb = 1
            l0 = df["benchname"].values
            l1 = to_int(df[f"OI_{xpnb}"].values)
            l2 = to_int(df[f"L1vol_{xpnb}"].values)
            l3 = to_int(df[f"L2vol_{xpnb}"].values)
            l4 = to_int(df[f"loop1"].values)
            l5 = to_int(df["loop2"].values)
            #df[color] = [weka(name,oi,L1vol_0,L2vol_0,loop1,loop2) for name,oi,L1vol_0,L2vol_0,loop1,loop2 in zip()]
            df[color] = [weka(name,oi,voll1,voll1,loop1,loop2) for oi,voll1,voll2,loop1,loop2 in zip(l1,l2,l3,l4,l5)]
        
        df['L2ratio']=df['L2miss']/df['L2vol_0']

        df=df.sort_values(ascending=False,by=[color])
        title = f"{benchname}"
        fig = px.scatter(df,x=xaxis, y='perf',  title=title, color=color, log_x=True, log_y=False, hover_data=['scheme'])

        fig.update_yaxes( tickformat='.2e')
        fig.update_layout(height=800, width=1200)
        
        if 'png' in outputformat:
            fig.write_image(filename('png', f'threshold-{benchname}'))
        if 'svg' in outputformat:
            fig.write_image(filename('svg', f'threshold-{benchname}'))
        if 'html' in outputformat:
            fig.write_html(filename('html', f'threshold-{benchname}'))


        print()

benchs=list(set(df2['benchname']))
benchs.sort()
genFigures(benchs)

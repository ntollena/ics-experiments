import plotly.express as px
import plotly.graph_objects as go
import pandas as pd

dfno = pd.read_csv("xp_unroll.csv")
dfyes = pd.read_csv("xp_unrollTrue.csv")
dfyeso2 = pd.read_csv("o2_unroll_true.csv")
dfnoo2 = pd.read_csv("o2_nounroll.csv")
for df in [dfyes,dfno]:
    df["id"] = df['i'].astype(str) + " " + df["j"].astype(str) + " " + df["k"].astype(str)

df = dfyes.merge(dfno).merge(dfyeso2).merge(dfnoo2)
df = df.sort_values(by=['i','j','k'], ascending=True).copy()
print(df)
name="Title"
fig = go.Figure()
fig.add_trace(go.Scatter(
    x=df["id"],
    y=df["Unroll"],name="UnrollO3",
     mode='markers'
    ))
fig.add_trace(go.Scatter(
    x=df["id"],
    y=df["UnrollO2"],name="UnrollO2",
     mode='markers'
    ))
fig.add_trace(go.Scatter(
    x=df["id"],
    y=df["NoUnroll"],name="NoUnroll",
     mode='markers'
    ))
fig.add_trace(go.Scatter(
    x=df["id"],
    y=df["NoUnrollO2"],name="NoUnrollO2",
     mode='markers'
    ))
fig.update_yaxes(title="% peak perf")
fig.update_xaxes(title="ID= i j k")
fig.update_layout(width=1600, height=800,title=f"AVX2 compiled with GCC, matmul_bench, microK matrix multiply.")
fig.write_html("UnrollComparison.html")
fig.write_image("UnrollComparison.png")


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# List files
#lYolo_num = [0, 2, 4, 5, 8, 9 ,12, 13, 18]
lYolo_num = [4]

def get_filename(yolonum):
    return "yolo9000_{}.csv".format(str(yolonum))

def get_figuretitle(yolonum):
    return "Yolo9000-{} perf vs reduction size".format(str(yolonum))


def perf_vs_reduction():
    fig = plt.figure(1)
    for i in range(0, len(lYolo_num)):
        yolonum = lYolo_num[i]
        
        file2read = get_filename(yolonum)
        titlefig = get_figuretitle(yolonum)

        # Extract the data from the csv
        df = pd.read_csv(file2read)

        c_red_size = [int(x) for x in list(df['c_red_size'])]
        perf = list(df["perf"])


        # Show
        ax = plt.subplot(3,3, (i+1))

        ax.set_xlabel("c_red_size")
        ax.set_ylabel("perf")

        plt.scatter(c_red_size,perf)

        ax.set_title(titlefig)
        

    fig.tight_layout()
    plt.show()


def perf_vs_occup_L1_redfixed():
    fig = plt.figure(1)
    for i in range(0, len(lYolo_num)):
        yolonum = lYolo_num[i]
        
        file2read = get_filename(yolonum)
        titlefig = get_figuretitle(yolonum)

        # Extract the data from the csv
        df = pd.read_csv(file2read)

        c_red_size = [int(x) for x in list(df['c_red_size'])]
        occ_l1_min = list(df["l1_min"])
        occ_l1_max = list(df["l1_max"])
        perf = list(df["perf"])

        # Occupancy L1 is average of min and max
        occ_l1 = []
        for k in range(0, len(occ_l1_min)):
            occ_l1.append((occ_l1_min[k] + occ_l1_max[k]) / 2.0)

        # Separate data according to c_red_size
        perf_sep = {}
        occ_sep = {}
        for k in range(0, len(perf)):
            cred = c_red_size[k]
            if cred in perf_sep:
                perf_sep[cred].append(perf[k])
                occ_sep[cred].append(occ_l1[k])
            else:
                perf_sep[cred] = [ perf[k] ]
                occ_sep[cred] = [ occ_l1[k] ]
        
        # Show
        #ax = plt.subplot(3,3, (i+1))
        ax = plt.subplot(1,1, (i+1))

        ax.set_xlabel("occ_l1")
        ax.set_ylabel("perf")

        for cred in perf_sep:
            lab = "cred = " + str(cred)
            plt.scatter(occ_sep[cred], perf_sep[cred], label=lab)

        ax.legend()
        ax.set_title(titlefig)
        

    fig.tight_layout()
    plt.show()



perf_vs_occup_L1_redfixed()


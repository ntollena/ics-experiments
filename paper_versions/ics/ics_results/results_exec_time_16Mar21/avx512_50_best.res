Name : yolo9000_0
perf : 28.271499
tile : [V f; U (1, f); ULambda y; U (3, h); T (3, c); Hoist_vars [c]; T (34, x);
  T (3, w); T (1, y); T (2, f); T (8, y); T (16, x);
  Lambda_apply y [((Iter 2), (Arg 13)); ((Iter 3), (Arg 14))]]

Name : yolo9000_2
perf : 79.348042
tile : [V f; U (2, f); U (8, y); T (32, c); Hoist_vars [c]; T (1, x); T (3, w);
  T (3, h); T (1, x); T (34, y); T (2, f); T (1, c); T (1, x); T (272, x)]

Name : Yolo9000_4
perf : 88.869651
tile : [V f; U (2, f); ULambda y; U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (1, x);
  Lambda_apply y [((Iter 9), (Arg 9)); ((Iter 5), (Arg 11))]; T (4, f);
  T (1, c); T (136, x)]

Name : yolo9000_5
perf : 68.837294
tile : [V f; U (2, f); ULambda y; T (128, c); Hoist_vars [c]; T (2, x); T (4, x);
  Lambda_apply y [((Iter 7), (Arg 13)); ((Iter 3), (Arg 15))]; T (2, f);
  T (17, x)]

Name : yolo9000_8
perf : 53.206441
tile : [V f; U (1, f); ULambda y; U (3, h); T (128, c); Hoist_vars [c]; T (2, x);
  T (3, w); T (1, c); T (34, x);
  Lambda_apply y [((Iter 2), (Arg 13)); ((Iter 3), (Arg 14))]; T (16, f);
  T (1, x)]

Name : yolo9000_9
perf : 87.688557
tile : [V f; U (2, f); ULambda y; T (256, c); Hoist_vars [c]; T (1, x); T (1, x);
  Lambda_apply y [((Iter 6), (Arg 9)); ((Iter 1), (Arg 14))]; T (4, f);
  T (1, c); T (68, x)]

Name : yolo9000_12
perf : 36.196327
tile : [V f; U (2, f); ULambda y; T (256, c); Hoist_vars [c]; T (1, x); T (3, w);
  T (3, h); T (2, f); T (34, x);
  Lambda_apply y [((Iter 1), (Arg 8)); ((Iter 2), (Arg 13))]; T (8, f);
  T (1, c); T (1, f)]

Name : yolo9000_13
perf : 86.954835
tile : [V f; U (4, f); ULambda y; T (512, c); Hoist_vars [c]; T (2, x); T (1, x);
  Lambda_apply y [((Iter 1), (Arg 6)); ((Iter 4), (Arg 7))]; T (4, f);
  T (1, c); T (17, x)]

Name : yolo9000_18
perf : 28.520017
tile : [V f; U (2, f); ULambda y; T (512, c); Hoist_vars [c]; T (17, x); T (3, w);
  T (3, h); T (4, f); T (1, x);
  Lambda_apply y [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))]; T (2, f);
  T (1, c); T (4, f)]

Name : yolo9000_19
perf : 76.301196
tile : [V f; U (2, f); ULambda y; T (128, c); Hoist_vars [c]; T (1, x); T (1, c);
  T (16, f); T (17, x);
  Lambda_apply y [((Iter 1), (Arg 8)); ((Iter 1), (Arg 9))]; T (8, c)]

Name : mobilNet_1
perf : 91.037338
tile : [V f; U (1, f); ULambda y; U (3, h); T (32, c); Hoist_vars [c]; T (8, x);
  T (3, w); T (1, x);
  Lambda_apply y [((Iter 9), (Arg 10)); ((Iter 2), (Arg 11))]; T (2, f);
  T (1, c); T (14, x)]

Name : mobilNet_2
perf : 80.448558
tile : [V f; U (1, f); ULambda y; U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (2, x);
  Lambda_apply y [((Iter 2), (Arg 10)); ((Iter 3), (Arg 12))]; T (4, f);
  T (1, c); T (28, x)]

Name : mobilNet_3
perf : 92.411887
tile : [V f; U (1, f); U (14, y); U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (1, c); T (2, x); T (4, y); T (8, f); T (28, x)]

Name : mobilNet_4
perf : 86.554099
tile : [V f; U (2, f); ULambda y; U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (1, c); T (28, x);
  Lambda_apply y [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]; T (1, f)]

Name : mobilNet_5
perf : 49.405952
tile : [V f; U (1, f); U (14, y); U (3, h); T (256, c); Hoist_vars [c]; T (2, x);
  T (3, w); T (16, f); T (14, x); T (2, y); T (1, f); T (1, c); T (1, f)]

Name : mobilNet_6
perf : 75.883334
tile : [V f; U (2, f); U (14, y); T (256, c); Hoist_vars [c]; T (14, x); T (3, w);
  T (3, h); T (1, f); T (1, c); T (1, x); T (1, y); T (8, f)]

Name : mobilNet_7
perf : 41.771546
tile : [V f; U (4, f); U (7, y); T (512, c); Hoist_vars [c]; T (14, x); T (3, w);
  T (3, h); T (1, c); T (8, f); T (1, x); T (2, y); T (1, c)]

Name : mobilNet_8
perf : 61.065682
tile : [V f; U (4, f); U (7, y); T (64, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (1, c); T (8, f); T (1, y); T (8, c)]

Name : mobilNet_9
perf : 23.772964
tile : [V f; U (4, f); U (7, y); T (512, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (1, c); T (2, f); T (1, y); T (2, c); T (8, f); T (1, c)]

Name : resNet18_1
perf : 47.788382
tile : [V f; U (4, f); U (7, y); T (3, c); Hoist_vars [c]; T (1, x); T (7, w);
  T (7, h); T (4, x); T (16, y); T (1, c); T (28, x)]

Name : resNet18_2
perf : 92.324960
tile : [V f; U (1, f); U (14, y); U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (7, x); T (4, y); T (4, f); T (1, c); T (8, x)]

Name : resNet18_3
perf : 81.756321
tile : [V f; U (4, f); ULambda y; T (64, c); Hoist_vars [c]; T (8, x); T (1, x);
  Lambda_apply y [((Iter 7), (Arg 6)); ((Iter 2), (Arg 7))]; T (7, x)]

Name : resNet18_4
perf : 89.020396
tile : [V f; U (2, f); ULambda y; U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (1, c); T (28, x);
  Lambda_apply y [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]; T (1, f)]

Name : resNet18_5
perf : 85.635899
tile : [V f; U (2, f); ULambda y; T (64, c); Hoist_vars [c]; T (4, x); T (4, f);
  T (7, x); Lambda_apply y [((Iter 1), (Arg 13)); ((Iter 1), (Arg 15))];
  T (1, f)]

Name : resNet18_6
perf : 95.140219
tile : [V f; U (2, f); ULambda y; U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (1, c); T (28, x);
  Lambda_apply y [((Iter 2), (Arg 9)); ((Iter 1), (Arg 10))]; T (1, f)]

Name : resNet18_7
perf : 80.014458
tile : [V f; U (2, f); U (14, y); T (128, c); Hoist_vars [c]; T (14, x); T (3, w);
  T (3, h); T (2, f); T (1, c); T (1, x); T (1, y); T (4, f)]

Name : resNet18_8
perf : 76.757933
tile : [V f; U (1, f); U (14, y); U (3, h); T (128, c); Hoist_vars [c]; T (7, x);
  T (3, w); T (8, f); T (1, c); T (4, x); T (2, y); T (2, f)]

Name : resNet18_9
perf : 80.621806
tile : [V f; U (2, f); U (14, y); T (256, c); Hoist_vars [c]; T (14, x); T (3, w);
  T (3, h); T (2, f); T (1, c); T (1, x); T (1, y); T (4, f)]

Name : resNet18_10
perf : 60.689239
tile : [V f; U (4, f); U (7, y); T (64, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (2, c); T (8, f); T (1, y); T (4, c)]

Name : resNet18_11
perf : 89.231125
tile : [V f; U (4, f); U (7, y); T (128, c); Hoist_vars [c]; T (1, x); T (8, f);
  T (2, c); T (7, x); T (1, y); T (1, f)]

Name : resNet18_12
perf : 65.365226
tile : [V f; U (4, f); U (7, y); T (64, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (1, c); T (8, f); T (1, y); T (8, c)]


Name : yolo9000_0
perf : 43.488678
tile : [V f; U (4, f); U (8, y); U (3, h); T (3, c); Hoist_vars [c]; T (32, x);
  T (3, w); T (1, y); T (68, y); T (17, x); T (1, y)]

Name : yolo9000_2
perf : 91.037027
tile : [V f; U (4, f); U (4, y); U (3, h); T (32, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (2, x); T (68, y); T (2, f); T (1, c); T (8, x); T (17, x)]

Name : Yolo9000_4
perf : 85.861460
tile : [V f; U (2, f); ULambda y; U (3, w); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, h); T (8, y); T (4, f); T (1, c); T (2, f); T (136, x);
  Lambda_apply y [((Iter 1), (Arg 5)); ((Iter 2), (Arg 6))]; T (1, f)]

Name : yolo9000_5
perf : 82.038359
tile : [V f; U (2, f); ULambda y; T (128, c); Hoist_vars [c]; T (4, x); T (1, x);
  Lambda_apply y [((Iter 7), (Arg 10)); ((Iter 6), (Arg 11))]; T (4, f);
  T (34, x)]

Name : yolo9000_8
perf : 50.494220
tile : [V f; U (2, f); U (4, y); U (3, h); T (128, c); Hoist_vars [c]; T (2, x);
  T (3, w); T (17, y); T (1, f); T (1, c); T (1, f); T (34, x); T (16, f)]

Name : yolo9000_9
perf : 91.980342
tile : [V f; U (2, f); ULambda y; T (256, c); Hoist_vars [c]; T (1, x); T (4, x);
  Lambda_apply y [((Iter 2), (Arg 13)); ((Iter 3), (Arg 14))]; T (8, f);
  T (1, c); T (17, x)]

Name : yolo9000_12
perf : 36.627370
tile : [V f; U (2, f); ULambda y; U (3, h); T (256, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (1, c); T (1, x);
  Lambda_apply y [((Iter 1), (Arg 4)); ((Iter 3), (Arg 10))]; T (32, f);
  T (1, c); T (34, x)]

Name : yolo9000_13
perf : 91.453709
tile : [V f; U (4, f); ULambda y; T (512, c); Hoist_vars [c]; T (1, x); T (1, x);
  Lambda_apply y [((Iter 1), (Arg 6)); ((Iter 4), (Arg 7))]; T (8, f);
  T (1, c); T (34, x)]

Name : yolo9000_18
perf : 15.287213
tile : [V f; U (2, f); ULambda y; U (3, w); T (512, c); Hoist_vars [c]; T (1, x);
  T (3, h); T (32, f); T (17, x);
  Lambda_apply y [((Iter 1), (Arg 5)); ((Iter 2), (Arg 6))]; T (1, f);
  T (1, c); T (2, f)]

Name : yolo9000_19
perf : 89.265324
tile : [V f; U (4, f); ULambda y; T (128, c); Hoist_vars [c]; T (1, x); T (1, c);
  T (16, f); T (17, x);
  Lambda_apply y [((Iter 1), (Arg 5)); ((Iter 2), (Arg 6))]; T (8, c)]

Name : mobilNet_1
perf : 96.936926
tile : [V f; U (4, f); U (4, y); U (3, h); T (32, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (1, x); T (28, y); T (1, c); T (112, x)]

Name : mobilNet_2
perf : 91.324486
tile : [V f; U (2, f); ULambda y; U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (28, x);
  Lambda_apply y [((Iter 7), (Arg 6)); ((Iter 2), (Arg 7))]; T (1, c);
  T (2, x)]

Name : mobilNet_3
perf : 89.436455
tile : [V f; U (2, f); ULambda y; U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); Lambda_apply y [((Iter 7), (Arg 5)); ((Iter 3), (Arg 7))];
  T (4, f); T (1, c); T (1, f); T (56, x); T (2, f)]

Name : mobilNet_4
perf : 92.317446
tile : [V f; U (2, f); U (7, y); U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (8, f); T (1, c); T (28, x); T (4, y); T (1, f)]

Name : mobilNet_5
perf : 41.110067
tile : [V f; U (2, f); ULambda y; U (3, h); T (256, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (1, c); T (28, x);
  Lambda_apply y [((Iter 1), (Arg 4)); ((Iter 3), (Arg 8))]; T (16, f);
  T (1, c)]

Name : mobilNet_6
perf : 53.145759
tile : [V f; U (2, f); ULambda y; U (3, h); T (256, c); Hoist_vars [c]; T (2, x);
  T (3, w); T (16, f); T (1, c); T (7, x);
  Lambda_apply y [((Iter 1), (Arg 4)); ((Iter 1), (Arg 10))]; T (1, f)]

Name : mobilNet_7
perf : 32.877719
tile : [V f; U (2, f); ULambda y; U (3, h); T (512, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (1, c); T (14, x);
  Lambda_apply y [((Iter 1), (Arg 4)); ((Iter 1), (Arg 10))]; T (8, f)]

Name : mobilNet_8
perf : 66.913000
tile : [V f; U (4, f); U (7, y); T (256, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (16, f); T (2, c); T (1, y); T (1, f)]

Name : mobilNet_9
perf : 15.044334
tile : [V f; U (2, f); U (7, y); U (3, w); T (1024, c); Hoist_vars [c]; T (7, x);
  T (3, h); T (8, f); T (1, c); T (1, y); T (1, f); T (8, f); T (1, c)]

Name : resNet18_1
perf : 41.718582
tile : [V f; U (4, f); U (7, y); T (3, c); Hoist_vars [c]; T (8, x); T (7, w);
  T (7, h); T (2, x); T (16, y); T (2, f); T (7, x)]

Name : resNet18_2
perf : 95.928790
tile : [V f; U (2, f); ULambda y; U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (56, x);
  Lambda_apply y [((Iter 7), (Arg 6)); ((Iter 2), (Arg 7))]; T (1, c);
  T (1, x)]

Name : resNet18_3
perf : 88.487682
tile : [V f; U (2, f); ULambda y; T (64, c); Hoist_vars [c]; T (14, x); T (4, f);
  T (4, x); Lambda_apply y [((Iter 5), (Arg 9)); ((Iter 1), (Arg 11))];
  T (1, f)]

Name : resNet18_4
perf : 93.839609
tile : [V f; U (2, f); U (7, y); U (3, h); T (64, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (8, f); T (1, c); T (28, x); T (4, y); T (1, f)]

Name : resNet18_5
perf : 91.054432
tile : [V f; U (4, f); ULambda y; T (64, c); Hoist_vars [c]; T (14, x); T (4, f);
  T (2, x); Lambda_apply y [((Iter 2), (Arg 5)); ((Iter 3), (Arg 6))];
  T (1, f)]

Name : resNet18_6
perf : 95.172476
tile : [V f; U (2, f); U (7, y); U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (8, f); T (1, c); T (28, x); T (4, y); T (1, f)]

Name : resNet18_7
perf : 58.209192
tile : [V f; U (2, f); U (7, y); U (3, h); T (128, c); Hoist_vars [c]; T (2, x);
  T (3, w); T (16, f); T (1, c); T (7, x); T (2, y); T (1, f)]

Name : resNet18_8
perf : 50.219463
tile : [V f; U (2, f); ULambda y; U (3, h); T (128, c); Hoist_vars [c]; T (1, x);
  T (3, w); T (4, f); T (1, c); T (28, x);
  Lambda_apply y [((Iter 1), (Arg 4)); ((Iter 2), (Arg 12))]; T (4, f)]

Name : resNet18_9
perf : 52.067864
tile : [V f; U (2, f); ULambda y; U (3, h); T (256, c); Hoist_vars [c]; T (2, x);
  T (3, w); T (2, f); T (1, c); T (7, x);
  Lambda_apply y [((Iter 1), (Arg 4)); ((Iter 1), (Arg 10))]; T (8, f)]

Name : resNet18_10
perf : 64.847173
tile : [V f; U (4, f); U (7, y); T (256, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (16, f); T (2, c); T (1, y); T (1, f)]

Name : resNet18_11
perf : 89.260094
tile : [V f; U (4, f); U (7, y); T (256, c); Hoist_vars [c]; T (7, x); T (1, f);
  T (1, c); T (1, y); T (16, f)]

Name : resNet18_12
perf : 67.704154
tile : [V f; U (4, f); U (7, y); T (256, c); Hoist_vars [c]; T (7, x); T (3, w);
  T (3, h); T (16, f); T (2, c); T (1, y); T (1, f)]


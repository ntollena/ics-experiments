open Search.Schemes_avx512

(* Yolo9000-0 *)
let yolo9000_0 =
  [C; X; W; H], [Y], [Y; X], [Y; F]
(* Yolo9000-2 *)
let yolo9000_2 =
  [C; X; W; H], [Y; F; C], [X], [Y; F]
(* Yolo9000-4 *)
let yolo9000_4 =
  [C; X; W; H], [Y; F; C], [X; Y], []
(* Yolo9000-5 *)
let yolo9000_5 =
  [C; X], [X; Y; F; C], [X; Y], []
(* Yolo9000-8 *)
let yolo9000_8 =
  [C; X; W; H], [F; X; Y], [X; Y; F; C], []
(* Yolo9000-9 *)
let yolo9000_9 =
  [C; X], [X; Y; F; C], [X; Y], []
(* Yolo9000-12 *)
let yolo9000_12 =
  [C; X; W; H], [F; X; Y], [X; F; C], []
(* Yolo9000-13 *)
let yolo9000_13 =
  [C; X], [X; Y; F; C], [X; Y], [F; C; Y]
(* Yolo9000-18 *)
let yolo9000_18 =
  [ C; X; W; H], [F; C; X; Y], [F; C], []
(* Yolo9000-19 *)
let yolo9000_19 =
  [ C; X], [C; F; X; Y], [C], []
(* Yolo9000-23 *)
let yolo9000_23 =
  [ C; X], [F; X; Y], [F; C], []

let yolo_perms =
  [yolo9000_0; yolo9000_2; yolo9000_4;
   yolo9000_5; yolo9000_8; yolo9000_9;
   yolo9000_12; yolo9000_13; yolo9000_18;
   yolo9000_19; yolo9000_23]

let yolo_perms_reasonable =
  [yolo9000_0; yolo9000_2; yolo9000_4;
   yolo9000_5; yolo9000_8; yolo9000_9;
   yolo9000_12; yolo9000_13; yolo9000_18;
   yolo9000_19;]
(*   MobilNet-1 *)
let mbnet_1 =
  [ C; X; W; H], [X; Y; C], [X], [] 
(* MobilNet-2 *)
let mbnet_2 =
  [ C; X; W; H], [X; Y; F; C], [X], [] 
(* MobilNet-3 *)
let mbnet_3 =
  [ C; X; W; H], [Y; F; C], [X; Y], [] 
(* MobilNet-4 *)
let mbnet_4 =
  [ C; X; W; H], [Y; F; C], [X; Y], [] 
(* MobilNet-5 *)
let mbnet_5 =
  [ C; X; W; H], [C; F; X; Y], [C], [] 
(* MobilNet-6 *)
let mbnet_6 =
  [ C; X; W; H], [C; F; X; Y], [C], [] 
(* MobilNet-7 *)
let mbnet_7 =
  [ C; X; W; H], [F; C; X; Y], [F; C], [] 
(* MobilNet-8 *)
let mbnet_8 =
  [ C; X; W; H], [C; F; X; Y], [C], [] 
(* MobilNet-9 *)
let mbnet_9 =
  [ C; X; W; H] , [C; F] , [C] , [C] 

let mbnet_perms = [mbnet_1; mbnet_2; mbnet_3;
                   mbnet_4; mbnet_5; mbnet_6;
                   mbnet_5; mbnet_6; mbnet_9;
                  ]



(* ResNet18-1 *)
let resnet_1 =
  [ C; X; W; H], [X; Y; F; C], [X], [] 
(* ResNet18-2 *)
let resnet_2 =
  [ C; X; W; H], [X; Y; F; C], [X], []
(* ResNet18-3 *)
let resnet_3 =
  [ C; X], [X; Y; F], [X; Y], [] 
(* ResNet18-4 *)
let resnet_4 =
  [ C; X; W; H], [X; Y; F; C], [X], [] 
(* ResNet18-5 *)
let resnet_5 =
  [ C; X], [X; Y; F], [X; Y], [] 
(* ResNet18-6 *)
let resnet_6 =
  [ C; X; W; H], [X; Y; F; C], [X], [] 
(* ResNet18-7 *)
let resnet_7 =
  [ C; X; W; H], [C; F; X; Y], [C], [] 
(* ResNet18-8 *)
let resnet_8 =
  [C; X; W; H], [F; C; X; Y], [X; F; C], [X] 
(* ResNet18-9 *)
let resnet_9 =
  [ C; X; W; H], [F; C; X; Y], [F; C], [] 
(* ResNet18-10 *)
let resnet_10 =
  [ C; X; W; H], [C; F; X; Y], [C], [] 
(* ResNet18-11 *)
let resnet_11 =
  [ C; X], [X; F; C], [X; Y], [] 
(* ResNet18-12 *)
let resnet_12 =
  [C; X; W; H] , [C; F] , [C] , []

let resnet_perm =
  [ resnet_1; resnet_2; resnet_3;
   resnet_4; resnet_5; resnet_6;
   resnet_7; resnet_8; resnet_9;
   resnet_10; resnet_11; resnet_12;
  ]

(* Yolo9000-0 *)
let yolo9000_0 =
[F; Y; C; X; W; H], [Y], [Y; X], [Y] 
(* Yolo9000-2 *)
let yolo9000_2 =
[F; Y; C; X; W; H], [Y; F; C], [X], [] 
(* Yolo9000-4 *)
let yolo9000_4 =
[F; Y; C; X; W; H], [Y; F; C], [X; Y], [] 
(* Yolo9000-5 *)
let yolo9000_5 =
[F; Y; C; X], [X; Y; F; C], [X; Y], [] 
(* Yolo9000-8 *)
let yolo9000_8 =
[F; Y; C; X; W; H], [F; X; Y], [X; Y; F; C], [X] 
(* Yolo9000-9 *)
let yolo9000_9 =
[F; Y; C; X], [X; Y; F; C], [X; Y], [] 
(* Yolo9000-12 *)
let yolo9000_12 =
[F; Y; C; X; W; H], [F; X; Y], [X; F; C], [X] 
(* Yolo9000-13 *)
let yolo9000_13 =
[F; Y; C; X], [X; Y; F; C], [X; Y], [] 
(* Yolo9000-18 *)
let yolo9000_18 =
[F; Y; C; X; W; H], [F; C; X; Y], [F; C], [] 
(* Yolo9000-19 *)
let yolo9000_19 =
[F; Y; C; X], [C; F; X; Y], [C], [] 
let yolo9000_23 =
(* Yolo9000-23 *)
[F; Y; C; X], [F; X; Y], [F; C], [F]

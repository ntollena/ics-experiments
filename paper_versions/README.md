TTILE paper has been submitted 4 times in 2021.
1. ICS
2. MICRO
3. ASPLOS: figures are the same as used for OOPSLA first submission (before review)
4. OOPSLA: still running


TODO: suggestion to refactor the paper_version subdirectory of ics-experiments:
* have just one place with all the data (csv+log files)
* have subdirectories with the different python scripts, referencing the corresponding data.
* thus oopsla/micro/ics/asplos directories would contain only python code.
  * and `data` new subdir would contain all the charts and brut data.
* .... maybe the contrary would be actually better.
#!/usr/bin/env python3
import pandas as pd
import argcomplete, argparse
import plotly.graph_objects as go


# Example of use:
# python3 plot-mm.py  output.png  expe_small_mm_blis_mkl_xsmm_3.csv  expe_mkl_small.csv 

parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('mmcsvfile', help="csv where lambda vs blis-like tactic are to be found")
parser.add_argument('mklcsvfile', help="csv where mkl tactic are to be found")
#parser.add_argument('ttile', help="path to tvm+ttile csv")
argcomplete.autocomplete(parser)
args = parser.parse_args()



# Add the padding from 1 µkernel in here (compute it from "perf_blis_unroll" in dfmm)
def add_perf_blis_unroll(dfmm):
    column_size_i = dfmm['i']
    col_part_tile = dfmm["perf_blis_unroll"]
    assert(len(column_size_i) == len(col_part_tile))

    padding_factor = 12   # Blis µkernel size along I
    size_j = 128
    size_k = 128


    # Prepare the columns of the new df
    l_i_dfpadded = []
    l_perf_dfpadded = []

    for ind in range(0, len(col_part_tile)):
        size_i = column_size_i[ind]

        # padded_size_i = size of the padded problem (multiple above size_i)
        mod_pad = size_i % padding_factor
        if mod_pad == 0:
            padded_size_i = size_i
        else:
            padded_size_i = size_i - mod_pad + padding_factor


        try:
            ind_elem = list(column_size_i).index(padded_size_i)
        except (ValueError):
            # No elem - report nothing
            # (in practice, the last point I=49 does not have a point above available)
            ind_elem = -1

        if (ind_elem == (-1)):
            continue    # Skip to the next row (in the case they are not sorted)

        # Computation of the padded performance
        perc_pperf_above = col_part_tile[ind_elem]


        # Pinocchio => AVX512 (16 float per vectors) + 2 FMA units
        theor_best_cycle_time_padded = (padded_size_i * size_j * size_k)  / (16 * 2)

        # Time of the computation for the padded size
        cycle_time_padded = theor_best_cycle_time_padded / (perc_pperf_above * 0.01)

        # Actual problem size are smaller than the padded one
        theor_best_cycle_time = (size_i * size_j * size_k)  / (16 * 2)
        pperf_padded = theor_best_cycle_time / cycle_time_padded

        # We update the lists
        l_i_dfpadded.append(size_i)
        l_perf_dfpadded.append(pperf_padded * 100)


    dict_padded = dict()
    dict_padded['i'] = l_i_dfpadded
    dict_padded['perf_padded'] = l_perf_dfpadded

    # Converting the dictionnary into a pandas dataframe
    df_padded = pd.DataFrame(dict_padded)
    return df_padded



def genPlot(outfname, dfmm, dfmkl, df_padded):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_us"],
                  mode="lines+markers",
                  marker_symbol="circle",
                  marker_size=12,
                  name="Combination of µkernels"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_blis_unroll"],
                  mode="lines+markers",
                  marker_symbol="triangle-down",
                  marker_size=12,
                  name="Single µkernel, partial tile"))
    fig.add_trace(go.Scatter(x=df_padded['i'], y=df_padded["perf_padded"],
                  mode="lines+markers",
                  marker_symbol="square",
                  marker_size=12,
                  name="Single µkernel, padded"))
    
    fig.add_trace(go.Scatter(x=dfmkl['m'], y=dfmkl["peakperf(%)"],
                  mode="lines+markers",
                  marker_symbol="cross",
                  marker_size=12,
                  name="MKL"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["blis"],
                  mode="lines+markers",
                  marker_symbol="triangle-up",
                  marker_size=12,
                  name="Blis"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["xsmm"],
                  mode="lines+markers",
                  marker_symbol="diamond",
                  marker_size=12,
                  name="Libxsmm"))
    fig.update_yaxes(title="% of peak performance")
    fig.update_xaxes(title="I")
    
    #legend_options = { "text" : 20 }
    fig.update_layout(legend_font_size=20)
    fig.update_xaxes(title_font_size=20)
    fig.update_yaxes(title_font_size=20)

    fig.show()
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.write_image(f'{outfname}.png')

dfmm = pd.read_csv(args.mmcsvfile, sep=",")
dfmkl = pd.read_csv(args.mklcsvfile, sep=",")
#dfmatmul_paper = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/onednn_nancy_seq.csv",sep=",")


# Add the padding from 1 µkernel in here (compute it from "perf_blis_unroll" in dfmm)
df_padded = add_perf_blis_unroll(dfmm)

#print(df_padded)

genPlot(args.outputname, dfmm, dfmkl, df_padded)




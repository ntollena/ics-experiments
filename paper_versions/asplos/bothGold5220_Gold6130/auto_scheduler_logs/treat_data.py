import pandas as pd
from statistics import mean

import json

ics_xp_home = "/home/hbrunie/gitlab/ics-experiments/"

pre_readdir = ics_xp_home +"/paper_versions/asplos/bothGold5220_Gold6130/auto_scheduler_logs/coldcache/"

writefile_basename = "performance.csv"

readdir_list = [
    "log_1_1_avx512_XeonGold5220",
    "log_1_18_avx512_XeonGold5220",
    "log_1_1_avx512_XeonGold6130",
    "log_1_32_avx512_XeonGold6130",
]

from input_conv import input_conv

filelist = input_conv.keys()

import os.path

for readdir in readdir_list:
    readdir = pre_readdir + readdir
    writefile = readdir + "/" + writefile_basename
    with open(writefile, "w") as ouf:
        ouf.write("index,conv2d_name,mean(time_sec),min(time_sec),max(time_sec)\n")
        for file_basename in filelist:
            file = readdir + "/" + file_basename + ".log"
            if not os.path.isfile(file):
                continue
            with open(file, "r") as inf:
                for index, il in enumerate(inf.readlines()):
                    # print(il)
                    res = json.loads(il)
                    time_sec = res["r"][0]
                    # print(res["r"])
                    # print(mean(time_ms), min(time_ms), max(time_ms))
                    ouf.write(
                        f"{index},{file_basename},{mean(time_sec)},{min(time_sec)},{max(time_sec)}\n"
                    )


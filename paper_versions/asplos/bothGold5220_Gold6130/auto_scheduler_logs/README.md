AutoScheduler log files
=======================

In this directory
-----------------

subdirs:
 * coldcache
 * hotcache
* `input_conv.py`:
  * Contains list of conv2d benchmarks and their attributed dimension sizes.
* `treat_data.py`:
  1. use `input_conv.py` file to get the list of possibly existing conv2d log files.
  2. Take the data from the different log files
  3. convert the data into one csv file `performance.csv` (one file per machine, per nbthreads)

Where does the data come from?
------------------------------
coldcache directory from
    tvm_ttile
        origin	git@gitlab.inria.fr:spouget/tvm_ttile.git (fetch)

    From git branch:

     >> git branch
      FlexTensor
      autotvm++
    * flex
      main
      mppa
      random_ttile
      tvm_june
      tvm_ttile_hotcache
      x86

    commit b05a5e8e4971d3a67bdbe3c8c810a23838544396

hotcache from
    commit b57dbb22babe72f765b63badfdd5aa9d681c4b30
    branch tvm_ttile_hotcache

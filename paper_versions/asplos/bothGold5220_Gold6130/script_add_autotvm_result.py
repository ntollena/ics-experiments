import pandas as pd
import re


ics_home = "/home/hbrunie/gitlab/ics-experiments/"

dir_1 = ics_home + "paper_versions/asplos/"
dir_2 = dir_1 + "bothGold5220_Gold6130/"
dir_3 = dir_1 + "Figure_perf_ASPLOS22/"

fname = dir_2 + "results_all_autotvm_fromlogs.csv"
df_autosched_fromlogs = pd.read_csv(fname)
print(df_autosched_fromlogs)

for machine, par_nbthreads in zip(["XeonGold5220", "XeonGold6130"], [18, 32]):
    fname = dir_3 + f"all_tools_result_{machine}_fromlogs.csv"
    fwritename = dir_3 + f"all_tools_result_{machine}_fromlogs.csv"
    df_all: pd.DataFrame = pd.read_csv(fname, sep=",")
    print(df_all)

    df = df_autosched_fromlogs.loc[df_autosched_fromlogs["archi"] == machine].copy()

    timems = []
    for name in df_all["NameConv_SP"].values:
        if "_Seq" in name:
            seqstr = "_Seq"
            nbthread = 1
        else:
            seqstr = "_Par"
            nbthread = par_nbthreads
        df2 = df.loc[df["nbthreads"] == nbthread].copy()
        df3 = df2.loc[df2["NameConv"] == re.sub(seqstr, "", name)]
        assert len(df3["Time(ms)"].values) == 1
        timems.append(df3["Time(ms)"].values[0])

    df_all["autotvm_fromlogs"] = timems
    df_all.to_csv(fwritename)
    print(df_all)

#!/usr/bin/env python3
import pandas as pd
import argcomplete, argparse
import os
import glob

parser = argparse.ArgumentParser(description='Merge all csv from the directory defined by path.')
parser.add_argument('outputname', help="Precise basename for the csv output file.")
parser.add_argument('sep', help="CSV separator")
parser.add_argument('--nbthread_list', help="Precise absolute or relative path for the directory full of csv or files.", nargs='+')
parser.add_argument('--path_dir_list', help="Precise absolute or relative path for the DIRECTORY full of csv.", nargs='+')
parser.add_argument('--path_list', help="Precise absolute or relative path for the csv FILES.", nargs='+')
parser.add_argument('--debug', help="Enable debug verbose", action='store_true')
parser.add_argument('--change_header', help="When header not in good format (hard coded OneDNN csv header).", action='store_true')
parser.add_argument('--sep_out', help="output file CSV separator", default=";")
parser.add_argument('--header_list', nargs='+', help="List of name in csv input file header", default=['MethodParallelization', 'NameConv', 'Time(ms)', 'std'], type=str)
argcomplete.autocomplete(parser)
args = parser.parse_args()
csvoutname = args.outputname
first = True
mode = 'w'
header=True
print("sep",args.sep)
if args.debug:
    print(args.nbthread_list, args.path_list)
paths = args.path_list if args.path_list else args.path_dir_list
for nbthread,path in zip(args.nbthread_list, paths):
    if args.debug:
        print(nbthread,path)
    file_list = glob.glob(os.path.join(path, "*.csv")) if args.path_dir_list else paths
    for filename in file_list:
        if args.debug:
            print(f"path: {path} read {filename} and write into {csvoutname} / nbthreads = {nbthread}")
        df = pd.read_csv(filename, sep=args.sep) 
        print(df)
        ## Some csv contains more columns, thus we cut and only extract the one of interest for us,
        ## so that we generate a valid csv with the same number of columns for each row.
        df = df[args.header_list]
        df['nbthreads'] = nbthread
        if args.change_header:
            df['Time(ms)'] = df['mean_wlast']
            df['std'] = df['stddev_wlast']
            df = df[['NameConv', 'Time(ms)', 'nbthreads','std']]
        df.to_csv(csvoutname,header=header, mode=mode,sep=args.sep_out, index_label=False)
        header = False
        mode = 'a'
print(csvoutname,"generated.")

import argcomplete, argparse
import pandas as pd


class Machine:
    def __init__(self, name="XeonGold6130"):
        self.name = name
        if name == "XeonGold6130":
            self.build6130()
        else:
            assert name == "XeonGold5220", f"wrong machine name: not supported {name}"
            self.build5220()

    def build5220(self):
        self.fma = 1
        self.vectorsize = 16
        self.freq = 2200000000
        self.cores = 18
        self.socket = 1
        self.avxtype = "avx512"

    def build6130(self):
        self.fma = 2
        self.vectorsize = 16
        self.freq = 2300000000
        self.cores = 32
        self.socket = 2
        self.avxtype = "avx512"


def peakPerf(
    x, y, f, c, h, w, stride, core, fma, vectorsize, freq, in_seconds=True, debug=False
):
    "Returns the best time in ms possible for this convolution on this machine"
    res = float(
        (
            (f * c * h * w * (y / stride) * (x / stride))
            / (vectorsize * fma * core)
            / (freq)
            # * 1000
        )
    )
    if not in_seconds:  ## in milliseconds
        res *= 1000

    if debug:
        print("Computing peak performance:")
        print("f,c,y,x,h,w,stride")
        print(f, c, y, x, h, w, stride)
        print(f"fma {fma} vec {vectorsize} freq {freq} nbcores {core}")
    ## multiplied by 1000 because time is in ms in the csv
    return res


def compute_peak_perf(
    df_convsize, NameConv, machine: Machine, timems, nbthreads, debug
):
    conv = df_convsize.loc[df_convsize.name == NameConv]
    vectorsize = machine.vectorsize
    freq = machine.freq
    fma = machine.fma

    peakperf = peakPerf(
        int(conv.x),
        int(conv.y),
        int(conv.f),
        int(conv.c),
        int(conv.h),
        int(conv.w),
        int(conv.stride),
        nbthreads,  ## different from machine nbcores because parallelism can use less threads.
        fma,
        vectorsize,
        freq,
        in_seconds=False,
        debug=debug,
    )
    ## Compute % of peak performance reached for this convolution
    percPeakPerf = float(100 * peakperf / timems)
    if percPeakPerf > 100:
        print(NameConv, timems, nbthreads, debug, peakperf)
        print("Error: % peakPerf > 100%. Have you specified the correct machine?")
        exit(-1)
    return percPeakPerf


if __name__ == "__main__":

    # machine = Machines["XeonGold5220"]
    #
    # machinename = machine["name"]
    # fma = machine["fma"]
    # vectorsize = machine["vectorsize"]
    # freq = machine["freq"]
    # nbcores = machine["cores"]
    # nbsockets = machine["socket"]
    # avxtype = machine["avxtype"]
    #
    # df_convsize = pd.read_csv(
    #    "/home/hbrunie/gitlab/ics-experiments/common_csv/conv_sizes.csv"
    # )
    # ics_xp_home = "/home/hbrunie/gitlab/ics-experiments/"
    #
    # writedir = "/home/hbrunie/tests/"
    #
    # _debug = False
    #

    parser = argparse.ArgumentParser(
        description="Generates png figure to show % of performance peak from given CSVs files. Be careful of your CSV header."
    )
    parser.add_argument("outputname", help="output file base name (without the .png)")
    parser.add_argument(
        "convsizes",
        help="path to conv size csv (ics-experiments/common_csv/conv_sizes.csv)",
    )
    parser.add_argument("tvmttile", help="path to tvm+ttile csv")
    parser.add_argument("--debug", help="Add some verbose", action="store_true")
    parser.add_argument(
        "--machine",
        help="Name of the machine on which have been executed the experiments.",
        default="XeonGold5220",
    )
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    debug = args.debug

    autosched_dir = "/paper_versions/asplos/bothGold5220_Gold6130/auto_scheduler_logs/"
    nbthreads = 18
    machine = "XeonGold5220"
    subdir = f"hotcache/log_1_{nbthreads}_avx512_{machine}/"
    autosched_dir = (
        "/paper_versions/asplos/bothGold5220_Gold6130/auto_scheduler_logs/" + subdir
    )
    readcsv = ics_xp_home + autosched_dir + "performance.csv"

    df = pd.read_csv(readcsv, sep=",")

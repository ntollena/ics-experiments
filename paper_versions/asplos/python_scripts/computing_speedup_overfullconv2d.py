
import pandas as pd

# Input files
readdir = "/home/hbrunie/gitlab/ics-experiments/common_csv/"
filename_convsizes = readdir + "conv_sizes.csv"
readdir = "/home/hbrunie/gitlab/ics-experiments/paper_versions/asplos/Figure_perf_ASPLOS22/"
file_name_data_1 = readdir+"all_tools_result_XeonGold5220.csv"
file_name_data_2 = readdir+"all_tools_result_XeonGold6130.csv"

# Output figure
writedir = "/home/hbrunie/gitlab/ics-experiments/paper_versions/asplos/Figure_perf_ASPLOS22/tests/"
output_filename_1 = writedir+"Figure_speedup_over_Ansor_XeonGold5220.png"
output_filename_2 = writedir+"Figure_speedup_over_Ansor_XeonGold6130.png"

#df_all_1 = pd.read_csv(file_name_data_1, sep=";")
#df_all_2 = pd.read_csv(file_name_data_2, sep=";")

def getSpeedup(df_all_1, conv2d="Yolo9000", typeps="Par"):
    x_namelist = list(filter(lambda x: conv2d in x and typeps in x, list(df_all_1["NameConv_SP"].values)))
    df_all = df_all_1[df_all_1["NameConv_SP"].isin(x_namelist)].copy()
    x = sum(list(df_all.autoscheduler)) / sum(list(df_all.tvm_ttile))
    return x
#x_namelist_seq_yolo = list(filter(lambda x: "Yolo9000" in x and "Seq" in x, list(df_all_1["NameConv_SP"].values)))
#x_namelist_par_resnet = list(filter(lambda x: "ResNet" in x and "Par" in x, list(df_all_1["NameConv_SP"].values)))
#x_namelist_seq_resnet = list(filter(lambda x: "ResNet" in x and "Seq" in x, list(df_all_1["NameConv_SP"].values)))
#df_all_par_yolo = df_all_1[df_all_1["NameConv_SP"].isin(x_namelist_par_yolo)].copy()
#df_all_seq_resnet = df_all_1[df_all_1["NameConv_SP"].isin(x_namelist_seq_resnet)].copy()
#df_all_par_resnet = df_all_1[df_all_1["NameConv_SP"].isin(x_namelist_par_resnet)].copy()

#print("XeonGold5220")
#print("Yolo9000 Seq",x)
#x = sum(list(df_all_par_yolo.autoscheduler)) / sum(list(df_all_par_yolo.tvm_ttile))
#print("Yolo9000 Par",x)
#x = sum(list(df_all_seq_resnet.autoscheduler)) / sum(list(df_all_seq_resnet.tvm_ttile))
#print("ResNet18 Seq",x)
#x = sum(list(df_all_par_resnet.autoscheduler)) / sum(list(df_all_par_resnet.tvm_ttile))
#print("ResNet18 Par",x)
#
#x_namelist_par_yolo = list(filter(lambda x: "Yolo9000" in x and "Par" in x, list(df_all_2["NameConv_SP"].values)))
#x_namelist_seq_yolo = list(filter(lambda x: "Yolo9000" in x and "Seq" in x, list(df_all_2["NameConv_SP"].values)))
#x_namelist_par_resnet = list(filter(lambda x: "ResNet" in x and "Par" in x, list(df_all_2["NameConv_SP"].values)))
#x_namelist_seq_resnet = list(filter(lambda x: "ResNet" in x and "Seq" in x, list(df_all_2["NameConv_SP"].values)))
#df_all_seq_yolo = df_all_2[df_all_2["NameConv_SP"].isin(x_namelist_seq_yolo)].copy()
#df_all_par_yolo = df_all_2[df_all_2["NameConv_SP"].isin(x_namelist_par_yolo)].copy()
#df_all_seq_resnet = df_all_2[df_all_2["NameConv_SP"].isin(x_namelist_seq_resnet)].copy()
#df_all_par_resnet = df_all_2[df_all_2["NameConv_SP"].isin(x_namelist_par_resnet)].copy()
#print("XeonGold6130")
#x = sum(list(df_all_seq_yolo.autoscheduler)) / sum(list(df_all_seq_yolo.tvm_ttile))
#print("Yolo9000 Seq",x)
#x = sum(list(df_all_par_yolo.autoscheduler)) / sum(list(df_all_par_yolo.tvm_ttile))
#print("Yolo9000 Par",x)
#x = sum(list(df_all_seq_resnet.autoscheduler)) / sum(list(df_all_seq_resnet.tvm_ttile))
#print("ResNet18 Seq",x)
#x = sum(list(df_all_par_resnet.autoscheduler)) / sum(list(df_all_par_resnet.tvm_ttile))
#print("ResNet18 Par",x)
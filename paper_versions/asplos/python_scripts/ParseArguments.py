import argparse, argcomplete


def parsePlotPerf():
    parser = argparse.ArgumentParser(
        description="Generates png figure to show % of performance peak from given CSVs files. Be careful of your CSV header."
    )
    parser.add_argument("outputname", help="output file base name (without the .png)")
    parser.add_argument(
        "convsizes",
        help="path to conv size csv (ics-experiments/common_csv/conv_sizes.csv)",
    )
    parser.add_argument("tvmttile", help="path to tvm+ttile csv")
    parser.add_argument("--autotvm", help="path to autotvm csv", default=None)
    parser.add_argument("--onednn", help="path to oneDNN csv", default=None)
    parser.add_argument(
        "--autoscheduler_fromlogs",
        help="path to autoscheduler (aka Ansor) csv built from log files.",
        default=None,
    )
    parser.add_argument(
        "--autoscheduler", help="path to autoscheduler (aka Ansor) csv", default=None
    )
    parser.add_argument("--flextensor", help="path to FlexTensor csv", default=None)
    parser.add_argument("--mopt", help="path to mopt csv", default=None)
    parser.add_argument("--debug", help="Add some verbose", action="store_true")
    parser.add_argument(
        "--machine",
        help="Name of the machine on which have been executed the experiments.",
        default="XeonGold5220",
    )
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    return args

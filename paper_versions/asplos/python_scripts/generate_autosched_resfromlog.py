#!/usr/bin/env python3

import argcomplete, argparse
import pandas as pd
from typing import List


def generateCsvFile(df_list: List[pd.DataFrame]) -> pd.DataFrame:
    df_all = pd.concat(df_list)
    return df_all


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""
        Generates csv file to contain best % of peak performance from log files.
        In practice, for now, this script only concatenate the different csv from
        log files. These csv have been generated with the treat_data.py script
        inside the autoscheduler_log subdirectory.
        """
    )
    parser.add_argument(
        "--ics_home",
        help="Absolute path to the home of ics-experiments directory.",
        required=True,
    )
    parser.add_argument(
        "--outputsubdir",
        help="output subdirectory relative path.",
        default="/paper_versions/asplos/bothGold5220_Gold6130/",
    )
    parser.add_argument(
        "--outputname",
        help="output file base name (without the .csv)",
        default="results_all_autoscheduler_fromlogs",
    )
    parser.add_argument(
        "--convsizes",
        help="path to conv size csv (ics-experiments/common_csv/conv_sizes.csv)",
    )
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    outputcsv = args.ics_home + args.outputsubdir + args.outputname + ".csv"

    autoscheddir_relpath = (
        "/paper_versions/asplos/bothGold5220_Gold6130/auto_scheduler_logs/"
    )
    autoscheddir_abspath = args.ics_home + autoscheddir_relpath
    ## for hot cache directory only: Note that this does not impact log measured performance
    # as they are always executed with hot cache.
    ## for all machines, for all nb of threads (serial and parallel)
    df_list: List[pd.DataFrame] = []
    for nbthreads_list, machine in zip(
        [[1, 18], [1, 32]], ["XeonGold5220", "XeonGold6130"]
    ):
        for nbthreads in nbthreads_list:
            subdir = f"hotcache/log_1_{nbthreads}_avx512_{machine}/"
            ## performance.csv contains the already formatted data from the log files.
            readcsv = autoscheddir_abspath + subdir + "performance.csv"
            df_tmp: pd.DataFrame = pd.read_csv(readcsv)
            df_tmp = df_tmp[["conv2d_name", "mean(time_sec)"]].copy()
            df_tmp["Time(ms)"] = df_tmp[["mean(time_sec)"]].apply(lambda x: x * 1000.0)
            df_tmp = df_tmp.rename(columns={"conv2d_name": "NameConv"}).copy()
            df_tmp = df_tmp.groupby(["NameConv"]).min()
            df_tmp["archi"] = machine
            df_tmp["nbthreads"] = nbthreads
            df_list.append(df_tmp)

    df_all = generateCsvFile(df_list)
    print(f"Generating CSV: {outputcsv}")
    ## nbthreads,archi,NameConv,Time(ms),std
    df_all.to_csv(outputcsv)

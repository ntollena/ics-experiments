#!/usr/bin/env python3
import argcomplete, argparse
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import os

parser = argparse.ArgumentParser(description='Generates png figure to show % of performance peak from given CSVs files. Be careful of your CSV header.')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('convsizes', help="path to conv size csv (ics-experiments/common_csv/conv_sizes.csv)")
parser.add_argument('tvmttile', help="path to tvm+ttile csv")
parser.add_argument('--autotvm', help="path to autotvm csv", default=None)
parser.add_argument('--onednn', help="path to oneDNN csv", default=None)
parser.add_argument('--autoscheduler', help="path to autoscheduler (aka Ansor) csv", default=None)
parser.add_argument('--flextensor', help="path to FlexTensor csv", default=None)
parser.add_argument('--mopt', help="path to mopt csv", default=None)
parser.add_argument('--debug', help="Add some verbose", action='store_true')
parser.add_argument('--machine', help="Name of the machine on which have been executed the experiments.", default="XeonGold5220")
argcomplete.autocomplete(parser)
args = parser.parse_args()
debug = args.debug

df_convsize = pd.read_csv(args.convsizes)

def add_background(fig,leny,scale=2,start=0.0025):
    """ Add the background grey colored column to differentiate the
        different convolution benchmarks.
        scale is for the column size (steps) and start is for the 
        x-axis coordinate to start the columns.
    """
    # find step size for an interval for the number of y-values
    steps = scale/leny
    # set up where each interval ends
    ends = [start + steps*(e+1) for e in np.arange(0, leny//scale)]
    # container for shapes to be added as backgrounds
    shapes = []
    # super-easy way of making a list for alternating backgrounds
    colors = ['grey', 'rgba(0,0,0,0)']*leny
    # set up shapes for alternating background colors
    for i, e in enumerate(ends):
            shapes.append(dict(type="rect",
                            xref="paper",
                            yref="paper",
                            x0=e-steps,
                            y0=0,
                            x1=e,
                            y1=1,
                            fillcolor=colors[i],
                            opacity=0.5,
                            layer="below",
                            line_width=0,
            )
        )
    ## The grid is removed from xaxis in order to better see the grey columns
    fig.update_layout(xaxis=dict(showgrid=False), shapes=shapes)

def peakPerf(x,y,f,c,h,w,stride,core,fma,vectorsize,freq, debug=False):
    "Returns the best time in ms possible for this convolution on this machine"
    if debug:
        print("Computing peak performance:")
        print("f,c,y,x,h,w,stride")
        print(f,c,y,x,h,w,stride)
        print(f"fma {fma} vec {vectorsize} freq {freq} nbcores {core}")
    ## multiplied by 1000 because time is in ms in the csv
    return float(((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000))

def get_best_of_all(df_input, max_nbthreads):
    """ 
        Returns a data frame COPY of input data frame
        Returns best (smallest) Time(ms) per convolution with regard
        to parallelization method and random schemes evaluated,
        or any other characteristics.
    """
    df_output_list = []
    ## Extracting only sequential data
    tmp = df_input.loc[df_input['nbthreads'] == 1].copy()
    print("************************************************************************")
    print("Missing Yolo9000_23 in some data (nbthreads 1), thus skipping everywhere.")
    print("************************************************************************")
    ## TODO: with Yolo9000_23 data, remove this line
    #tmp = tmp.loc[(tmp['NameConv'] != "Yolo9000_23")]
    ## Get best time (sort_values --> first) for each (groupby) convolution
    best = tmp.sort_values("Time(ms)").groupby("NameConv", as_index=False).first()
    df_output_list.append(best.copy())
    ## Extracting only parallel data
    tmp = df_input.loc[df_input['nbthreads'] == max_nbthreads].copy()
    print("************************************************************************")
    print(f"Missing Yolo9000_23 in some data (nbthreads {max_nbthreads}), thus skipping everywhere.")
    print("************************************************************************")
    ## TODO: with Yolo9000_23 data, remove this line
    #tmp = tmp.loc[(tmp['NameConv'] != "Yolo9000_23")]
    best = tmp.sort_values("Time(ms)").groupby("NameConv", as_index=False).first()
    df_output_list.append(best.copy())
    ## Concatenating data frames from sequential and parallel data extraction.
    df_output = pd.concat(df_output_list)
    return [df_output]

def get_best_for_each(df_input, numParMethods):
    """ UNUSED for now.
        TO USE: set  one_per_parallel_method to True in genPlot call.
        Return 3 data frame COPY of input data frame
        Returns best (smallest) Time(ms) per convolution for each
        of the parallelization methods tested
    """
    df_output_list = []
    for parMethodIndex in range(numParMethods):
        ## Extract results for specific parallelization method
        tmp = df_input.loc[df_input['MethodParallelization'] == parMethodIndex].copy()
        ## Extract the best result (smallest Time(ms)) per convolution
        tmp = tmp.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
        ## Append best result per convolution to list of size "number of parallel methods"
        df_output_list.append(tmp)
    return df_output_list
    
Machines = {
        "XeonGold6130": {
            "name": "XeonGold6130",
            "fma": 2,
            "vectorsize": 16,
            "freq": 2300000000,
            "cores": 32,
            "socket": 2,
            "avxtype": "avx512"
            },
        "XeonGold5220": {
            "name": "XeonGold5220",
            "fma": 1,
            "vectorsize": 16,
            "freq": 2200000000,
            "cores": 18,
            "socket": 1,
            "avxtype": "avx512"
            }
        }

def getNamesConv(dflist):
    from functools import reduce
    dfl = filter(lambda a: a is not None, dflist)
    list_names = list(map(lambda df: set(df['NameConv']), dfl))
    return sorted(list(reduce(set.union, list_names, set([]))))

def genPlot(debug, outfname, dfttile, machine=Machines["XeonGold5220"], dfautotvm=None,
        dfonednn=None, dfautoscheduler=None, dfflextensor=None, dfmopt=None,
        one_per_parallel_method=False):
    """ 
        Generates PNG plot.
    """
    ## Extract data from Machines dictionnary
    machinename = machine["name"]
    fma = machine["fma"]
    vectorsize = machine["vectorsize"]
    freq = machine["freq"]
    nbcores = machine["cores"]
    nbsockets = machine["socket"]
    avxtype = machine["avxtype"]

    conv_names = getNamesConv([dfmopt, dfttile, dfonednn, dfautoscheduler])
    ## Build list of data frame for TVM+TTILE
    df_ttile_parallel_methods = []
    ## Build list of names for the chart
    name_list = ['TVM+TTILE']
    if one_per_parallel_method:
        numParMethods = 3
        df_ttile_parallel_methods = get_best_for_each(dfttile,numParMethods)
    else:
        df_ttile_parallel_methods = get_best_of_all(dfttile, nbcores)

    ## List containing all data frames
    all_list = [] + df_ttile_parallel_methods

    def add_to_list(df, name, all_list, name_list):
        if df is not None:
            all_list += [df.copy()]
            name_list += [name]
    add_to_list(dfautotvm.loc[(dfautotvm.nbthreads == 1)
                              | (dfautotvm.nbthreads == nbcores)],
                'AutoTVM', all_list, name_list)
    add_to_list(dfautoscheduler, 'AutoScheduler', all_list, name_list)
    add_to_list(dfonednn, 'OneDNN', all_list, name_list)
    add_to_list(dfflextensor, 'FlexTensor', all_list, name_list)
    add_to_list(dfmopt, 'Mopt', all_list, name_list)

    def compute_peak_perf(NameConv, timems, nbthreads, debug):
        conv = df_convsize.loc[df_convsize.name == NameConv]
        core = nbthreads
        print(conv)
        peakperf = peakPerf(int(conv.x), int(conv.y), int(conv.f), int(conv.c),
                            int(conv.h), int(conv.w), int(conv.stride), core, fma,
                            vectorsize, freq, debug=debug)
        ## Compute % of peak performance reached for this convolution
        percPeakPerf = float(100 * peakperf / timems)
        if percPeakPerf > 100:
            print(NameConv, timems, nbthreads, debug, peakperf)
            print("Error: % peakPerf > 100%. Have you specified the correct machine?")
            exit(-1)
        return percPeakPerf

    ## Change Time(ms) into % peak performance --> 'perPP'
    nbthreads = 0
    for name, df in zip(name_list, all_list):
        def from_row(row):
            time = 100000
            if row["Time(ms)"] != "ERROR":
                time = float(row["Time(ms)"])
            print("NameConv ", name)
            print(row)
            return compute_peak_perf(str(row['NameConv']), time, int(row['nbthreads']), debug)
        df['perPP'] = df.apply(from_row, axis = 1)
        if name == "OneDNN" and machine["name"] == "XeonGold6130":
            df['perPP'] = df[["PerPP", "perPP"]].max(axis=1)
        print(df.loc[df.NameConv == "Yolo9000_23"])
        lendf = len(df['perPP'].values)
        df.sort_values(by=['NameConv', 'nbthreads'], ascending=True,inplace=True)
        nbthreads = int(max(list(df.nbthreads.values)))

    title = f"Sequential --- Parallel ({nbthreads} threads out of {nbcores} cores):"
    title += f"TVM+Ttile vs AutoTVM on {machinename} {nbcores} cores,"
    ghzfreq = freq / 1000000000
    title += f"{nbsockets} socket, {fma} FMA, {avxtype}, {ghzfreq}GHz"
    fig = go.Figure()

    colors = ['indianred', 'blue', 'green', "orange", 'purple', 'yellow',
              'black']
    symbols = ['square', 'diamond', 'circle', 'circle-x-open', 'diamond-open',
               'cross']

    ## Building zipped list of names, colors and data frames.
    zip_list = zip(name_list, symbols, colors, all_list)
    ## Iterate over them and trace each of the lines
    def build_column(name, pp_dict, nthreads_list):
        pass
    for name, symbol, color, df in zip_list:
        print("Name ", name)
        print(df.loc[df.NameConv == "Yolo9000_23"])
        ## TODO: clean x_namelist creation
        name_list = list(set([name for name in df['NameConv']]))
        x_namelist = [name + f"_{threads}_threads" for name in name_list for threads in [1, nbcores]]
        print("Name List :", name_list)
        print("X Name List :", x_namelist)
        #x_namelist = [name + f"_{threads}_threads" for name in df.loc[df.nbthreads == 1 ]['NameConv'].values for threads in [1,nbcores]]
        if debug:
            print(f"Adding trace: {name}")
            lendf = len(df['perPP'].values)
            lenname = len(x_namelist)
            print(lendf, lenname)
        fig.add_trace(go.Scatter(
            x=x_namelist,
            ##TODO error bars based on perPP
            #error_y=dict(type='data', array=dfttile_0['std'].values),
            y=df['perPP'].values,
            name=name,
            mode="markers",
            marker_color=color,
            marker_symbol=symbol
        ))
        for i in range(0, len(x_namelist)-1, 2):
            fig.add_trace(go.Scatter(
                x=x_namelist[i:i+2],
                ##TODO error bars based on perPP
                #error_y=dict(type='data', array=dfttile_0['std'].values),
                y=df['perPP'].values[i:i+2],
                name=name,
                mode="lines",
                marker_color=color,
                showlegend=False
            ))
        ## TODO: clean x_namelist creation
        x_namelist = [name if threads == 1 else "" for name in name_list for threads in [1, nbcores]]
        #x_namelist = [name if threads ==1 else "" for name in df.loc[df.nbthreads ==1 ]['NameConv'].values for threads in [1,18]]
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(legend=dict(
    yanchor="middle",
    y=0.9,
    xanchor="right",
    x=1.00
    ))
    print("Namelist", x_namelist)
    add_background(fig, len(x_namelist))
    fig.update_layout(height=600, width=1200)
    fig.update_yaxes(title="% of peak performance", range=[0,101])
    fig.update_xaxes(title=title, tickmode = 'array',
        ##TODO: update range when adding Yolo9000_23 and full CNNs
        tickvals = [x+0.25 for x in range(65)],
        ticktext = x_namelist,
        ##TODO: update range when adding Yolo9000_23 and full CNNs
        range=[-0.5,63.5])
    fig.write_image(f'{outfname}.png')

## AVX512 Gold
dfautotvm = None
dfautoscheduler = None
dfonednn = None
dfmopt = None
if args.onednn:
    dfonednn = pd.read_csv(args.onednn,sep=",")
if args.autotvm:
    dfautotvm = pd.read_csv(args.autotvm, sep=",")
    dfautotvm = dfautotvm.loc[dfautotvm.archi == f"avx512_{args.machine}"].copy()
if args.autoscheduler:
    dfautoscheduler = pd.read_csv(args.autoscheduler, sep=",")
    dfautoscheduler = dfautoscheduler.loc[dfautoscheduler.archi == "avx512_XeonGold5220"].copy()
if args.mopt:
    dfmopt = pd.read_csv(args.mopt, sep=",")
dfttile = pd.read_csv(args.tvmttile,sep=";")
genPlot(debug, args.outputname, dfttile, machine=Machines[args.machine], dfautotvm=dfautotvm,
        dfautoscheduler=dfautoscheduler, dfonednn=dfonednn, dfmopt=dfmopt)

#!/usr/bin/env python3

import csv

# INPUT Filename convention
# Example: "result_autoscheduler_1_18_avx512_XeonGold5220.csv"
def get_name_file(toolname, b_isparallel, archi):
    assert(toolname == "autoscheduler" or toolname == "autoTVM" or toolname == "onednn"
            or toolname == "mOpt" or toolname == "tvm_ttile")
    assert(archi == "5220" or archi == "6130")
    
    num_thread = 1
    if (b_isparallel):
        if (archi == "5220"):
            num_thread = 18
        else:
            num_thread = 32

    name_file = "result_" + toolname + "_1_" + str(num_thread) + "_avx512_XeonGold" + archi + ".csv"
    return name_file


# OUTPUT filename
def get_output_filename(archi):
    name_file = "all_tools_result_XeonGold" + archi + ".csv"
    return name_file




# Open all csv and gather the data inside a single datastructure
# Datastructure: [NameBench_[Seq/Par]] -> (autoscheduler, autoTVM, oneDNN, mOpt, tvm_ttile)
#  * If absence of a number, put "0"
def gather_all_data(archi):
    # Order of tools same than the tuple inside the final dictionnary
    l_toolname = ["autoscheduler", "autoTVM", "onednn", "mOpt", "tvm_ttile"]
    l_bench =  [
        "ResNet18_01",
        "ResNet18_02",
        "ResNet18_03",
        "ResNet18_04",
        "ResNet18_05",
        "ResNet18_06",
        "ResNet18_07",
        "ResNet18_08",
        "ResNet18_09",
        "ResNet18_10",
        "ResNet18_11",
        "ResNet18_12",
        "Yolo9000_00",
        "Yolo9000_02",
        "Yolo9000_04",
        "Yolo9000_05",
        "Yolo9000_08",
        "Yolo9000_09",
        "Yolo9000_12",
        "Yolo9000_13",
        "Yolo9000_18",
        "Yolo9000_19",
        "Yolo9000_23"
    ]

    # Extract a dict [Namebench] -> [Time] from the info of a single csv
    def extract_dict_from_single_csv(name_file):
        dic_single_csv = {}

        csv_file = open(name_file, "r")
        csv_reader = csv.reader(csv_file, delimiter=';')

        first_line = True
        for row in csv_reader:
            if first_line:
                first_line = False
                continue

            namebench = row[0]
            time = round(float(row[1]), 4)      # Only 4 digits after the comma
            #std_error = float(row[2])   # Not used in Figure!!!

            dic_single_csv[namebench] = time

        csv_file.close()

        return dic_single_csv

    # Temporary dic: [ toolname -> (dict_seq, dict_par) ]
    dic_temp = {}
    for toolname in l_toolname:

        # Missing parts <============================= TEMPORARY
        #if (toolname == "mOpt"):
        #    continue
        # END TEMPORARY

        # Sequential
        name_file_seq = get_name_file(toolname, False, archi)
        dict_seq = extract_dict_from_single_csv(name_file_seq)

        # Parallel
        name_file_par = get_name_file(toolname, True, archi)
        dict_par = extract_dict_from_single_csv(name_file_par)

        dic_temp[toolname] = (dict_seq, dict_par)

    # DEBUG
    #print(dic_temp)

    # We reorganise dic_temp in order to fit the figure
    # dic_figure: [NameBench_[Seq/Par]] -> (autoscheduler, autoTVM, oneDNN, mOpt, tvm_ttile)
    #  * If absence of a number, put "-1"
    dic_figure = {}
    for namebench in l_bench:
        namebench_seq = namebench + "_Seq"
        tuple_tool_seq = []
        for toolname in l_toolname:
            try:
                time_seq = dic_temp[toolname][0][namebench]
                tuple_tool_seq.append(time_seq)
            except KeyError:
                tuple_tool_seq.append("-1")     # For the holes
        dic_figure[namebench_seq] = tuple_tool_seq

        namebench_par = namebench + "_Par"
        tuple_tool_par = []
        for toolname in l_toolname:
            try:
                time_par = dic_temp[toolname][1][namebench]
                tuple_tool_par.append(time_par)
            except KeyError:
                tuple_tool_par.append("-1")     # For the holes
        dic_figure[namebench_par] = tuple_tool_par

    return dic_figure


# Write the final dictionnary info inside a csv
def write_figure_csv(csv_name, dic_figure):
    output_f = open(csv_name, mode="w")
    output_f.write("NameConv_SP;autoscheduler;autoTVM;oneDNN;mOpt;tvm_ttile\n")

    for name in iter(dic_figure):
        (autoscheduler, autoTVM, oneDNN, mOpt, tvm_ttile) = dic_figure[name]
        output_f.write("{};{};{};{};{};{}\n".format(name, autoscheduler, autoTVM, oneDNN, mOpt, tvm_ttile))

    output_f.close()
    return


# === Main ===
def main():
    l_archi = [
        "5220",     # gros
        "6130"      # dahu
        ]

    for archi in l_archi:
        dic_figure = gather_all_data(archi)
        #print(dic_figure)

        output_name = get_output_filename(archi)
        write_figure_csv(output_name, dic_figure)

    return

# Let's go
main()

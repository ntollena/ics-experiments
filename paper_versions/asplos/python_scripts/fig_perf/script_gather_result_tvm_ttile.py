

# Python script that produces the "result_tvm_ttile_..." files from the "full_result_tvm_ttile_..." files
# Author: Guillaume

import csv

# Input files
filename_fullres1 = "full_result_tvm_ttile_1_and_18_threads_XeonGold5220.csv"
filename_fullres2 = "full_result_tvm_ttile_1_and_32_threads_XeonGold6130.csv"

# Output files
filename_res_seq_1 = "result_tvm_ttile_1_1_avx512_XeonGold5220.csv"
filename_res_par_1 = "result_tvm_ttile_1_18_avx512_XeonGold5220.csv"

filename_res_seq_2 = "result_tvm_ttile_1_1_avx512_XeonGold6130.csv"
filename_res_par_2 = "result_tvm_ttile_1_32_avx512_XeonGold6130.csv"


def summarize_full_result(csv_name):
	# Dictionnary [NameConv] -> ([BestTime], [Std])
	best_result_seq = {}
	best_result_par = {}

	# Auxilliary function to add an entry in dic only if the time is lower
	def add_result_to_dic(dic, name, time, std):
		if name not in dic:
			dic[name] = (time, std)
		else:
			(cand_time, cand_std) = dic[name]
			if (time < cand_time):
				dic[name] = (time, std)
			# else: do nothing (keep candidate in place)
		return


	with open(csv_name) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=';')
		
		first_line = True
		for row in csv_reader:
			if first_line:
				first_line = False
				continue

			namebench = row[2]
			time = float(row[3])
			std_error = float(row[4])
			num_thread = int(row[5])

			#print("{}, {}, {}, {}".format(namebench, time, std_error, num_thread))
			#return

			# Remove the "MobilNet_XX"
			if namebench.startswith("MobilNet"):
				continue

			# Differentiate seq and parallel (assuming only one kind of parallel)
			if (num_thread==1):
				add_result_to_dic(best_result_seq, namebench, time, std_error)
			else:
				add_result_to_dic(best_result_par, namebench, time, std_error)

	return (best_result_seq, best_result_par)


def write_result(csv_name, dic):
	output_f = open(csv_name, mode="w")
	output_f.write("NameConv;Time;Std\n")

	for name in iter(dic):
		(time, std) = dic[name]
		output_f.write("{};{};{}\n".format(name, time, std))

	output_f.close()
	return



def main():
	# Dictionnary [NameConv] -> ([BestTime], [Std])
	(best_result_seq_1, best_result_par_1) = summarize_full_result(filename_fullres1)
	(best_result_seq_2, best_result_par_2) = summarize_full_result(filename_fullres2)

	write_result(filename_res_seq_1, best_result_seq_1)
	write_result(filename_res_par_1, best_result_par_1)

	write_result(filename_res_seq_2, best_result_seq_2)
	write_result(filename_res_par_2, best_result_par_2)

	return


# Let's go
main()

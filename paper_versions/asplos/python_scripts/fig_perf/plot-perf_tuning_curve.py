#!/usr/bin/env python3
import argcomplete, argparse
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import os


# parser = argparse.ArgumentParser(description='Generates png figure to show % of performance peak from given CSVs files. Be careful of your CSV header.')
# parser.add_argument('outputname', help="output file base name (without the .png)")
# parser.add_argument('convsizes', help="path to conv size csv (ics-experiments/common_csv/conv_sizes.csv)")
# parser.add_argument('tvmttile', help="path to tvm+ttile csv")
# parser.add_argument('--debug', help="Add some verbose", action='store_true')
# parser.add_argument('--machine', help="Name of the machine on which have been executed the experiments.", default="XeonGold5220")
# argcomplete.autocomplete(parser)
# args = parser.parse_args()
# debug = args.debug

Machines = {
    "XeonGold6130": {
        "name": "XeonGold6130",
        "fma": 2,
        "vectorsize": 16,
        "freq": 2300000000,
        "cores": 32,
        "socket": 2,
        "avxtype": "avx512",
    },
    "XeonGold5220": {
        "name": "XeonGold5220",
        "fma": 1,
        "vectorsize": 16,
        "freq": 2200000000,
        "cores": 18,
        "socket": 1,
        "avxtype": "avx512",
    },
}

machine = Machines["XeonGold5220"]

machinename = machine["name"]
fma = machine["fma"]
vectorsize = machine["vectorsize"]
freq = machine["freq"]
nbcores = machine["cores"]
nbsockets = machine["socket"]
avxtype = machine["avxtype"]

df_convsize = pd.read_csv(
    "/home/hbrunie/gitlab/ics-experiments/common_csv/conv_sizes.csv"
)
ics_xp_home = "/home/hbrunie/gitlab/ics-experiments/"

writedir = "/home/hbrunie/tests/"

_debug = False


def peakPerf(
    x, y, f, c, h, w, stride, core, fma, vectorsize, freq, in_seconds=True, debug=False
):
    "Returns the best time in ms possible for this convolution on this machine"
    res = float(
        (
            (f * c * h * w * (y / stride) * (x / stride))
            / (vectorsize * fma * core)
            / (freq)
            # * 1000
        )
    )
    if not in_seconds:  ## in milliseconds
        res *= 1000

    if debug:
        print("Computing peak performance:")
        print("f,c,y,x,h,w,stride")
        print(f, c, y, x, h, w, stride)
        print(f"fma {fma} vec {vectorsize} freq {freq} nbcores {core}")
    ## multiplied by 1000 because time is in ms in the csv
    return res


def compute_peak_perf(df_convsize, machine, NameConv, timems, nbthreads, debug):
    conv = df_convsize.loc[df_convsize.name == NameConv]
    core = nbthreads
    if machine == "XeonGold5220":
        fma = 1
    elif machine == "XeonGold6130":
        fma = 2
    peakperf = peakPerf(
        int(conv.x),
        int(conv.y),
        int(conv.f),
        int(conv.c),
        int(conv.h),
        int(conv.w),
        int(conv.stride),
        core,
        fma,
        vectorsize,
        freq,
        debug=debug,
    )
    ## Compute % of peak performance reached for this convolution
    percPeakPerf = float(100 * peakperf / timems)
    if percPeakPerf > 100:
        print(NameConv, timems, nbthreads, debug, peakperf)
        print("Error: % peakPerf > 100%. Have you specified the correct machine?")
        exit(-1)
    return percPeakPerf


autosched_dir = "/paper_versions/asplos/bothGold5220_Gold6130/auto_scheduler_logs/"
nbthreads = 18
machine = "XeonGold5220"
subdir = f"hotcache/log_1_{nbthreads}_avx512_{machine}/"
autosched_dir = (
    "/paper_versions/asplos/bothGold5220_Gold6130/auto_scheduler_logs/" + subdir
)
readcsv = ics_xp_home + autosched_dir + "performance.csv"

df = pd.read_csv(readcsv, sep=",")
fig = go.Figure()
conv2d_list = ["Yolo9000_00"]  ##df.conv2d_name.unique()[21:]:
for conv2d_name in conv2d_list:
    df_local = df.loc[df.conv2d_name == conv2d_name].copy()
    df_local["mean(peakperf)"] = df_local.apply(
        lambda row: compute_peak_perf(
            df_convsize, machine, conv2d_name, row["mean(time_sec)"], nbthreads, _debug
        ),
        axis=1,
    )
    ref_list = list(df_local["mean(peakperf)"])
    max_list = [0] + [max(ref_list[:x]) for x in range(1, 2048)]
    x_list = list(df_local["index"])
    fig.add_trace(
        go.Scatter(
            x=x_list,
            ##TODO error bars based on perPP
            # error_y=dict(type='data', array=dfttile_0['std'].values),
            # y=df_local["mean(peakperf)"],
            y=max_list,
            name=conv2d_name,
            mode="lines",
            # marker_color=color,
            # marker_symbol=symbol
        )
    )


# fig.update_layout(barmode='group', xaxis_tickangle=-45)
# fig.update_layout(legend=dict(
# yanchor="middle",
# y=0.20,
# xanchor="right",
# x=1.00
# ))
fig.update_layout(height=600, width=1200)
fig.update_yaxes(title="% of peak performance", range=[0, 101])
# fig.update_yaxes(title="Time (ms)", range=[0.00001, 1.0], type="log")
fig.update_xaxes(title="Trials Index", range=[0, 150])  # 2048])
fig.write_image(f"{writedir}/Yolo9000_00_limited_150.png")

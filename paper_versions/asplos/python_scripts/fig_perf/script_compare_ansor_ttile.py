import functools
import itertools
import re
import math
import argcomplete, argparse
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
import os


debug = False

# Input files
basedir = os.path.expanduser("./")
#readdir = "/home/nico/ics-experiments/common_csv/"
readdir = basedir
filename_convsizes = readdir + "conv_sizes.csv"
#readdir = "/home/nico/ics-experiments/paper_versions/asplos/Figure_perf_ASPLOS22/"
readdir = basedir
file_name_data_1 = readdir+"all_tools_result_XeonGold5220.csv"
file_name_data_2 = readdir+"all_tools_result_XeonGold6130.csv"

# Output figure
#writedir = "/home/nico/gitlab/ics-experiments/paper_versions/asplos/Figure_perf_ASPLOS22/tests/"
writedir = basedir
output_filename_1 = writedir+"Figure_speedup_over_Ansor_XeonGold5220.png"
output_filename_2 = writedir+"Figure_speedup_over_Ansor_XeonGold6130.png"


# Machine caract
Machines = {
        "XeonGold6130": {
            "name": "XeonGold6130",
            "fma": 2,
            "vectorsize": 16,
            "freq": 2300000000,
            "cores": 32,
            "socket": 2,
            "avxtype": "avx512"
            },
        "XeonGold5220": {
            "name": "XeonGold5220",
            "fma": 1,
            "vectorsize": 16,
            "freq": 2200000000,
            "cores": 18,
            "socket": 1,
            "avxtype": "avx512"
            }
        }



# Graphical background
def add_background(fig,leny,scale=2,start=0.0025):
    """ Add the background grey colored column to differentiate the
        different convolution benchmarks.
        scale is for the column size (steps) and start is for the 
        x-axis coordinate to start the columns.
    """
    # find step size for an interval for the number of y-values
    steps = scale/leny
    # set up where each interval ends
    ends = [start + steps*(e+1) for e in np.arange(0, leny//scale)]
    # container for shapes to be added as backgrounds
    shapes = []
    # super-easy way of making a list for alternating backgrounds
    colors = ['grey', 'rgba(0,0,0,0)']*leny
    # set up shapes for alternating background colors
    for i, e in enumerate(ends):
            shapes.append(dict(type="rect",
                            xref="paper",
                            yref="paper",
                            x0=e-steps,
                            y0=0,
                            x1=e,
                            y1=1,
                            fillcolor=colors[i],
                            opacity=0.5,
                            layer="below",
                            line_width=0,
            )
        )
    ## The grid is removed from xaxis in order to better see the grey columns
    fig.update_layout(xaxis=dict(showgrid=False), shapes=shapes)

def is_seq(x):
    return "Seq" in x

def is_par(x):
    return "Par" in x

# select_par_seq : 0 for par, 1 for seq
def genPlotBar(machine, df_all,  df_convsize, select_par_seq):
    # Extract data from Machines dictionnary
    machinename = machine["name"]
    fma = machine["fma"]
    vectorsize = machine["vectorsize"]
    freq = machine["freq"]
    nbcores = machine["cores"]
    nbsockets = machine["socket"]
    avxtype = machine["avxtype"]
    # List of the tools (extracted from "df_all" columns names)
    name_list = list(df_all.columns)[1:]


    def ratios_from_row(row):
        nameConv_SP = str(row["NameConv_SP"])
        ttile = row["tvm_ttile"]
        ansor = row["autoscheduler"]
        return ansor / ttile

    # Figure title
    nbthreads = nbcores
    if select_par_seq == 0:
        title = f"Parallel ({nbthreads} threads out of {nbcores} cores):"
        title += f"TVM+Ttile vs AutoTVM on {machinename} {nbcores} cores,"
    elif select_par_seq == 1:
        title = f"Sequential:"
        title += f"TVM+Ttile vs AutoTVM on {machinename} ,"
    #title += f"TVM+Ttile vs AutoTVM on {machinename} {nbcores} cores,"
    ghzfreq = freq / 1000000000
    title += f"{nbsockets} socket, {fma} FMA, {avxtype}, {ghzfreq}GHz"
    # Temporary names for the X axis. Ex: "ResNet18_01_Seq"
    def select(x):
        if select_par_seq == 0:
            return is_par(x)
        elif select_par_seq == 1:
            return is_seq(x)
        else:
            return True

    x_namelist = list(filter(select, list(df_all["NameConv_SP"].values)))
    df_all_par = df_all[df_all["NameConv_SP"].isin(x_namelist)].copy()

    def compute_sum_for_network(network_name, bench):
        nw_only = list(filter(lambda x :network_name in x, list(df_all_par["NameConv_SP"].values)))
        df = df_all_par[df_all_par["NameConv_SP"].isin(nw_only)]
        return sum(df[bench])

    def speedup_by_nw(networks):
        sum_ttile = list(map(lambda nw_name: compute_sum_for_network(nw_name, "tvm_ttile"),
            networks))
        sum_ansor =  list(map(lambda nw_name: compute_sum_for_network(nw_name, "autoscheduler"),
            networks))
        def compute_speedup(name_ttile_ansor):
            ttile, ansor = name_ttile_ansor
            return ansor / ttile
        return list(map(compute_speedup, zip(sum_ttile, sum_ansor)))
    df_all_ratios = df_all_par.apply(ratios_from_row, axis = 1, result_type='expand')
    yolo_speedup, resnet_speedup = speedup_by_nw(["Yolo9000", "ResNet"])
    def adapt_for_score_over_threshold(score_list, threshold):
        def text_from_score(s):
            if s < threshold:
                return s, ""
            else:
                return threshold, "{:.2f}".format(s)
        return map(lambda x: text_from_score(x), score_list)

    number_resnets = len(list(filter(lambda x : "ResNet" in x, x_namelist)))
    x_namelist.insert(number_resnets, "ResNet")
    x_namelist.append("Yolo9000")

    df_all_ratios = list(df_all_ratios)
    color_discrete_sequence = ['blue'] * len(df_all_ratios)
    df_all_ratios.insert(number_resnets, resnet_speedup)
    color_discrete_sequence.insert(number_resnets, 'darkblue')
    df_all_ratios.append(yolo_speedup)
    color_discrete_sequence.append('darkblue')

    def unzip(l):
        return [x for (x, _) in l], [y for (_, y) in l]
    df_all_ratios, text_list = unzip(list(adapt_for_score_over_threshold(df_all_ratios, 4)))

    plot = go.Figure(
            data=[
        go.Bar(
            name = 'Speedup Ttile / Ansor',
            x =x_namelist,
            y = df_all_ratios,
            text=text_list,
            textposition="auto",
            marker = dict(color = color_discrete_sequence),
            )
        ])

    plot.add_shape(type='line',
            x0=-0.5,
            y0=1,
            x1=len(x_namelist) - 0.5,
            y1=1,
            line=dict(color='Red',),
            )
    plot.update_layout(title_text = title,title_x = 0.5)
    plot['layout']['title']['font'] = dict(size=10)
    plot.update_yaxes(title="time Ansor / time TVM + Ttile", range=[0,4])
    if select_par_seq == 0:
        par_seq_str = "Par"
    else:
        par_seq_str = "Seq"
    plot.write_image(writedir+f'ttile_speedup_vs_ansor_{machinename}_{par_seq_str}.png')


# =========================================================



def main():
    df_convsize = pd.read_csv(filename_convsizes)

    df_all_1 = pd.read_csv(file_name_data_1, sep=";")
    #genPlot(debug, output_filename_1, Machines["XeonGold5220"], df_all_1, df_convsize, True)

    df_all_2 = pd.read_csv(file_name_data_2, sep=";")
    #genPlot(debug, output_filename_2, Machines["XeonGold6130"], df_all_2, df_convsize, False)

    Par = 0
    Seq = 1
    genPlotBar(Machines["XeonGold5220"], df_all_1, df_convsize, Seq)
    genPlotBar(Machines["XeonGold5220"], df_all_1, df_convsize, Par)
    genPlotBar(Machines["XeonGold6130"], df_all_2, df_convsize, Seq)
    genPlotBar(Machines["XeonGold6130"], df_all_2, df_convsize, Par)

    return


# Let's go
main()

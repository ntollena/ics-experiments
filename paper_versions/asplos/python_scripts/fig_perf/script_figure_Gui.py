import numpy as np
import pandas as pd
import plotly.graph_objects as go


from types import SimpleNamespace as ns

debug = False


def geo_mean(values):
    values = list(values)
    # values = list(filter(lambda x: x >= 0.1, values))
    # prod = functools.reduce(lambda x, y: x * y, values, 1)
    # return prod ** (1 / len(values))

    # Assume that the even values are for "seq" and the odd ones are for "par"
    values_seq = 1.0
    num_val_seq = 0
    values_par = 1.0
    num_val_par = 0

    for i in range(0, len(values)):
        v = values[i]

        # Skip the values that we do not have (Mopt, looking at you!)
        if v < 0.1:
            continue

        if i % 2 == 0:
            values_seq = values_seq * v
            num_val_seq += 1
        else:
            values_par = values_par * v
            num_val_par += 1

    average_seq = values_seq ** (1 / num_val_seq)
    average_par = values_par ** (1 / num_val_par)

    return [average_seq, average_par]


# Input files
readdir = "/home/hbrunie/gitlab/ics-experiments/common_csv/"
filename_convsizes = readdir + "conv_sizes.csv"
readdir = (
    "/home/hbrunie/gitlab/ics-experiments/paper_versions/asplos/Figure_perf_ASPLOS22/"
)
file_name_data_1 = readdir + "all_tools_result_XeonGold5220_fromlogs.csv"
file_name_data_2 = readdir + "all_tools_result_XeonGold6130_fromlogs.csv"

# Output figure
# writedir = "/home/hbrunie/gitlab/ics-experiments/paper_versions/asplos/Figure_perf_ASPLOS22/tests/"
writedir = "/home/hbrunie/Pictures/"
output_filename_1 = writedir + "Figure_result_XeonGold5220.png"
output_filename_2 = writedir + "Figure_result_XeonGold6130.png"

# Machine caract
Machines = {
    "XeonGold6130": {
        "name": "XeonGold6130",
        "fma": 2,
        "vectorsize": 16,
        "freq": 2300000000,
        "cores": 32,
        "socket": 2,
        "avxtype": "avx512",
    },
    "XeonGold5220": {
        "name": "XeonGold5220",
        "fma": 1,
        "vectorsize": 16,
        "freq": 2200000000,
        "cores": 18,
        "socket": 1,
        "avxtype": "avx512",
    },
}


# To get the peak perfs
def peakPerf(x, y, f, c, h, w, stride, core, fma, vectorsize, freq, debug=False):
    "Returns the best time in ms possible for this convolution on this machine"
    if debug:
        print("Computing peak performance:")
        print("f,c,y,x,h,w,stride")
        print(f, c, y, x, h, w, stride)
        print(f"fma {fma} vec {vectorsize} freq {freq} nbcores {core}")
    ## multiplied by 1000 because time is in ms in the csv
    return float(
        (
            (f * c * h * w * (y / stride) * (x / stride))
            / (vectorsize * fma * core)
            / (freq)
            * 1000
        )
    )


# Graphical background
def add_background(fig, leny, scale=2, start=0.0025):
    """Add the background grey colored column to differentiate the
    different convolution benchmarks.
    scale is for the column size (steps) and start is for the
    x-axis coordinate to start the columns.
    """
    # find step size for an interval for the number of y-values
    steps = scale / leny
    # set up where each interval ends
    ends = [start + steps * (e + 1) for e in np.arange(0, leny // scale)]
    # container for shapes to be added as backgrounds
    shapes = []
    # super-easy way of making a list for alternating backgrounds
    colors = ["grey", "rgba(0,0,0,0)"] * leny
    # set up shapes for alternating background colors
    for i, e in enumerate(ends):
        shapes.append(
            dict(
                type="rect",
                xref="paper",
                yref="paper",
                x0=e - steps,
                y0=0,
                x1=e,
                y1=1,
                fillcolor=colors[i],
                opacity=0.5,
                layer="below",
                line_width=0,
            )
        )
    ## The grid is removed from xaxis in order to better see the grey columns
    fig.update_layout(xaxis=dict(showgrid=False), shapes=shapes)


# Generate the PNG figure
def genPlot(debug, outfname, machine, df_all, df_convsize, legendBottom):

    # Function that will be mapped to all cells of df_all
    def compute_peak_perf(NameConv_SP, timems, machine, debug):
        # Extract data from Machines dictionnary
        fma = machine.fma
        vectorsize = machine.vectorsize
        freq = machine.freq
        nbcores = machine.cores
        # First column of the df is the names => skip
        if str(type(timems)) == "<class 'str'>":
            return timems

        ## First column is the index
        if str(type(timems)) == "<class 'int'>":
            return timems

        # NO DATA AVAILABLE
        if timems == -1.0:  # or (timems == "ERROR"): ERROR Should have been removed
            return 0.0

        # Get the number of thread from name of bench + machine (if par, numthread = numcores)
        assert NameConv_SP.endswith("_Seq") or NameConv_SP.endswith("_Par")
        nbthreads = 1
        if NameConv_SP.endswith("_Par"):
            nbthreads = nbcores

        # Remove the "_Seq" or "_Par" to fit "conv_sizes.csv"
        NameConv = NameConv_SP.split("_Seq")[0]
        NameConv = NameConv.split("_Par")[0]

        # Get the convolution sizes
        conv = df_convsize.loc[df_convsize.name == NameConv]

        core = nbthreads
        # print(conv)
        peakperf = peakPerf(
            int(conv.x),
            int(conv.y),
            int(conv.f),
            int(conv.c),
            int(conv.h),
            int(conv.w),
            int(conv.stride),
            core,
            fma,
            vectorsize,
            freq,
            debug=debug,
        )

        ## Compute % of peak performance reached for this convolution
        percPeakPerf = float(100 * peakperf / float(timems))
        if percPeakPerf > 100:
            print(NameConv_SP, timems, nbthreads, debug, peakperf)
            print("Error: % peakPerf > 100%. Have you specified the correct machine?")
            exit(-1)

        return percPeakPerf

    def sumPeakPerf(resnet_name, machine, seq):
        df_conv = df_convsize[df_convsize["name"].str.contains(resnet_name)].copy()
        sum_timems = 0
        if seq:
            nbthreads = 1
        else:
            nbthreads = machine.cores
        for index, row in df_conv.iterrows():
            sum_timems += peakPerf(
                int(row.x),
                int(row.y),
                int(row.f),
                int(row.c),
                int(row.h),
                int(row.w),
                int(row.stride),
                nbthreads,
                machine.fma,
                machine.vectorsize,
                machine.freq,
                debug=debug,
            )
        return sum_timems

    # Mapping compute_perk_perf
    def from_row(row):
        # Note: "row" is a pandas.Series
        nameConv_SP = str(row["NameConv_SP"])

        def from_elem(time):
            return compute_peak_perf(nameConv_SP, time, machine, debug)

        nrow = row.apply(from_elem)
        return nrow

    # Note: df_all (csv) : NameConv_SP | autoscheduler | autoTVM | oneDNN | mOpt | tvm_ttile

    # List of the tools (extracted from "df_all" columns names)
    name_list = list(df_all.columns)[1:]

    # Geo mean for seq / par
    df_local_resnet = df_all[df_all["NameConv_SP"].str.contains("ResNet")].copy()
    num_resnet = len(df_local_resnet)
    df_local_yolo = df_all[df_all["NameConv_SP"].str.contains("Yolo9000")].copy()
    final_add = []
    for df_local, net_name in [
        (df_local_resnet, "ResNet"),
        (df_local_yolo, "Yolo9000"),
    ]:
        lmeantime_seq = [f"MeanTime_{net_name}_Seq"]
        lmeantime_par = [f"MeanTime_{net_name}_Par"]

        for col in df_local.columns:
            if col == "NameConv_SP":
                continue
            lvalues = list(df_local[col])
            seqvalues = lvalues[0::2]
            parvalues = lvalues[1::2]
            seqmean = sumPeakPerf(net_name, machine, seq=True) / sum(seqvalues) * 100.0
            parmean = sumPeakPerf(net_name, machine, seq=False) / sum(parvalues) * 100.0
            if -1.0 in seqvalues:
                lmeantime_seq += ["NA"]
            else:
                lmeantime_seq += [seqmean]
            if -1.0 in parvalues:
                lmeantime_par += ["NA"]
            else:
                lmeantime_par += [parmean]
        final_add.append(lmeantime_seq)
        final_add.append(lmeantime_par)

    # === Convert time into peak_perf in "df_all" ===

    df_all_pp = df_all.apply(from_row, axis=1, result_type="expand").copy()
    # print(df_all_pp)

    # Geometric mean
    # OLD
    # df_all_mean = df_all_pp.loc[:, df_all_pp.columns != "NameConv_SP"].apply(geo_mean, axis = 0)
    # print(df_all_mean)

    # Geo mean for seq / par
    lgeomean_seq = ["GeoMean_Seq"]
    lgeomean_par = ["GeoMean_Par"]

    for col in df_all_pp.columns:
        if col == "NameConv_SP":
            continue
        lvalues = list(df_all_pp[col])
        lmean = geo_mean(lvalues)
        seq_mean = lmean[0]
        par_mean = lmean[1]

        lgeomean_seq.append(seq_mean)
        lgeomean_par.append(par_mean)

    df_all_pp.loc[num_resnet] = final_add.pop(0)
    df_all_pp.loc[num_resnet + 1] = final_add.pop(0)
    df_resnet_pp = df_all_pp.iloc[: num_resnet + 2].copy()
    df_yolo_pp = df_all_pp.iloc[num_resnet + 2 :].copy()
    df_yolo_pp.loc[len(df_yolo_pp.index)] = final_add.pop(0)
    df_yolo_pp.loc[len(df_yolo_pp.index)] = final_add.pop(0)
    df_yolo_pp.loc[len(df_yolo_pp.index)] = lgeomean_seq
    df_yolo_pp.loc[len(df_yolo_pp.index)] = lgeomean_par

    df_all_pp = pd.concat([df_resnet_pp, df_yolo_pp], ignore_index=True)

    print(df_all_pp)

    # Figure title
    nbthreads = machine.cores
    title = (
        f"Sequential --- Parallel ({nbthreads} threads out of {machine.cores} cores):"
    )
    title += f"TVM+Ttile vs AutoTVM on {machine.name} {machine.cores} cores,"
    ghzfreq = machine.freq / 1000000000
    title += (
        f"{machine.socket} socket, {machine.fma} FMA, {machine.avxtype}, {ghzfreq}GHz"
    )

    fig = go.Figure()

    # Temporary names for the X axis. Ex: "ResNet18_01_Seq"
    x_namelist = df_all_pp["NameConv_SP"].values
    assert len(x_namelist) % 2 == 0

    colors = [
        "indianred",
        "blue",
        "green",
        "orange",
        "purple",
        # "yellow",
        "black",
        "grey",
    ]
    symbols = [
        "square",
        "diamond",
        "circle",
        "circle-x-open",
        "diamond-open",
        "cross",
        "square-open",
    ]

    assert (len(colors) >= (len(name_list) - 1)) and (
        len(symbols) >= (len(name_list) - 1)
    ), f" add more color {len(colors)} and symbols {len(symbols)} to match {len(name_list)-1}"

    # For each curve inside the Figure
    for (name, symbol, color) in zip(name_list, symbols, colors):
        # DEBUG
        if debug:
            print(f"Adding trace: {name}")
            lendf = len(df_all_pp[name].values)
            lenname = len(x_namelist)
            print(lendf, lenname)

        # Add the points
        fig.add_trace(
            go.Scatter(
                x=x_namelist,
                y=df_all_pp[name].values,
                name=name,
                mode="markers",
                marker_color=color,
                marker_symbol=symbol,
            )
        )

        # Add the lines
        for i in range(0, len(x_namelist) - 1, 2):
            fig.add_trace(
                go.Scatter(
                    x=x_namelist[i : i + 2],
                    y=df_all_pp[name].values[i : i + 2],
                    name=name,
                    mode="lines",
                    marker_color=color,
                    showlegend=False,
                )
            )

    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode="group", xaxis_tickangle=-45)

    if legendBottom:
        fig.update_layout(legend=dict(yanchor="bottom", y=0, xanchor="left", x=1.00))
    else:
        fig.update_layout(
            legend=dict(yanchor="bottom", y=0.75, xanchor="right", x=1.00)
        )

    # Add the grey vertical rectangles
    add_background(fig, len(x_namelist))
    fig.update_layout(height=600, width=1200)
    fig.update_yaxes(title="% of peak performance", range=[0, 101])

    # Better X-axis legend (with one name on 2)
    x_better_namelist = []
    for x_name in x_namelist:
        if x_name.endswith("_Par"):
            x_better_namelist.append(" ")  # Empty legend when parallel
        else:
            assert x_name.endswith("_Seq")
            new_x_name = x_name.split("_Seq")[0]
            x_better_namelist.append(new_x_name)  # Just the name when sequential

    num_tick_val = len(df_all_pp) - 1
    fig.update_xaxes(
        title=title,
        tickmode="array",
        tickvals=[x + 0.25 for x in range(num_tick_val + 1)],  # TODO: check that
        ticktext=x_better_namelist,
        ##TODO: update range when adding Yolo9000_23 and full CNNs
        range=[-0.5, (num_tick_val + 0.5)],
    )

    # fig.show()
    fig.write_image(f"{outfname}")


# =========================================================


def main(fromLogs=True):
    """
    if fromLogs == False, build csv from evaluation of schedule, with evaluation
    framework.
    Otherwise based on TVM intern evaluation which generates log files.
    """
    if fromLogs:
        autosched_str = "autoscheduler_fromlogs"
        autotvm_str = "autotvm_fromlogs"
    else:
        autosched_str = "autoscheduler"
        autotvm_str = "autotvm"
    df_convsize = pd.read_csv(filename_convsizes)

    df_all_1 = pd.read_csv(file_name_data_1, sep=",", index_col=False)
    df_all_1 = df_all_1[
        [
            "NameConv_SP",
            autosched_str,
            autotvm_str,
            "oneDNN",
            "mOpt",
            "tvm_ttile",
        ]
    ]
    machine = ns(**Machines["XeonGold5220"])
    genPlot(debug, output_filename_1, machine, df_all_1, df_convsize, True)

    df_all_2 = pd.read_csv(file_name_data_2, sep=",", index_col=False)
    df_all_2 = df_all_2[
        [
            "NameConv_SP",
            autosched_str,
            autotvm_str,
            "oneDNN",
            "mOpt",
            "tvm_ttile",
        ]
    ]
    machine = ns(**Machines["XeonGold6130"])
    genPlot(debug, output_filename_2, machine, df_all_2, df_convsize, False)


# Let's go
main()

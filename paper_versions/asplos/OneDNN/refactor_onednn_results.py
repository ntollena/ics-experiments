#!/usr/bin/env python3
import pandas as pd
import argcomplete, argparse
import os
import glob

parser = argparse.ArgumentParser(description='Merge all csv from the directory defined by path.')
parser.add_argument('outputname', help="Precise basename for the csv output file.")
parser.add_argument('--nbthread_list', type=int, help="Precise absolute or relative path for the directory full of csv.", nargs='+')
parser.add_argument('--path_list', help="Precise absolute or relative path for the directory full of csv.", nargs='+')
parser.add_argument('--debug', help="Enable debug verbose", action='store_true')
argcomplete.autocomplete(parser)
args = parser.parse_args()
csvoutname = args.outputname
first = True
mode = 'w'
header=True
if args.debug:
    print(args.nbthread_list, args.path_list)

import inspect

def getLineInfo():
    print(inspect.stack()[1][1],": line ",inspect.stack()[1][2],":",
          inspect.stack()[1][3])

## Merge all data to same format.
for nbthread,path in zip(args.nbthread_list, args.path_list):
    if args.debug:
        print(nbthread,path)
    for filename in glob.glob(os.path.join(path, "*.csv")):
        if args.debug:
            print(f"path: {path} read {filename} and write into {csvoutname} / nbthreads = {nbthread}")
        df = pd.read_csv(filename, sep=",") 
        ## Some csv contains more columns, thus we cut and only extract the one of interest for us,
        ## so that we generate a valid csv with the same number of columns for each row.
        if nbthread == 1:
            if args.debug:
                print(df)
                print(df.columns)
            df = df[['NameConv', 'min_wlast(ms)']]
            df['nbthreads'] = nbthread
            df['Time(ms)'] = df['min_wlast(ms)']
            ## TODO: get real std deviation
            print("NO real std dev for OneDNN in sequential")
            getLineInfo()
            df['std'] = [0] * len(df['min_wlast(ms)'].values)
            df = df[['NameConv', 'Time(ms)', 'nbthreads', 'std']]
        else: 
            assert nbthread == 18
            if args.debug:
                print(df)
            df = df[['NameConv', 'mean_wlast', 'stddev_wlast']]
            df['nbthreads'] = nbthread
            df['Time(ms)'] = df['mean_wlast']
            df['std'] = df['stddev_wlast']
            df = df[['NameConv', 'Time(ms)', 'nbthreads','std']]
        if args.debug:
            print("Dumping", df)
        df.to_csv(csvoutname,header=header, mode=mode,sep=",", index=False)
        header = False
        mode = 'a'
print(csvoutname,"generated.")

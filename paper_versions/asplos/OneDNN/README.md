BE AWARE: OneDNN results are a mess.
====================================

Use refactor like this:
- `python3 refactor_onednn_results.py new_csv_filename.csv --nbthread_list 1 18 --path_list ./oneDNN_seq_avx512_Gold5220/ ./oneDNN_par_avx512_Gold5220/`
- it won't work with other argument values

Refactoring Gold6130 data:
```
../python_scripts/refactor_tvm-ttile-csv.py onednn_avx512_gold6130.csv , --change_header --nbthread_list 1 32 --path_list oneDNN_seq_avx512_Gold6130/dahu-hot-cache/dahu-seq-hot-cache.csv oneDNN_par_avx512_Gold6130/dahu/dahu-par-hot-cache.csv --header_list NameConv mean_flast stddev_flast mean_wlast stddev_wlast --sep_out ,
```

All are extracted from `./perf_ref/onednn/par/avx512/nancy-gros` and `./perf_ref/onednn/par/avx512/nancy-gros`
BE AWARE: I have changed some headers from their original names.

But still, none of the file has the same CSV header originally:
* `oneDNN_par_avx512_Gold5220/nancy.csv`
`Name,mean_flast,stddev_flast,mean_wlast,stdddev_wlast`

* `oneDNN_seq_avx512_Gold5220/benchdnn_seq_gros_3db1ff.csv`
`name,min(ms),avg(ms)`

* `oneDNN_seq_avx512_Gold5220/benchdnn_seq_gros_9e67d40.csv`
`name,min(ms),avg(ms)`

* `oneDNN_seq_avx512_Gold5220/nancy_seq.csv `
`name,avg(ms)`

* `oneDNN_seq_avx512_Gold5220/onednn_gros_seq.csv`
`name,min_flast(ms),avg_flast(ms),min_wlast(ms),avg_wlast(ms)`

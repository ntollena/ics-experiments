# ASPLOS paper submission.

## Content

* All python scripts are in the subdirectory `python_scripts`
* autoscheduler (+logs) and autotvm one csv for all data are in `bothGold5220_Gold6130`
* all csv (1 csv **per** machine **per** nbthreads) to plot paper's performance graph are in `Figure_perf_ASPLOS22`
* all csv (1 csv **per** library used) are in `gold5220` and `gold6130`
* figure showing perfromance results for different sizes of Matrix Multiplications are in `small_mm_figure_Gui`
* Some generated pngs are in `pngs` (TODO delete this directory)

## TODO

* Add to the current documentation how to generate charts for:
  1. mOpt
  2. FlexTensor

* Refactor the directory relative positions:
  1. Right now, **brut data** are all around: `../../perf_schemes/` contains `tvm_ttile` data
  2. `../../common_csv/conv_sizes.csv` contains benchmark conv2d dimension sizes.
  3. `./bothGold5220_Gold6130` contains data from autotvm and autoscheduler.
  4. `./bothGold5220_Gold6130/auto_scheduler_logs` contains **brut data** from autoscheduler log files.

## Recent modifications:

### 10/05/2022 (date in French order)

* Thanks to next paragraph modifications (`07/02/2022`) we have created 2 new csv files
* Creation of files:
    - `Figure_perf_ASPLOS22/all_tools_result_XeonGold5220_fromlogs.csv`
    - `Figure_perf_ASPLOS22/all_tools_result_XeonGold6130_fromlogs.csv`
* BE AWARE: this fromlogs results are not consistent with Conv2d evaluation
with concern of AutoTVM: indeed the internal evaluation for logs is only focused
on a small microKernel inside the conv2d computation (see TVM topi conv2d schedule
definition)


### 07/02/2022  (date in French order)

* To generate the `autoscheduler` csv from log files use the python script: 
  `python3 ./python_scripts/generate_autosched_resfromlog.py`
* It will generate another csv file in `bothGold5220_Gold6130` subdirectory called
`results_all_autoscheduler_fromlogs.csv`
* You can then use this csv file instead of the one mentionned inside the `Full png generation`
section of this README.

# Instruction to create the paper's charts

## To (re)generate the csv:

1. refactor tvm+ttile csv from brut data
    - python python_scripts/refactor_tvm-ttile-csv.py --help
    - XEONGOLD5220 example: `./python_scripts/refactor_tvm-ttile-csv.py ./gold5220/tvm_ttile_only_1_and_18_threads.csv --nbthread_list 1 18 --path_list ../../perf_schemes/avx512/XeonGold5220/1/  ../../perf_schemes/avx512/XeonGold5220/18/random/`
    - You could replace random with metric to get the TVM+TTILE results thanks to the metric implemented in `matmul_bench`.
    - XEONGOLD6130 example: `./python_scripts/refactor_tvm-ttile-csv.py gold6130/tvm_ttile_only_1_and_32_threads.csv --nbthread_list 1 32 --path_list ../../perf_schemes/avx512/XeonGold6130/1/  ../../perf_schemes/avx512/XeonGold6130/32/random/`

## Results for XeonGold6130:

* Full png generation:
 - `./python_scripts/plot-perf.py chart_6130 ../../common_csv/conv_sizes.csv gold6130/tvm_ttile_only_1_and_32_threads.csv --machine XeonGold6130 --onednn gold6130/results_all_onednn.csv  --autotvm bothGold5220_Gold6130/results_all_autotvm.csv --autoscheduler bothGold5220_Gold6130/results_all_autoscheduler.csv`

* To generate with autoscheduler from log files:
 - `--autoscheduler bothGold5220_Gold6130/results_all_autoscheduler_fromlogs.csv`

## Results for XeonGold5220:

* Full png generation:
 - `./python_scripts/plot-perf.py chart_5220 ../../common_csv/conv_sizes.csv gold5220/tvm_ttile_only_1_and_18_threads.csv --machine XeonGold5220 --onednn gold5220/results_all_onednn.csv  --autotvm bothGold5220_Gold5220/results_all_autotvm.csv --autoscheduler bothGold5220_Gold5220/results_all_autoscheduler.csv`

* To generate with autoscheduler from log files:
 - `--autoscheduler bothGold5220_Gold6130/results_all_autoscheduler_fromlogs.csv`

## Problem with OneDNN results:

* On Gold6130 (dahu) results looks too bad to be true: `./OneDNN/oneDNN_seq_avx512_Gold6130/dahu-hot-cache/dahu-seq-hot-cache.csv`
    - using these data with `compute_peak_performance`, the % of peak performance is close to 0.
    - Instead we are using results from `OneDNN/oneDNN_par_avx512_Gold6130/onednn-eval-grid5000-Gold6130/results_32threads_2sockets_formatted.csv` which have been hand copied to `./gold6130/results_all_onednn.csv` and the column PerPP added.
    - Thus in `python_scripts/plot-perf.py` you can read (line 203):
    ``` python
        if name == "OneDNN" and machine["name"] == "XeonGold6130":
            df['perPP'] = df[["PerPP", "perPP"]].max(axis=1)
    ```
    which replaced results computed from Time(ms) and `compute_peak_performance` to this copied column.
    - Feel free to try other csv if you wanna check the different results available for OneDNN.

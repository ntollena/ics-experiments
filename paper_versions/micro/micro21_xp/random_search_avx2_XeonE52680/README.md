# Bench

CPU: Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz
Script: stephane_search (branche grid5000_avx2)

Dossier 1/ pour chaque convolution test sur 1500 configurations différentes.

Dossier 2/ pour chaque convolution test sur 1500 configurations sauf ResNet18_8 (1419) et ResNet18_{9, 10, 11, 12} (0)

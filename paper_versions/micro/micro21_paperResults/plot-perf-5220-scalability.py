import pandas as pd
import argcomplete, argparse
import os

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('nbcores', help="nb cores (18 par, 1 seq)")
parser.add_argument('convsizes', help="path to conv size csv")
parser.add_argument('tvmttile', help="path to tvm+ttile csv")
parser.add_argument('autotvm', help="path to autotvm csv")
parser.add_argument('onednn', help="path to oneDNN csv")
parser.add_argument('--onednn_bool', help="Optional: set true to plot oneDNN too", action='store_true', default=False)
argcomplete.autocomplete(parser)
args = parser.parse_args()
oneDNN = args.onednn_bool

d = pd.read_csv(args.convsizes)

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    ## multiplied by 1000 because time is in ms in the csv
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)


def genPlot(outfname,dfttile,dfautotvm,dfonednn,core,fma,vectorsize,freq, parmethod=0, oneDNN=False):
    ## TODO: std should be in perPP
    dfttile_0 = dfttile.loc[dfttile['MethodParallelization'] == 0].copy()
    dfttile_1 = dfttile.loc[dfttile['MethodParallelization'] == 1].copy()
    dfttile_2 = dfttile.loc[dfttile['MethodParallelization'] == 2].copy()
    dfttile_0 = dfttile_0.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    dfttile_1 = dfttile_1.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    dfttile_2 = dfttile_2.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    alist=[dfttile_0,dfttile_1,dfttile_2,dfautotvm]
    concat = []
    if oneDNN:
        alist += [dfonednn]
    for k,df in enumerate(alist):
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl
        #concat+=[df[['NameConv','perPP','std']]]
    #concat = [dfttile[['NameConv','perPP','std']],dfautotvm[['NameConv','perPP','std']]]
    #if oneDNN:
    #    concat += [onednn[['NameConv','perPP','std']]]
    
    import plotly.graph_objects as go
    
    title = "Parallel (18 threads): TVM+Ttile vs AutoTVM on XeonGold5220 18 cores, 1 socket, 1 FMA, avx512, 2.2GHz"
    fig = go.Figure()
    #df = pd.concat(concat)

    fig.add_trace(go.Bar(
        x=dfttile_0['NameConv'].values,
        error_y=dict(type='data', array=dfttile_0['std'].values),
        y=dfttile_0['perPP'].values,
        name='TVM+TTILE (same order as ioopt, but largest subset no reduiction)',
        marker_color='indianred'
    ))
    fig.add_trace(go.Bar(
        x=dfttile_1['NameConv'].values,
        error_y=dict(type='data', array=dfttile['std'].values),
        y=dfttile_1['perPP'].values,
        name='TVM+TTILE (parallelize everything out of tensorize, reduction inner most)',
        marker_color='lightsalmon'
    ))
    fig.add_trace(go.Bar(
        x=dfttile_2['NameConv'].values,
        error_y=dict(type='data', array=dfttile['std'].values),
        y=dfttile_2['perPP'].values,
        name='TVM+TTILE (parallelize everything out tensorize, reduction outter most)',
        marker_color='#fe4d00'
    ))
    #, '#EF553B', '#00CC96', '#AB63FA'' 
    fig.add_trace(go.Bar(
        x=dfautotvm['NameConv'].values,
        y=dfautotvm['perPP'].values,
        error_y=dict(type='data', array=dfautotvm['std'].values),
        name='AutoTVM',
        marker_color='#636EFA'
    ))

    #fig.add_trace(go.Bar(
    #    x=dfonednn_flast['NameConv'].values,
    #    y=dfonednn_flast['perPP'].values,
    #    error_y=dict(type='data', array=dfonednn_flast['std'].values),
    #    name='onednn_clast',
    #    marker_color='green'
    #))

    if oneDNN:
        fig.add_trace(go.Bar(
            x=dfonednn['NameConv'].values,
            y=dfonednn['perPP'].values,
            error_y=dict(type='data', 
                array=dfonednn['std'].values*dfonednn['perPP'].values/100.),
            name='onednn_wlast',
            marker_color='green'
        ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(legend=dict(
    yanchor="top",
    y=1.30,
    xanchor="left",
    x=0.31
    ))
    fig.update_layout(height=600, width=1200)
    #fig.update_layout(title=dict("text":title))
    fig.update_yaxes(title="% of peak performance")
    fig.update_xaxes(title=title)
    fig.write_image(f'{outfname}.png')

## AVX512 Gold
dfautotvm = pd.read_csv(args.autotvm, sep=";")
dfttile = pd.read_csv(args.tvmttile,sep=";")
if oneDNN:
    dfonednn = pd.read_csv(args.onednn,sep=",")
else:
    dfonednn = None
## sequential or Parallel
genPlot(args.outputname,dfttile,dfautotvm,dfonednn,core=int(args.nbcores),fma=1,vectorsize=16,freq=2200000000,oneDNN=oneDNN)
## Obsolete
#genPlot("XeonGold5220-avx512-micro21rebuttal-sequential",dfttile,dfautotvm,dfonednn,core=1,fma=1,vectorsize=16,freq=2200000000)

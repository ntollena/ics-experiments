#!/usr/bin/env python
import pandas as pd
import argcomplete, argparse
import os

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('convsizes', help="path to conv size csv")
parser.add_argument('matmulbench_with_flush', help="path to measure with a call to cache flush")
parser.add_argument('matmulbench_without_flush', help="path to measure without a call to cache flush")
parser.add_argument('matmulbench_papi_peak_perf_flush', help="path to peak perf percents measured with papi")
parser.add_argument('matmulbench_papi_peak_perf_no_flush', help="path to peak perf percents measured with papi")
parser.add_argument('benchdnn', help="path to BenchDNN csv")
#parser.add_argument('ttile', help="path to tvm+ttile csv")
argcomplete.autocomplete(parser)
args = parser.parse_args()

d = pd.read_csv(args.convsizes)

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    ## multiplied by 1000 because time is in ms in the csv
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)

def genPlot(outfname, dftime_flush, dftime_no_flush, dfpapi_flush, dfpapi_no_flush, dfbenchdnn,
        fma, vectorsize, freq):
    ##BEST of all parallelization techniques
    #dfttile_0 = dfttile.loc[dfttile['MethodParallelization'] == 0].copy()
    core = 1
    seqlist = [dftime_flush, dftime_no_flush, dfbenchdnn]
    time_field = "avg(ms)"
    for df in seqlist:
        def val(field):
            return field.values[0]

        def call_pp(name):
            d_row = d.loc[d.name == name]
            return peakPerf(
                    val(d_row.f),
                    val(d_row.c),
                    val(d_row.h),
                    val(d_row.w),
                    val(d_row.y),
                    val(d_row.stride),
                    val(d_row.x),
                    core, fma, vectorsize, freq)
        peakperf = map(call_pp, df.name.values)
        PPl = [100*peak/cycles for cycles, peak in zip(df[time_field].values, peakperf)]
        df['perPP'] = PPl

    for df in (dfpapi_flush, dfpapi_no_flush):
        df['perPP'] = df['wlast']

    import plotly.graph_objects as go
    
    title = "Comparison OneDNN: papi vs clock_gettime, flush and no flush"
    fig = go.Figure()
    #colors = ['#085dee', '#1550b6', '#04f116','#15b61a','#ee8908','#b96b07']
    #colors = ['#085dee','#04f116', '#1550b6', '#15b61a']
    colors = ["Blue", "Red", "Green", "Purple", "Black"]
    namelist = [
            "clock_gettime with flush",
            "clock_gettime without flush",
            "Papi with flush",
            "Papi without flush",
            "Benchdnn"
            ]

    #for color,name,df in zip(colors,namelist,[dfttile1,dfautotvm1,dfttile32, dfautotvm32]):
    #    fig.add_trace(go.Bar(
    #        x=df['NameConv'].values,
    #        error_y=dict(type='data', array=df['std'].values),
    #        y=df['perPP'].values,
    #        name=name,
    #        marker_color=color
    #    ))

    seqlist = [dftime_flush, dfpapi_flush, dftime_no_flush, dfpapi_no_flush, dfbenchdnn]

    for color, name, dfonednn in zip(colors, namelist, seqlist):
        print(name)
        fig.add_trace(go.Bar(
            x=dfonednn['name'].values,
            y=dfonednn['perPP'].values,
            #error_y=dict(type='data', 
            #    #array=dfonednn['std'].values*dfonednn['perPP'].values/100.),
            #    array=dfonednn['std'].values),
            name=name,
            marker_color=color
        ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(legend=dict(
    yanchor="middle",
    y=0.50,
    xanchor="right",
    x=1.31
    ))
    fig.update_layout(height=600, width=1200)
    #fig.update_layout(title=dict("text":title))
    fig.update_yaxes(title="% of peak performance")
    fig.update_xaxes(title=title)
    fig.write_image(f'{outfname}.png')

dfmatmul_paper_cache_flush = pd.read_csv(args.matmulbench_with_flush, sep=",")
dfmatmul_paper_no_cache_flush = pd.read_csv(args.matmulbench_without_flush, sep=",")
dfmatmul_paper_papi_no_flush = pd.read_csv(args.matmulbench_papi_peak_perf_no_flush, sep=",")
dfmatmul_paper_papi_flush = pd.read_csv(args.matmulbench_papi_peak_perf_flush, sep=",")
dfbenchdnn = pd.read_csv(args.benchdnn, sep=",")
#dfmatmul_paper = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/onednn_nancy_seq.csv",sep=",")

genPlot(args.outputname, dfmatmul_paper_cache_flush, dfmatmul_paper_no_cache_flush,
        dfmatmul_paper_papi_flush, dfmatmul_paper_papi_no_flush,
        dfbenchdnn,
        fma=2,vectorsize=16,freq=2100000000)

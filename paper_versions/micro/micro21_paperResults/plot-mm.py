#!/usr/bin/env python
import pandas as pd
import argcomplete, argparse
import plotly.graph_objects as go

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('mmcsvfile', help="csv where lambda vs blis-like tactic are to be found")
parser.add_argument('mklcsvfile', help="csv where lambda vs blis-like tactic are to be found")
#parser.add_argument('ttile', help="path to tvm+ttile csv")
argcomplete.autocomplete(parser)
args = parser.parse_args()


def genPlot(outfname, dfmm, dfmkl):
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_us"],
                  mode="markers",
                  name="Lambda/div"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_blis_for"],
                  mode="markers",
                  name="Blis-like with for-partial-tile"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_blis_unroll"],
                  mode="markers",
                  name="Blis-like with unroll-partial-tile"))
    fig.add_trace(go.Scatter(x=dfmkl['m'], y=dfmkl["peakperf(%)"],
                  mode="markers",
                  name="Mkl"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["blis"],
                  mode="markers",
                  name="Blis"))
    fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["xsmm"],
                  mode="markers",
                  name="xsmm"))
    fig.update_yaxes(title="% of peak performance")
    fig.update_xaxes(title="I")
    fig.show()
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.write_image(f'{outfname}.png')

dfmm = pd.read_csv(args.mmcsvfile, sep=",")
dfmkl = pd.read_csv(args.mklcsvfile, sep=",")
#dfmatmul_paper = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/onednn_nancy_seq.csv",sep=",")

genPlot(args.outputname, dfmm, dfmkl)

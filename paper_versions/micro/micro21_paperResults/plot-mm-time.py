#!/usr/bin/env python
import pandas as pd
import argcomplete, argparse
import plotly.graph_objects as go

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('mmcsvfile', help="csv where lambda vs blis-like tactic are to be found")
parser.add_argument('mklcsvfile', help="csv where lambda vs blis-like tactic are to be found")
#parser.add_argument('ttile', help="path to tvm+ttile csv")
argcomplete.autocomplete(parser)
args = parser.parse_args()

freq = 2_100


def getTimeFromPeakPerf(m, pp, num_fma, vec_size):
    peak = m * 128 * 128 / (num_fma * vec_size)
    cycles = peak * 100 / pp
    return cycles / freq


def genPlot(outfname, dfmm, dfmkl):
    dftime = dfmm[['i', 'blis']]
    def convert_time(df, idx, pmarker):
        d =[]
        dselect = df[[idx, pmarker]]
        for df in dselect.values:
            i = df[0]
            pp = df[1]
            d.append(getTimeFromPeakPerf(i, pp, 2, 16))
        return d
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=dfmm['i'],
                  y=convert_time(dfmm, 'i', 'perf_us'),
                  mode="markers",
                  name="Lambda/div"))
    #fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_blis_for"],
    #              mode="markers",
    #              name="Blis-like with for-partial-tile"))
    #fig.add_trace(go.Scatter(x=dfmm['i'], y=dfmm["perf_blis_unroll"],
    #              mode="markers",
    #              name="Blis-like with unroll-partial-tile"))
    fig.add_trace(go.Scatter(x=dfmkl['m'],
                  y=convert_time(dfmkl, 'm', 'peakperf(%)'),
                  mode="markers",
                  name="Mkl"))
    fig.add_trace(go.Scatter(x=dfmm['i'],
                  y=convert_time(dfmm, 'i', 'blis'),
                  mode="markers",
                  name="Blis"))
    fig.update_yaxes(title="time(us)")
    fig.update_xaxes(title="M")
    fig.show()
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.write_image(f'{outfname}.png')

dfmm = pd.read_csv(args.mmcsvfile, sep=",")
dfmkl = pd.read_csv(args.mklcsvfile, sep=",")
#dfmatmul_paper = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/onednn_nancy_seq.csv",sep=",")

genPlot(args.outputname, dfmm, dfmkl)

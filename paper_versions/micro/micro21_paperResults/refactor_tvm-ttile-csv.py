import pandas as pd
import argcomplete, argparse
import os
import glob

parser = argparse.ArgumentParser(description='Merge all csv from the directory defined by path.')
parser.add_argument('path', help="Precise absolute or relative path for the directory full of csv.")
parser.add_argument('outputname', help="Precise basename for the csv output file.")
argcomplete.autocomplete(parser)
args = parser.parse_args()
path=args.path
csvoutname = args.outputname
first = True
for filename in glob.glob(os.path.join(path, "*.csv")):
    print(filename)
    if first:
        first = False
        df = pd.read_csv(filename, sep=";") 
        df.to_csv(csvoutname,mode='w',sep=";")
    else:
        df = pd.read_csv(filename,sep=";") 
        df.to_csv(csvoutname,header=False, mode='a',sep=";")
print(csvoutname,"generated.")

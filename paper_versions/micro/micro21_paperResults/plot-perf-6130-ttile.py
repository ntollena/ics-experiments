import pandas as pd
import argcomplete, argparse
import os

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('convsizes', help="path")
parser.add_argument('ttile_old', help="path")
parser.add_argument('ttile_random', help="path")
parser.add_argument('ttile_metric', help="path")
argcomplete.autocomplete(parser)
args = parser.parse_args()

d = pd.read_csv(args.convsizes)

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)


def genPlot(outfname,dfttile_old,dfttile_random,dfttile_metric,core,fma,vectorsize,freq):
    """
    """
    ## per peak performance list for one archi
    ## handle ttile CSV (multiple values)
    dfl = [dfttile_old.loc[dfttile_old.MethodParallelization == 0],
    dfttile_random.loc[dfttile_random.MethodParallelization == 0],
    dfttile_random.loc[dfttile_random.MethodParallelization == 1],
    dfttile_random.loc[dfttile_random.MethodParallelization == 2],
    dfttile_metric.loc[dfttile_metric.MethodParallelization == 0],
    dfttile_metric.loc[dfttile_metric.MethodParallelization == 1],
    dfttile_metric.loc[dfttile_metric.MethodParallelization == 2]]
    newdfl = []
    for k,df in enumerate(dfl):
        df = df.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl
        newdfl += [df]
    import plotly.graph_objects as go
    
    fig = go.Figure()
    namel = ['TVM+TTILE old (// ioopt)',
            'TVM+TTILE random (// ioopt)',
            'TVM+TTILE random (// inner)',
            'TVM+TTILE random (// outter)',
            'TVM+TTILE metric (// ioopt)',
            'TVM+TTILE metric (// inner)',
            'TVM+TTILE metric (// outter)',
            ]
    colorl=['blue','#17aa15','#3bd80d','#f61608','indianred','lightsalmon','#ba94ef','#955ae7','#7227dc']
    for df,name,color in zip(newdfl,namel,colorl):
        fig.add_trace(go.Bar(
            x=df['NameConv'].values,
            error_y=dict(type='data', array=df['std'].values),
            y=df['perPP'].values,
            name=name,
            marker_color=color
        ))
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(height=600, width=1200)
    fig.update_yaxes(title="% of peak performance")
    fig.write_image(f'{outfname}.png')

## AVX512 Gold
dfttile_old = pd.read_csv   (args.ttile_old,sep=";")
#dfttile_old2 = pd.read_csv  (args.ttile_old2,sep=";")
dfttile_random = pd.read_csv(args.ttile_random,sep=";")
dfttile_metric = pd.read_csv(args.ttile_metric,sep=";")
genPlot("ttile-differentImplem-XeonGold-avx512",dfttile_old,dfttile_random,dfttile_metric,core=32,fma=2,vectorsize=16,freq=2100000000)

import pandas as pd
import argcomplete, argparse
import os

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('convsizes', help="path to conv size csv")
parser.add_argument('tvmttile32', help="path to tvm+ttile csv")
parser.add_argument('tvmttile1', help="path to tvm+ttile csv")
parser.add_argument('autotvm32', help="path to autotvm csv")
parser.add_argument('autotvm1', help="path to autotvm csv")
parser.add_argument('onednn32', help="path to oneDNN csv")
parser.add_argument('onednn1', help="path to oneDNN csv")
parser.add_argument('--onednn_bool', help="Optional: set true to plot oneDNN too", action='store_true', default=False)
argcomplete.autocomplete(parser)
args = parser.parse_args()
oneDNN = args.onednn_bool

d = pd.read_csv(args.convsizes)

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    ## multiplied by 1000 because time is in ms in the csv
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)


def genPlot(outfname,dfttile32,dfttile1,dfautotvm32,dfautotvm1,dfonednn_old32,dfonednn_old1,fma,vectorsize,freq, parmethod=0, oneDNN=False):
    ##BEST of all parallelization techniques
    #dfttile_0 = dfttile.loc[dfttile['MethodParallelization'] == 0].copy()
    dfttile32 = dfttile32.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    dfttile1 = dfttile1.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    seqlist=[dfttile1,dfautotvm1]
    parlist=[dfttile32,dfautotvm32]
    concat = []
    if oneDNN:
        dfonednn32 = dfonednn_old32.rename(columns={'stddev_wlast': 'std', 'mean_wlast': 'Time(ms)'})
        dfonednn1 = dfonednn_old1.rename(columns={'stddev_wlast': 'std', 'mean_wlast': 'Time(ms)'})
        seqlist += [dfonednn1]
        parlist += [dfonednn32]
    core = 32
    for k,df in enumerate(parlist):
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl
    core = 1
    for k,df in enumerate(seqlist):
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl

    if oneDNN:
        dfonednn1['std'] = dfonednn1['std'].values*dfonednn1['perPP'].values/100.
        dfonednn32['std'] = dfonednn32['std'].values*dfonednn32['perPP'].values/100.
    import plotly.graph_objects as go
    
    title = "Parallel (32 threads): TVM+Ttile vs AutoTVM on XeonGold6130 32 cores, 2 socket, 2 FMA, avx512, 2.1GHz"
    fig = go.Figure()
    #colors = ['#085dee', '#1550b6', '#04f116','#15b61a','#ee8908','#b96b07']
    colors = ['#085dee', '#1550b6']
    colors = ["Blue","Red"]
    colors = ['#085dee','#04f116', '#1550b6', '#15b61a']
    ## Red and Black
    colors = ['#f50000','#141212','#ff706b','#aaaaaa']
    namelist = [
            "TVM+TTILE best of all: 1 thread",
            "AutoTVM 1 thread",
            "TVM+TTILE best of all: 32 threads",
            "AutoTVM 32 threads",
            ]

    for color,name,df in zip(colors,namelist,[dfttile1,dfautotvm1,dfttile32, dfautotvm32]):
        fig.add_trace(go.Bar(
            x=df['NameConv'].values,
            error_y=dict(type='data', array=df['std'].values),
            y=df['perPP'].values,
            name=name,
            marker_color=color
        ))

    if oneDNN:
        for color,name,dfonednn in zip(colors,namelist,[dfonednn32, dfonednn1]):
            fig.add_trace(go.Bar(
                x=dfonednn['NameConv'].values,
                y=dfonednn['perPP'].values,
                error_y=dict(type='data', 
                    #array=dfonednn['std'].values*dfonednn['perPP'].values/100.),
                    array=dfonednn['std'].values),
                name=name,
                marker_color=color
            ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(legend=dict(
    yanchor="middle",
    y=0.50,
    xanchor="right",
    x=1.31
    ))
    fig.update_layout(height=600, width=1200)
    #fig.update_layout(title=dict("text":title))
    fig.update_yaxes(title="% of peak performance")
    fig.update_xaxes(title=title)
    fig.write_image(f'{outfname}.png')

## AVX512 Gold
dfautotvm32 = pd.read_csv(args.autotvm32, sep=",")
dfautotvm1 = pd.read_csv(args.autotvm1, sep=",")

dfttile32 = pd.read_csv(args.tvmttile32,sep=";")
dfttile1 = pd.read_csv(args.tvmttile1,sep=";")
if oneDNN:
    dfonednn32 = pd.read_csv(args.onednn32,sep=",")
    dfonednn1 = pd.read_csv(args.onednn1,sep=",")
else:
    dfonednn32 = None
    dfonednn1 = None

## sequential or Parallel
genPlot(args.outputname,dfttile32,dfttile1,dfautotvm32,dfautotvm1,dfonednn32,dfonednn1,fma=2,vectorsize=16,freq=2100000000,oneDNN=oneDNN)
## Obsolete
#genPlot("XeonGold5220-avx512-micro21rebuttal-sequential",dfttile,dfautotvm,dfonednn,core=1,fma=1,vectorsize=16,freq=2200000000)

#!/usr/bin/env python
import pandas as pd
import argcomplete, argparse
import os

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('outputname', help="output file base name (without the .png)")
parser.add_argument('convsizes', help="path to conv size csv")
parser.add_argument('benchdnn_old', help="path to BenchDNN old commit csv")
parser.add_argument('benchdnn_new', help="path to BenchDNN new commit csv")
parser.add_argument('matmulbench_onednn', help="path to Matmul_bench OneDNN csv")
#parser.add_argument('ttile', help="path to tvm+ttile csv")
argcomplete.autocomplete(parser)
args = parser.parse_args()

d = pd.read_csv(args.convsizes)

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    ## multiplied by 1000 because time is in ms in the csv
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)

def genPlot(outfname, dfbdnn_old, dfbdnn_recent, dfmatmul, fma, vectorsize, freq):
    ##BEST of all parallelization techniques
    #dfttile_0 = dfttile.loc[dfttile['MethodParallelization'] == 0].copy()
    core =1
    seqlist = [dfbdnn_old,dfbdnn_recent, dfmatmul]
    time_field = "avg(ms)"
    for k,df in enumerate(seqlist):
        print(df)
        #print(d.loc[d.name == x].f.values[0])
        print(df.name.values)
        peakperf = [
                peakPerf(d.loc[d.name == x].f.values[0],
                    d.loc[d.name ==x].c.values[0],
                    d.loc[d.name ==x].h.values[0],
                    d.loc[d.name ==x].w.values[0],
                    d.loc[d.name ==x].y.values[0],
                    d.loc[d.name ==x].stride.values[0],
                    d.loc[d.name ==x].x.values[0],
                    core,fma,vectorsize, freq)
                for x,y in zip(df.name.values, [float(x) for x in list(df[time_field].values)])]
        PPl = [100*y/x for x,y in zip(df[time_field].values,peakperf)]
        df['perPP'] = PPl

    import plotly.graph_objects as go
    
    title = "Comparison OneDNN: new and old API"
    fig = go.Figure()
    #colors = ['#085dee', '#1550b6', '#04f116','#15b61a','#ee8908','#b96b07']
    #colors = ['#085dee','#04f116', '#1550b6', '#15b61a']
    colors = ['#085dee', '#1550b6']
    colors = ["Blue","Red","Green"]
    namelist = [
            "Bench DNN 3db1ff (old)",
            "Bench DNN 9e67d40 (most recent)",
            "OneDNN matmul_bench"
            ]

    #for color,name,df in zip(colors,namelist,[dfttile1,dfautotvm1,dfttile32, dfautotvm32]):
    #    fig.add_trace(go.Bar(
    #        x=df['NameConv'].values,
    #        error_y=dict(type='data', array=df['std'].values),
    #        y=df['perPP'].values,
    #        name=name,
    #        marker_color=color
    #    ))

    for color,name,dfonednn in zip(colors,namelist,seqlist):
        fig.add_trace(go.Bar(
            x=dfonednn['name'].values,
            y=dfonednn['perPP'].values,
            #error_y=dict(type='data', 
            #    #array=dfonednn['std'].values*dfonednn['perPP'].values/100.),
            #    array=dfonednn['std'].values),
            name=name,
            marker_color=color
        ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(legend=dict(
    yanchor="middle",
    y=0.50,
    xanchor="right",
    x=1.31
    ))
    fig.update_layout(height=600, width=1200)
    #fig.update_layout(title=dict("text":title))
    fig.update_yaxes(title="% of peak performance")
    fig.update_xaxes(title=title)
    fig.write_image(f'{outfname}.png')

#dfbenchdnn_old = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/benchdnn_seq_gros_3db1ff.csv",sep=",") #new=
#dfbenchdnn_recent = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/benchdnn_seq_gros_9e67d40.csv",sep=",") #old
#dfmatmul_paper = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/nancy_seq.csv",sep=",")
dfbenchdnn_old = pd.read_csv(args.benchdnn_old,sep=",") #new=
dfbenchdnn_recent = pd.read_csv(args.benchdnn_new,sep=",") #old
dfmatmul_paper = pd.read_csv(args.matmulbench_onednn,sep=",")
#dfmatmul_paper = pd.read_csv("/home/hbrunie/CORSE/gitlab/ics-experiments/perf_ref/onednn/seq/avx512/nancy-gros/onednn_nancy_seq.csv",sep=",")

genPlot(args.outputname,dfbenchdnn_old, dfbenchdnn_recent,dfmatmul_paper,fma=2,vectorsize=16,freq=2100000000)

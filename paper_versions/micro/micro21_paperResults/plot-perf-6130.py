import pandas as pd
import argcomplete, argparse
import os

##['IdRun', 'NameConv', 'Time(ms)', 'std', 'NbMicroKernel', 'AxeFuse',
## 'SizeAxeFuse', 'Schema', 'Time_search']
parser = argparse.ArgumentParser(description='Pretty plot csv')
parser.add_argument('convsizes', help="Precise absolute path for root dir of ics-experiments.")
parser.add_argument('onednn', help="Precise absolute path for root dir of ics-experiments.")
parser.add_argument('tvmttile', help="Precise absolute path for root dir of ics-experiments.")
parser.add_argument('autotvm', help="Precise absolute path for root dir of ics-experiments.")
argcomplete.autocomplete(parser)
args = parser.parse_args()

d = pd.read_csv(args.convsizes)

def peakPerf(f,c,h,w,y,stride,x,core,fma,vectorsize,freq):
    print(freq)
    return ((f * c * h * w * (y/stride) * (x/stride)) / (vectorsize* fma * core)  / (freq) * 1000)


def genPlot(outfname,dfttile,dfautotvm,dfonednn,core,fma,vectorsize,freq):
    """
    """
    ## per peak performance list for one archi
    ## handle ttile CSV (multiple values)
    dfttile = dfttile.loc[dfttile['NameConv'] != "ttile"].copy()
    dfttile = dfttile.sort_values("Time(ms)").groupby("NameConv", as_index=False).first().copy()
    for df in [dfttile,dfautotvm]:
        print(df)
        peakperf = [peakPerf(d.loc[d.name == x].f.values[0],d.loc[d.name ==x].c.values[0],d.loc[d.name ==x].h.values[0],d.loc[d.name ==x].w.values[0],d.loc[d.name ==x].y.values[0],d.loc[d.name ==x].stride.values[0],d.loc[d.name ==x].x.values[0],core,fma,vectorsize, freq) for x,y in zip(df.NameConv.values, [float(x) for x in list(df["Time(ms)"].values)])]
        #print(peakperf)
        PPl = [100*y/x for x,y in zip(df["Time(ms)"].values,peakperf)]
        df['perPP'] = PPl
    print(dfonednn)
    dfonednn_xlast = dfonednn[['bench','perf_xlast','std_dev_xlast']].rename(columns={"bench": "NameConv", "perf_xlast": "perPP", "std_dev_xlast" : "std"}).copy() 
    dfonednn_flast = dfonednn[['bench','perf_flast','std_dev_flast']].rename(columns={"bench": "NameConv", "perf_flast": "perPP", "std_dev_flast" : "std"}).copy() 
    concat = [dfttile[['NameConv','perPP','std']],dfautotvm[['NameConv','perPP','std']],dfonednn_flast,dfonednn_xlast]
    
    import plotly.graph_objects as go
    
    fig = go.Figure()
    df = pd.concat(concat)
    print(dfonednn_flast )

    fig.add_trace(go.Bar(
        x=dfttile['NameConv'].values,
        error_y=dict(type='data', array=dfttile['std'].values),
        y=dfttile['perPP'].values,
        name='TVM+TTILE',
        marker_color='indianred'
    ))
    
    fig.add_trace(go.Bar(
        x=dfautotvm['NameConv'].values,
        y=dfautotvm['perPP'].values,
        error_y=dict(type='data', array=dfautotvm['std'].values),
        name='AutoTVM',
        marker_color='lightsalmon'
    ))

    #fig.add_trace(go.Bar(
    #    x=dfonednn_flast['NameConv'].values,
    #    y=dfonednn_flast['perPP'].values,
    #    error_y=dict(type='data', array=dfonednn_flast['std'].values),
    #    name='onednn_clast',
    #    marker_color='green'
    #))

    fig.add_trace(go.Bar(
        x=dfonednn_xlast['NameConv'].values,
        y=dfonednn_xlast['perPP'].values,
        error_y=dict(type='data', #array=dfonednn_xlast['std'].values),
        array=dfonednn_xlast['std'].values*dfonednn_xlast['perPP'].values/100.),
        name='onednn_wlast',
        marker_color='green'
    ))
    
    # Here we modify the tickangle of the xaxis, resulting in rotated labels.
    fig.update_layout(barmode='group', xaxis_tickangle=-45)
    fig.update_layout(height=600, width=1200)
    fig.update_yaxes(title="% of peak performance")
    fig.write_image(f'{outfname}.png')

## AVX512 Gold
dfautotvm = pd.read_csv(args.autotvm)
dfttile = pd.read_csv(args.tvmttile,sep=";")
dfonednn = pd.read_csv(args.onednn)
genPlot("ttile-differentImplem-XeonGold-avx512",dfttile,dfautotvm,dfonednn,core=32,fma=2,vectorsize=16,freq=2100000000)

IMPORTANT:
==========
When adding directory with new results: 
 - write the machine name in README file
 - nb threads used
 - time

DIRECTORY TREE 
=============== 

* common_csv:
    - CSV with Benchmark sizes
* data_mine:
    - OCaml scripts to parse log and csv files
* heatmap_gen:
    - Python scripts to generate heatmap from microkernel performance CSV.
* ics_results:
    - explicit
* micro21_xp:
    - explicit
* paper_writting:
    - Review from ICS submission

PERFORMANCE
=================

perf_microkernels:
------------
Performance obtained with matmul_bench/ml_utils make microkern
- HEADER: `x,y,w,h,c,f,res`
- matmul_bench COMMIT `7ad1e219a3ccca64eba6ffa806f434f128878628`
    * avx2 or avx512/INTELi9-7940X_motoko/
    * avx2/XeonE52680/

perf_ref: 
------------
* AutoTVM:
    - GIT: git@gitlab.inria.fr:spouget/tvm_ttile.git
    - COMMIT: 76ed4c3898be4b4cd75075228aea88860c0d4e4a
    - HEADER: nbthreads,archi,MainConv,NameConv,Time(ms),std
    - SEP: ,
* avx512 XeonGold6130(1,2,4,8,16 and 32 threads) and XeonGold5220(1 and 18 threads)
       - result_all.csv contains all csv merged into one.
       - /!\ NO YOLO9000_23 RESULTS for architecture XeonGold5220 /!\
       - log files from AutoTVM
* old: obsolete results, old method for measuring time.
* OneDNN
    - GIT: git@gitlab.inria.fr:ntollena/matmul_bench.git
    - COMMIT: 7ad1e219a3ccca64eba6ffa806f434f128878628
    - HEADER: bench,perf_xlast,std_dev_xlast,perf_flast,std_dev_flast
    * par (parallel: max num threads) seq (sequential: 1 thread)
    * 2 types of file: peak_perf and execution_time
    * Percentage Peak Perf:
      - onednn_32threads_2sockets.csv // Data brut
      - comparison-ttile-onednn-64threads.csv // Data brut: hyperthreads
      - results_32threads_2sockets_formatted.csv // from treat.py script on data brut
      - results_32threads_normalized.csv /!\ SAME BUT BETTER PERF: WHY?
    * OBSOLETE: onednn-results-tobastavx512-parallel /!\ Frequence were not fixed.
    * OBSOLETE: onednn/par/avx2/XeonE3-1240 /!\ Frequence were not fixed.
    * OBSOLETE: onednn/seq/avx2/XeonE3-1240v6 /!\ Frequence were not fixed.

perf_schemes: 
------------
    GIT: git@gitlab.inria.fr:spouget/tvm_ttile.git
    COMMIT: 76ed4c3898be4b4cd75075228aea88860c0d4e4a
    HEADER: IdRun;MethodParallelization;NameConv;Time(ms);std;NbMicroKernel;AxeFuse1;SizeAxeFuse1;AxeFuse2;SizeAxeFuse2;Schema;Time_search(2.936302423477173)
    /!\ python_scripts/refactor.py to refactor all the csv into one /!\
    (*) avx512/ 
        -XeonGold5220/ (tvm+ttile)
        -XeonGold6130/
        -ttile/INTELi9-7940X_motoko
    (*) avx2/
        -INTELi9-7940X_motoko/ (N Derumigny)
        -obsolete_XeonE3-1240v6/TVMTTILE/
        -obsolete_XeonE3-1240v6/ttile/
        -XeonE52680/ (Grid5000)

python_scripts: 
-----------
    * Python Script to generate csv with volumes and other data from perf,scheme csv.
    * Python Script to generate figure to highlight selected scheme with metric
autotvm_1_core_avx2_tobast_server:
    * AutoTVM sequential on avx2 tobast server

ARCHIS:
=========
* tobast (OBSOLETE /!\ Frequence not fixed ...)
    modelname: Intel(R) Xeon(R) Silver 4114 CPU @ 2.20GHz
    10 cores (2 hyperthreads)
    AVX512 1FMA
    modelname: Intel(R) Xeon(R) CPU E3-1240 v6 @ 3.70GHz
    AVX2 2FMA

* motoko
    modelname: Intel(R) Core(TM) i9-7940X CPU @ 3.10GHz
    L1/L2/L3: 32KB/1MB/19MB
    AVX512 2FMA
    AVX2 2FMA

* pinocchio
    CPU: 2x Intel Xeon Gold 6230R with 26 cores / 52 threads each
    - Base Frequency:         2.1 GHz
    - Turbo Frequency:        4.0 GHz
    - AVX512 Units:             2 per core

* Grid5000
    * 1 socket Cascade Lake:
    - Nancy gros
    - modelname: Intel(R) Xeon(R) Gold 5220 CPU @ 2.20GHz
    - AVX512 2FMA
    - 18 cores
    - l1,l2,l3 = 32KB(32768),1024KB(1048576),24.75MB(25952256)
    * 2 sockets Skylake:
    - Grenoble dahu
    - modelname: Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz
    - AVX512 2FMA
    - 16 cores
    - l1,l2,l3 = 32KB(32768),1024KB(1048576),22MB(23068672)
    * Broadwell:
    - Lille chifflet
    - modelname: Intel(R) Xeon(R) CPU E5-2680 v4 @ 2.40GHz
    - 14 cores
    - AVX2 2FMA
    - l1,l2,l3 = 32KB(32768),256KB(262144),35MB(36700160)
    * Haswell:
    - Nancy / grisou
    - modelname: 2 x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40GHz
    - 8 cores
    - AVX2 2FMA
    - l1,l2,l3 = 32KB(32768),256KB(262144),20MB(20971520)

Note on notations:
------

``` C
input = c * (x + w - 1) * (y + h -1) 
ouput = f * x * y 
params = f * c* w * h 
K(=f)=32, C(=c)=32, H/W(=x/y)=112 et R/S(=h/w)=3

int naive_conv(float* output, float* input, float* params,
        int X, int Y, int W, int H, int C, int F, int B) {
    for (int x = 0; x < X; x++) { //width
        for (int y = 0; y < Y; y++) { //height
            for (int f = 0; f < F; f++) {input channel
                for (int c = 0; c < C; c++) {//output channel
                    for (int h = 0; h < H; h++) {//height
                        for (int w = 0; w < W; w++) {//width
                            for (int b = 0; b < B; b++) {
                                output[b, x, y, f] += input[b, x + w, y + h, c] * params[w, h, c, f];
                            }
                        }
                    }
                }
            }
        }
    }
}
```
